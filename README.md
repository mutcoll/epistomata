# Epistomata

To get an empty project that compiles and already writes something, check that you have the requirements 
installed, and do the following:

Linux:

```
git clone https://bitbucket.org/mutcoll/epistomata.git
cd epistomata
mkdir build && cd build
conan install .. --build missing
cmake -G "Unix Makefiles" ..
make
./bin/epistomata
```

## Requirements

- C++ compiler (`sudo apt-get install build-essential`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended: `sudo apt-get install python-pip; sudo pip install conan`)

## TODO
```
[x] goal based behaviour
[x] learn changes from observations
[ ] Test Sally-Anne
  [x] include goals in memory
  [ ] add autonomous changes
  [ ] alice finds out other agent's planning algorithm
[x] changes that alter your knowledge
[x] goal that require to perform several changes (recursive planning)
[/] space-based contexts
  [x] each space has name
  [ ] change requirement must differentiate regular position from inventory
[ ] automate traits anouncement
[/] more universe features
  [ ] healable
  [x] water fountain
  [ ] add give command
[x] performance test: look below
[ ] knowledge about internal goals?
[x] knowledge about internal states
[ ] more than one goal
[ ] compound changes
[x] add amount to a concept
[ ] changes include the delay they take
[ ] virtual changes which need computation to be created (WWJD?)
[ ] situations only achievable through one change should not need thorough planning
[ ] refactors
  [ ] unify lexicographicallyCompareSortedConcepts
[ ] normalize changes with concepts unchanged (count:0)?
```

#### performance as of 316e105b98c80dcd9daed26ebec8c8259cd02ceb

```
$ valgrind --tool=callgrind ./bin/benchmark_plan
...
plan: [movable]: 0.393486279 s
==12597== Events    : Ir
==12597== Collected : 25474881
==12597==
==12597== I   refs:      25,474,881
```


## Knowledge format (recursive contexts)

In order to express knowledge in and out of epistomata, the next data structure can be serialized or parsed. It must be fully conformant JSON. The main object is called a context. It has 4 fields: concepts, changes, subcontext and goals.

```
context = {
    "concepts" = map<string, double>,
    "changes" = array<change>,
    "subcontexts" = map<string, context>,
    "goals" = array<context>
}
```

A concept is an entity that is present. Without taking time into account. It's a map of string to
doubles, that stores number of occurrences of each concept.

A change is an action (or trait used) upon objects (having some syntax, i.e. with specific order) that results in some difference in a context, and has a probability of success. This can be thought as a possible evolution of a context across time.

```
change = {
    "trait" = string,
    "certainty" = double,
    "objects" = array<string>,
    "difference" = context
}
```

The subcontexts are other contexts that are part of the main context. This can be used to express the knowledge or belongings of an agent within an environment, or an environment reachable from another environment (spatial or temporal).

The goals are a list of desired contexts. If this knowledge is used to plan an action, the chosen
changes will affect the current context in order to convert it into these goal contexts.

### Examples

The next contexts show how to express the knowledge stated right before each of them:

There are 4 fruits:
```
{
    "concepts" : {
        "fruit" : 4
    }
}
```

There is a lack of 4 fruits:
```
{
    "concepts" : {
        "fruit" : -4
    }
}
```

Alice has 4 fruits:
```
{
    "subcontexts": {
        "alice": {
            "concepts": {
                "fruit": 4
            }
        }
    }
}
```

Alice knows that Bob lacks 4 fruits
```
{
    "subcontexts": {
        "alice": {
            "subcontexts": {
                "bob": {
                    "concepts": {
                        "fruit": -4
                    }
                }
            }
        }
    }
}
```

An action called "eat" that alice can do over fruits, reduces in 2 the amount of fruits, and has 0.75 of probability of success. If there's an environment where this action can be done, applying the change in the context `{ "concepts" : { "fruit" : 5 } }` may result in the context `{ "concepts" : { "fruit" : 3 } }`.
```
{
    "changes:": [{
        "trait": "eat",
        "certainty": 0.75,
        "objects": ["alice", "fruit"],
        "difference": {
            "concepts": {
                "fruit": -2
            }
        }
    }]
}
```

If alice reads a book, with 0.9 probability there will occur a difference in alice's knowledge (she will learn) so that she will know how to do an "unknown" action that with 0.4 certainty will make appear 1 unit of water.

```
{
    "changes": [{
        "trait": "askable",
        "certainty": 0.9,
        "objects": ["alice", "book"],
        "difference": {
            "subcontexts": {
                "alice": {
                    "changes": [{
                        "trait": "unknown",
                        "certainty": 0.4,
                        "difference": {
                            "concepts": {
                                "water": 1
                            }
                        }
                    }]
                }
            }
        }
    }]
}
```

Of course, the previous context was general knowledge, and alice could know it. Here we are expressing that alice knows of an action that will lead to her knowing a second action (that she might not know currently) and that second action will make water appear.
```
{
    "subcontexts": {
        "alice": {
            "changes": [{
                "trait": "askable",
                "certainty": 0.9,
                "objects": ["alice", "book"],
                "difference": {
                    "subcontexts": {
                        "alice": {
                            "changes": [{
                                "trait": "unknown",
                                "certainty": 0.4,
                                "difference": {
                                    "concepts": {
                                        "water": 1
                                    }
                                }
                            }]
                        }
                    }
                }
            }]
        }
    }
}
```

The goal is to have water in the surroundings:
```
{
  "goals": [
    {
      "concepts": {
        "water": 1
      }
    }
  ]
}
```

The goal of alice is to have water in her surroundings:
```
{
  "subcontexts": {
    "alice": {
      "goals": [
        {
          "concepts": {
            "water": 1
          }
        }
      ]
    }
  }
}
```
