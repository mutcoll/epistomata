/**
 * @file change.test.cpp
 * @author jmmut
 * @date 2016-11-09.
 */

#include <iostream>
#include <memory/Context.h>
#include <memory/Change.h>
#include <memory/Knowledge.h>
#include <entities/Seed.h>
#include <entities/WaterSource.h>
#include <entities/Character.h>
#include <entities/Book.h>
#include <managers/Map.h>
#include <utils/graphExplorer/GraphExplorer.h>
#include <utils/pathRanker/MostCertainPathRanker.h>
#include <io/TreeContextPrinter.h>
#include <entities/Fruit.h>
#include <utils/pathRanker/MostUsefulPathRanker.h>
#include <entities/Tree.h>
#include <managers/Engine.h>
#include "utils/test/TestSuite.h"
#include "utils/SignalHandler.h"

using namespace randomize::utils;
using namespace randomize::utils::graph;
using std::shared_ptr;
using std::make_shared;
using std::string;

const std::string ALICE = "alice";
const std::string FRUIT = "fruit";
const std::string TREE = "tree";
const std::string POSITION_0_0 = "position_0_0";
const std::string POSITION_1_0 = "position_1_0";
const std::string POSITION_1_1 = "position_1_1";
const std::string NOT_HUNGRY = "not hungry";
const std::string POSITION_2_1 = "position_2_1";
const std::string BOOK = "book";


namespace std {
  inline const string &to_string(const string &val) {
      return val;
  }
  inline string to_string(string &&val) {
      return val;
  }
};


/**
 * TODO: if you add more tests, try to put the TestSuite as a global variable, in several .cpp and
 * here one empty `main()`, and then compile them together.
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {
    SigsegvPrinter::activate();

    LOG_LEVEL(argc == 2? randomize::log::parseLogLevel(argv[1]) : LOG_WARN_LEVEL);

    bool failed = false;

    failed = failed or 0 != test::TestSuite({
            []() {
                shared_ptr<Changes> changes = std::make_shared<Changes>();
                shared_ptr<Concepts> concepts = std::make_shared<Concepts>();

                changes->addNode(std::make_shared<Change>(
                        traits::names[Properties::EDIBLE], buildContext({NOT_HUNGRY})));
                changes->addNode(std::make_shared<Change>(
                        traits::names[Properties::ASKABLE], buildContext({TREE})));
                Context context({}, concepts, changes, {});
                Knowledge knowledge(context);
                
                Planner planner;

                Context goal = buildContext({NOT_HUNGRY});
                const GraphExplorer::Path &path = planner.plan(
                        ALICE, knowledge, {}, goal);

                ASSERT_OR_THROW(path.valid);
                ASSERT_EQUALS_OR_THROW(path.certainty, 1);
                ASSERT_EQUALS_OR_THROW(path.accomplishment, 1);
                const Context &difference = path.accumulated->difference;
                ASSERT_EQUALS_OR_THROW(difference.concepts().size(), 1);
                ASSERT_OR_THROW(difference.concepts().findNode(Concept(NOT_HUNGRY)).first);
            }
    }, "basic plan test").countFailed();

    failed = failed or 0 != test::TestSuite({
            []() {
                shared_ptr<Changes> changes = std::make_shared<Changes>();
                shared_ptr<Concepts> concepts = std::make_shared<Concepts>();

                bool inserted = changes->addNode(std::make_shared<Change>(
                        traits::names[Properties::EDIBLE],
                        buildSortedConcepts({FRUIT}),
                        buildContext({NOT_HUNGRY}),
                        0.8));
                ASSERT_OR_THROW(inserted);
                inserted = changes->addNode(std::make_shared<Change>(
                        traits::names[Properties::NO_ACTION],
                        buildSortedConcepts({TREE}),
                        buildContext({FRUIT}),
                        0.5
                ));
                ASSERT_OR_THROW(inserted);
                Context context({}, concepts, changes, {});
                Knowledge knowledge(context);

                Planner planner;

                Context goal = buildContext({NOT_HUNGRY});
                const GraphExplorer::Path &path = planner.plan(
                        ALICE, knowledge, buildContext({TREE}), goal);

                ASSERT_OR_THROW(path.valid);
                ASSERT_EQUALS_OR_THROW(path.certainty, 0.4);
                ASSERT_EQUALS_OR_THROW(path.accomplishment, 0.5);   // to fulfill goal, there must not be any tree
                const Context &difference = path.accumulated->difference;
                ASSERT_EQUALS_OR_THROW(difference.concepts().size(), 2);
                ASSERT_OR_THROW(difference.concepts().findNode(Concept(NOT_HUNGRY)).first);
            }
    }, "medium plan test").countFailed();

    return failed;
}
