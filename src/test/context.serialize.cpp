/**
 * @file context.serialize.cpp.cpp
 * @author jmmut
 * @date 2017-04-02.
 */

#include <memory/Context.h>
#include <io/ContextParser.h>
#include <memory/Knowledge.h>
#include "utils/test/TestSuite.h"

void assertConceptFound(Context &parsed, const Concept &concept);
void assertChangeFound(Context &parsed, const PtrChange &change);

using randomize::utils::test::TestSuite;


int main(int argc, char **argv) {
    LOG_LEVEL(argc == 2? randomize::log::parseLogLevel(argv[1]) : LOG_WARN_LEVEL);
    TestSuite fromContext({
            [](){
                std::stringstream s;
                s << "{}";
                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 0);
            },
            [](){
                Context empty;
                std::string s(empty.stringJsonPrint());
                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 0);
            },
            [](){
                Context context;
                Concept concept = Concept("example", 2);
                context.concepts().addNode(concept);
                std::stringstream s(context.stringJsonPrint());
                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(not parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 1);
                ASSERT_EQUALS_OR_THROW(parsed.concepts().size(), 1);
                assertConceptFound(parsed, concept);
            },
            [](){
                Context context;
                PtrChange change = std::make_shared<Change>(Properties::ASKABLE,
                        buildSortedConcepts({"alice", "book"}),
                        buildContext({"alice", "seed"}), Certainty(0.7));

                context.changes().addNode(change);
                std::string s(context.stringJsonPrint());
                LOG_INFO("%s", s.c_str());

                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(not parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 1);
                ASSERT_EQUALS_OR_THROW(parsed.changes().size(), 1);
                assertChangeFound(parsed, change);
            },
            [](){
                Context context;
                Concept concept = Concept("example", 2);
                PtrChange change = std::make_shared<Change>(Properties::ASKABLE,
                        buildSortedConcepts({"charlie", "book"}),
                        buildContext({"alice", "seed"}), Certainty(0.7));

                context.concepts().addNode(concept);
                context.changes().addNode(change);
                std::string s(context.stringJsonPrint());
                LOG_INFO("%s", s.c_str());

                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(not parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 2);
                ASSERT_EQUALS_OR_THROW(parsed.concepts().size(), 1);
                assertConceptFound(parsed, concept);
                ASSERT_EQUALS_OR_THROW(parsed.changes().size(), 1);
                assertChangeFound(parsed, change);
            },
            [](){
                Context context;
                PtrChange change = std::make_shared<Change>(Properties::ASKABLE,
                        buildSortedConcepts({"alice", "book"}),
                        buildContext({"alice", "seed"}), Certainty(0.7));

                PtrChange change2 = std::make_shared<Change>(Properties::ASKABLE,
                        buildSortedConcepts({"bob", "book"}),
                        buildContext({"bob", "seed"}), Certainty(0.4));

                context.changes().addNode(change);
                context.changes().addNode(change2);
                std::string s(context.stringJsonPrint());
                LOG_INFO("%s", s.c_str());

                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(not parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 2);
                ASSERT_EQUALS_OR_THROW(parsed.changes().size(), 2);
                assertChangeFound(parsed, change);
                assertChangeFound(parsed, change2);
            },
            [](){
                Context subcontext;
                Concept concept("example", 2);
                Concept who("who");

                Context context;
                context.subcontext(who).concepts().addNode(concept);
                Concept outer("outer");
                context.concepts().addNode(outer);

                std::string s(context.stringJsonPrint());
                LOG_INFO("%s", s.c_str());

                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(not parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 2);
                assertConceptFound(parsed, outer);

                ASSERT_EQUALS_OR_THROW(parsed.subcontext().size(), 1);
                assertConceptFound(parsed.subcontext(who), concept);
            },
            [](){
                Context subcontext;
                Concept concept("example", 2);
                Concept who("who");

                Context context;
                context.subcontext(who).concepts().addNode(concept);

                PtrChange change = std::make_shared<Change>(Properties::ASKABLE,
                        buildSortedConcepts({"alice", "book"}),
                        buildContext({"alice", "seed"}), Certainty(0.7));
                context.subcontext(who).changes().addNode(change);

                Concept outer("outer");
                context.concepts().addNode(outer);

                std::string s(context.stringJsonPrint());
                LOG_INFO("%s", s.c_str());

                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(not parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 3);
                assertConceptFound(parsed, outer);

                ASSERT_EQUALS_OR_THROW(parsed.subcontext(who).size(), 2);
                assertConceptFound(parsed.subcontext(who), concept);
                assertChangeFound(parsed.subcontext(who), change);
            },
            [](){
                Concept concept("example", 2);
                Context goal;
                goal.concepts().addNode(concept);

                Context context;
                context.setGoals(goal);

                std::string s(context.stringJsonPrint());
                LOG_INFO("%s", s.c_str());

                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(not parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 1);
                ASSERT_OR_THROW(parsed.hasGoals());
                assertConceptFound(parsed.goals().operator*(), concept);
            },
            [](){
                Context context;
                PtrChange change = std::make_shared<Change>(Properties::ASKABLE,
                        buildSortedConcepts({"alice", "book"}),
                        buildContext({"alice", "seed"}), Certainty(0.7));

                PtrChange change2 = std::make_shared<Change>(Properties::GIVABLE,
                        buildSortedConcepts({"bob", "book"}),
                        buildContext({"bob", "seed"}), Certainty(0.4));

                PtrChange compound = Change::build({change, change2});
                context.changes().addNode(compound);

                std::string s(context.stringJsonPrint());
                LOG_INFO("%s", s.c_str());

                ContextParser parser;
                Context parsed = parser.parse(s);
                ASSERT_OR_THROW(not parsed.isEmpty());
                ASSERT_EQUALS_OR_THROW(parsed.size(), 1);
                ASSERT_EQUALS_OR_THROW(parsed.changes().size(), 1);
            }
    }, "from context");
    TestSuite fromString({
            [](){
                std::string s("{\"changes\":[{\"trait\":\"edible\", \"certainty\":1,\"objects\":[],"
                        "\"difference\":{}}]}");

                ContextParser parser;
                Context parsed = parser.parse(s);

                ASSERT_EQUALS_OR_THROW(parsed.changes().size(), 1);
            },
            [](){
                std::string s("{\"changes\":[{\"trait\":\"unprogrammedAction\", \"certainty\":1,"
                        "\"objects\":[],\"difference\":{}}]}");

                ContextParser parser;
                Context parsed = parser.parse(s);

                ASSERT_EQUALS_OR_THROW(parsed.changes().size(), 1);
            }
    }, "from string");
    return 0;
}

void assertConceptFound(Context &parsed, const Concept &concept) {
    std::pair<bool, Concepts::PtrNodeWrapper> found = parsed.concepts().findNode(concept);
    ASSERT_EQUALS_OR_THROW(found.first, true);
    bool lessThan, equal;
    found.second->node.isIdentical(concept, lessThan, equal);
    ASSERT_OR_THROW(equal);
}

void assertChangeFound(Context &parsed, const PtrChange &change) {
    std::pair<bool, Changes::PtrNodeWrapper> found = parsed.changes().findNode(change);
    ASSERT_EQUALS_OR_THROW(found.first, true);
    bool lessThan, equal;
    found.second->node->compare(*change, lessThan, equal);
    ASSERT_OR_THROW(equal);
    ASSERT_EQUALS_OR_THROW(found.second->node->certainty.get(), change->certainty.get());
}


