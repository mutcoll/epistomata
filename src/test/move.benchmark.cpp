/**
 * @file main.cpp
 */

#include <iostream>
#include <entities/Book.h>
#include "utils/SignalHandler.h"
#include "utils/time/TimeRecorder.h"
#include "log/log.h"
#include "managers/Map.h"
#include "managers/Engine.h"
#include "managers/Shell.h"
#include "entities/Character.h"

using namespace std;

int main(int argc, char **argv)
{
    // this prints in cout the stack trace if there is a segfault. try dereferencing a null in an inner function
    SigsegvPrinter::activate(cout);
    LOG_LEVEL(LOG_FATAL_LEVEL)
    randomize::utils::time::TimeRecorder t;


    size_t iterations = argc == 2? std::stoul(argv[1]) : 1;
    size_t x = 1000, y = 1000;
    size_t n = 10000;

    t.start();
    Map map(x, y);
    t.stop();
    std::cout << t.toString("create map") << std::endl;


    t.start();
    size_t i;
    for (i = 0; i < n/2; ++i) {
        size_t p = (i * i * i + i * i % (i + 1) + n * i) % 1000000;
        map.positions[p]->prepareToBeWalked(
                std::make_shared<Character>(1000000, std::to_string(p)));
        map.positions[p]->finishMovement();
    }
    t.stop();
    std::cout << t.toString("initialization characters") << std::endl;

    t.start();
    for (; i < n; ++i) {
        size_t p = (i * i * i + i * i % (i + 1) + n * i) % 1000000;
        map.positions[p]->prepareToBeWalked(
                std::make_shared<Book>(std::to_string(p)));
        map.positions[p]->finishMovement();
    }
    t.stop();
    std::cout << t.toString("initialization books") << std::endl;


    t.start();
    Engine engine(std::move(map));
    t.stop();
    std::cout << t.toString("create engine") << std::endl;


    t.start();
    for (size_t j = 0; j < iterations; ++j) {
        engine.iterate();
    }
    t.stop();
    std::cout << t.toString(std::to_string(iterations) + " iterations") << std::endl;

    cout << "engine.iterations = " << engine.iterations << endl;
    cout << "engine.searches = " << engine.searches << endl;
    cout << "avg = " << engine.iterations / double(engine.searches) << endl;

    return 0;
}
