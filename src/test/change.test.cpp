/**
 * @file change.test.cpp
 * @author jmmut
 * @date 2016-11-09.
 */

#include <iostream>
#include <memory/Context.h>
#include <memory/Change.h>
#include <memory/Knowledge.h>
#include <entities/Seed.h>
#include <entities/WaterSource.h>
#include <entities/Character.h>
#include <entities/Book.h>
#include <managers/Map.h>
#include <utils/graphExplorer/GraphExplorer.h>
#include <utils/pathRanker/MostCertainPathRanker.h>
#include <io/TreeContextPrinter.h>
#include <entities/Fruit.h>
#include <utils/pathRanker/MostUsefulPathRanker.h>
#include <entities/Tree.h>
#include <managers/Engine.h>
#include "utils/test/TestSuite.h"
#include "utils/SignalHandler.h"

using namespace randomize::utils;
using namespace randomize::utils::graph;
using std::shared_ptr;
using std::make_shared;
using std::string;

const std::string ALICE = "alice";
const std::string TREE = "tree";
const std::string POSITION_0_0 = "position_0_0";
const std::string POSITION_1_0 = "position_1_0";
const std::string POSITION_1_1 = "position_1_1";
const std::string NOT_HUNGRY = "not hungry";
const std::string POSITION_2_1 = "position_2_1";
const std::string BOOK = "book";


namespace std {
  inline const string &to_string(const string &val) {
      return val;
  }
  inline string to_string(string &&val) {
      return val;
  }
};


/**
 * TODO: if you add more tests, try to put the TestSuite as a global variable, in several .cpp and
 * here one empty `main()`, and then compile them together.
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {
    SigsegvPrinter::activate();

    LOG_LEVEL(argc == 2? randomize::log::parseLogLevel(argv[1]) : LOG_WARN_LEVEL);

    bool failed = false;

    failed = failed or 0 != test::TestSuite(argc, argv, {
            [](int argc, char **argv) -> int {
                int fails = 0;

                std::string entityName = "window";
                std::shared_ptr<Knowledge::Graph> concepts = std::make_shared<Knowledge::Graph>();
                concepts->addNode(Concept(entityName));
                ASSERT_OR_THROW(concepts->begin().operator*().getName() == entityName);

                //                Memory m({concepts, {}});
                //                ASSERT_OR_THROW(m.changes().begin() == m.changes().end());
                //                ASSERT_OR_THROW(m.concepts().begin().operator*().name == entityName);

                Context context(concepts);
                ASSERT_OR_THROW(context.concepts().begin().operator*().getName() == entityName);
                return fails;
            },
            [](int argc, char **argv) -> int {
                int fails = 0;

                auto rm = buildContext({"table"});
                ASSERT_OR_THROW(rm.concepts().begin().operator*().getName() == "table");

                Change change(Properties::TEACHABLE, {}, buildContext({"book"}), 1);
                ASSERT_OR_THROW(change.difference.concepts().begin()
                        != change.difference.concepts().end());
                ASSERT_OR_THROW(change.difference.concepts().begin().operator*().getName() == "book");

                return fails;
            }}, "change").countFailed();

    failed = failed or 0 != test::TestSuite(argc, argv, {
            [](int argc, char **argv) -> int {
                int fails = 0;
                std::string treeName = TREE;
                Context tree = buildContext({treeName});
                ASSERT_OR_THROW(tree.concepts().size() == 1);
                ASSERT_OR_THROW(tree.concepts().findNode(Concept(treeName)).first);

                Context empty;
                ASSERT_OR_THROW_MSG(not (tree - empty).isEmpty(), (tree - empty).stringTreePrint().c_str());
                ASSERT_OR_THROW_MSG(not (empty - tree).isPositive(), (empty - tree).stringTreePrint().c_str());

                ASSERT_OR_THROW_MSG((tree - tree).isEmpty(), (tree - tree).stringTreePrint().c_str());

                return fails;
            }}, "Context difference").countFailed();

    failed = failed or 0 != test::TestSuite({
            []() {
                std::string treeName = TREE;
                Digraset<Concept> tree;
                tree.addNode(Concept(treeName));
                ASSERT_OR_THROW(tree.size() == 1);
                ASSERT_OR_THROW(tree.findNode(Concept(treeName)).first);

                Digraset<Concept> empty;
                ASSERT_OR_THROW((tree - empty).size() == 1);

                ASSERT_OR_THROW((tree - tree).size() == 0);

                ASSERT_OR_THROW(empty < tree);
            },
            []() {
                Digraset<Concept> tree;
                tree.addNode(Concept(TREE, -1));

                Digraset<Concept> alice;
                alice.addNode(Concept(ALICE));
                ASSERT_OR_THROW(sum(tree, alice).size() == 2);

                bool stopped = false;
                const Concepts &concepts = sumOrStop(tree, alice, stopped);
                ASSERT_OR_THROW(stopped);
            },
            []() {
                Digraset<Concept> alice;
                alice.addNode(Concept(ALICE));

                Digraset<Concept> hungry;
                hungry.addNode(Concept(NOT_HUNGRY));

                bool stopped = false;
                const Concepts &concepts = sumOrStop(hungry, alice, stopped);
                ASSERT_OR_THROW(not stopped);
            }}, "Digraset difference").countFailed();

    failed = failed or 0 != test::TestSuite(argc, argv, {
            [](int argc, char **argv) -> int {
                int fails = 0;
                Map::PositionIterator iterator(5, 3);
                iterator.pos = iterator.getIndex(0, 0);
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, 0), iterator.getIndex(0, 0));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, +1), iterator.getIndex(0, 1));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, -1), iterator.getIndex(0, 2));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(-1, 0), iterator.getIndex(4, 0));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(+1, 0), iterator.getIndex(1, 0));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(-1, -1), iterator.getIndex(4, 2));
                iterator.pos = iterator.getIndex(1, 0);
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, 0), iterator.getIndex(1, 0));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, +1), iterator.getIndex(1, 1));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, -1), iterator.getIndex(1, 2));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(-1, 0), iterator.getIndex(0, 0));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(+1, 0), iterator.getIndex(2, 0));
                iterator.pos = iterator.getIndex(0, 1);
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, 0), iterator.getIndex(0, 1));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, +1), iterator.getIndex(0, 2));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, -1), iterator.getIndex(0, 0));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(-1, 0), iterator.getIndex(4, 1));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(+1, 0), iterator.getIndex(1, 1));
                iterator.pos = iterator.getIndex(4, 2);
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, 0), iterator.getIndex(4, 2));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, +1), iterator.getIndex(4, 3));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(0, -1), iterator.getIndex(4, 1));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(-1, 0), iterator.getIndex(3, 2));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(+1, 0), iterator.getIndex(0, 2));
                ASSERT_EQUALS_OR_THROW(iterator.getRelativeIndex(+1, +1), iterator.getIndex(0, 0));
                return fails;
            }}, "position iterator").countFailed();

    failed = failed or 0 != test::TestSuite({
            []() {
                Change firstChange(Properties::MOVABLE,
                        {},
                        buildContext({WaterSource::genericName}), 1);
                Change secondChange(Properties::MOVABLE,
                        {},
                        buildContext({WaterSource::genericName}), 1);
                size_t firstHash = firstChange.hash();
                size_t secondHash = secondChange.hash();
                ASSERT_EQUALS_OR_THROW(firstHash, secondHash);
            },[]() {
                Change firstChange(Properties::MOVABLE,
                        {},
                        buildContext({WaterSource::genericName}), 1);

                Concept negativeWaterSource(WaterSource::genericName, -1);
                Context waterSourceDifference;
                waterSourceDifference.concepts().addNode(negativeWaterSource);
                Change secondChange(Properties::MOVABLE, {}, waterSourceDifference, 1);

                size_t firstHash = firstChange.hash();
                size_t secondHash = secondChange.hash();
                ASSERT_OR_THROW(firstHash != secondHash);
            },
            []() {
                Change waterSourceIndication(Properties::UNKNOWN,
                        {},
                        buildContext({WaterSource::genericName}), 0.1);
                SortedConcepts objects = buildSortedConcepts({ALICE, "book"});
                Context difference;
                Concept aliceConcept = Concept("alice");
                difference.subcontext(aliceConcept).changes().addNode(copyChange(waterSourceIndication));

                Change readWillTeachAliceTheWaterSourceLocation(
                        Properties::Traits::ASKABLE, objects, difference, 1);

                size_t firstHash = readWillTeachAliceTheWaterSourceLocation.hash();
                size_t secondHash = readWillTeachAliceTheWaterSourceLocation.hash();
                ASSERT_EQUALS_OR_THROW(firstHash, secondHash);
            },
            []() {
                Change pathTo_10(
                        buildMovement("alice", "position_0_0", "position_1_0", {Seed::genericName},
                                {"position_1_1"}));
                Change pathTo_11(buildMovement("alice", "position_1_0", "position_1_1", {},
                        {"position_2_1"}));
                Change waterSourceLocation(buildMovement("alice", "position_1_1", "position_2_1",
                        {}, {WaterSource::genericName}));
                PtrChange pathToWater = Change::build({std::make_shared<Change>(pathTo_10),
                        std::make_shared<Change>(pathTo_11),
                        std::make_shared<Change>(waterSourceLocation)
                });

                Change pathReturnTo_11(buildMovement("alice", "position_2_1", "position_1_1",
                        {WaterSource::genericName}, {"position_1_0"}));
                Change pathReturnTo_10(buildMovement("alice", "position_1_1", "position_1_0",
                        {}, {"position_0_0"}));
                Change seedLocation(buildMovement("alice", "position_1_0", "position_0_0",
                        {}, {Seed::genericName}));
                PtrChange pathToSeed = Change::build({std::make_shared<Change>(pathReturnTo_11),
                        std::make_shared<Change>(pathReturnTo_10),
                        std::make_shared<Change>(seedLocation)
                });
                ASSERT_OR_THROW(pathToWater->hash() != pathToSeed->hash());
            }
    }, "hash change").countFailed();

    failed = failed or 0 != test::TestSuite(argc, argv, {
            [](int argc, char **argv) -> int {
                int fails = 0;

                std::shared_ptr<Character> character = std::make_shared<Character>(
                        Character::childrenEnergy, ALICE);
                Concept aliceConcept(ALICE);
                Change waterSourceIndication(Properties::MOVABLE,
                        {},
                        buildContext({WaterSource::genericName}), 1);
                Context disappear;
                SortedConcepts objects = buildSortedConcepts({ALICE, "book"});
                Context difference;
                difference.subcontext(aliceConcept).changes().addNode(
                        copyChange(waterSourceIndication));
                Change readWillTeachAliceTheWaterSourceLocation(
                        Properties::Traits::ASKABLE, objects, difference, 1);
                character->beTaught(readWillTeachAliceTheWaterSourceLocation);
                auto knowledge = character->beAsked();

                ASSERT_EQUALS_OR_THROW(knowledge.concepts().size(), 0);
                ASSERT_EQUALS_OR_THROW(knowledge.changes().size(), 1);
                auto &change = knowledge.changes().begin().operator*().operator*();
                ASSERT_EQUALS_OR_THROW(change.difference.size(), 1);


                std::shared_ptr<Book> book = std::make_shared<Book>();
                character->getGoals() = buildContext({WaterSource::genericName});
                Map world(2, 2);
                world.positions[0]->prepareToBeWalked(book);
                world.positions[0]->prepareToBeWalked(character);
                world.positions[0]->finishMovement();

                std::shared_ptr<Position> position(world.positions[0]);
                Action action = character->action({position});
                ASSERT_EQUALS_OR_THROW(action.trait, Properties::ASKABLE);
                return fails;
            }}, "Digraset knowledge planning").countFailed();

    failed = failed or 0 != test::TestSuite(argc, argv, {
            [](int argc, char **argv) -> int {
                int fails = 0;
                WalkableGraph::Edge a, b, c;
                SortedConcepts noSyntax;
                Context empty;
                a = std::make_shared<Change>(Properties::UNKNOWN, noSyntax, empty, 0.75);
                b = std::make_shared<Change>(Properties::UNKNOWN, noSyntax, empty, 0.2);
                c = std::make_shared<Change>(Properties::UNKNOWN, noSyntax, empty, 0.6);
                std::shared_ptr<GraphExplorer::Path> path = make_shared<GraphExplorer::Path>();
                path->edges = std::vector<WalkableGraph::Edge>({a, b, c});
                MostCertainPathRanker ranker;
                double expectedCertainty = 0.75 * 0.2 * 0.6;
                ASSERT_EQUALS_OR_THROW(ranker.getCertainty(path), expectedCertainty);
                return fails;
            },
            [](int argc, char **argv) -> int {
                int fails = 0;
                WalkableGraph::Edge a, b, c, d, e, f;
                SortedConcepts noSyntax;
                Context empty;
                a = std::make_shared<Change>(Properties::UNKNOWN, noSyntax, empty, 0.75);
                b = std::make_shared<Change>(Properties::UNKNOWN, noSyntax, empty, 0.2);
                c = std::make_shared<Change>(Properties::UNKNOWN, noSyntax, empty, 0.6);
                d = std::make_shared<Change>(Properties::NO_ACTION, noSyntax, empty, 0.8);
                e = std::make_shared<Change>(Properties::NO_ACTION, noSyntax, empty, 0.8);
                f = std::make_shared<Change>(Properties::NO_ACTION, noSyntax, empty, 0.8);
                std::shared_ptr<GraphExplorer::Path> firstPath = make_shared<GraphExplorer::Path>();
                std::shared_ptr<GraphExplorer::Path> secondPath = make_shared<GraphExplorer::Path>();
                firstPath->edges = std::vector<WalkableGraph::Edge>({a, b, c});
                secondPath->edges = std::vector<WalkableGraph::Edge>({d, e, f});
                MostCertainPathRanker ranker;
                ASSERT_EQUALS_OR_THROW(
                        ranker.getMostCertainPath(firstPath, secondPath)->edges[0]->trait,
                        secondPath->edges[0]->trait);
                return fails;
            },
            [](int argc, char **argv) -> int {
                int fails = 0;
                MostCertainPathRanker ranker;
                double certainty = 0.5;
                size_t repetitions = 4;
                double comparableCertainty = ranker.getComparableCertainty(certainty, repetitions);
                ASSERT_EQUALS_OR_THROW(comparableCertainty,
                        0.5 + 0.25 + 0.125 + 0.0625);
                certainty = 0.75;
                repetitions = 3;
                comparableCertainty = ranker.getComparableCertainty(certainty, repetitions);
                ASSERT_EQUALS_OR_THROW(comparableCertainty,
                        0.75 + 0.75 * 0.25 + 0.75 * 0.25 * 0.25);
                return fails;
            }
    }, "certainty ranker").countFailed();

    failed = failed or 0 != test::TestSuite({
            []() {
                Context aliceTree(buildContext({ALICE, TREE}));
                Context aliceTree2(buildContext({ALICE, TREE}));
                Context tree(buildContext({TREE}));
                ASSERT_OR_THROW(aliceTree.isSubsetOf(aliceTree2));
                ASSERT_OR_THROW(aliceTree.isSubsetOf(aliceTree));
                ASSERT_OR_THROW(tree.isSubsetOf(aliceTree));
                ASSERT_OR_THROW(not aliceTree.isSubsetOf(tree));
            }, []() {
                SortedConcepts aliceTree(buildSortedConcepts({ALICE, TREE}));
                Concepts aliceTree2(buildContext({ALICE, TREE}).concepts());
                SortedConcepts tree(buildSortedConcepts({TREE}));
                Concepts tree2(buildContext({TREE}).concepts());
                ASSERT_OR_THROW(conceptsAreSubsetOf(aliceTree, aliceTree2));
                ASSERT_OR_THROW(conceptsAreSubsetOf(tree, aliceTree2));
                ASSERT_OR_THROW(not conceptsAreSubsetOf(aliceTree, tree2));
            }
    }, "subsetTest").countFailed();

    failed = failed or 0 != test::TestSuite({
            []() {
                std::shared_ptr<Change> pathTo_10 = std::make_shared<Change>(buildMovement(ALICE, POSITION_0_0, POSITION_1_0,
                        {Seed::genericName}, {POSITION_1_1}));

                std::shared_ptr<Change> pathReturnTo_10 = std::make_shared<Change>(buildMovement(ALICE, POSITION_1_1, POSITION_1_0,
                        {WaterSource::genericName}, {POSITION_0_0}));

                std::shared_ptr<Change> fruitToSeed = std::make_shared<Change>(Properties::EDIBLE,
                        buildSortedConcepts({ALICE, Fruit::genericName}),
                        buildContext({Seed::genericName, NOT_HUNGRY}), 1);

                Context movementTo_10;
                movementTo_10.changes().addNode(pathTo_10);
                Context movementReturn_10;
                movementReturn_10.changes().addNode(pathReturnTo_10);
                const string &to_10_string = movementTo_10.stringTreePrint();
                const string &return_10_string = movementReturn_10.stringTreePrint();
                bool movementsAreEqual, lessThan, selfEqual;
                movementTo_10.compare(movementReturn_10, movementsAreEqual, lessThan);
                ASSERT_OR_THROW_MSG(not movementsAreEqual, (to_10_string + return_10_string).c_str());
                movementTo_10.compare(movementTo_10, selfEqual, lessThan);
                ASSERT_OR_THROW_MSG(selfEqual, to_10_string.c_str());
            }, []() {
                Change waterSourceIndication(Properties::UNKNOWN,
                        {},
                        buildContext({WaterSource::genericName}), 0.1);
                SortedConcepts objects = buildSortedConcepts({ALICE, "book"});
                Context difference;
                Concept aliceConcept = Concept("alice");
                difference.subcontext(aliceConcept).changes().addNode(copyChange(waterSourceIndication));

                Change readWillTeachAliceTheWaterSourceLocation(
                        Properties::Traits::ASKABLE, objects, difference, 1);

                bool equal, lessThan;
                readWillTeachAliceTheWaterSourceLocation.compare(
                        readWillTeachAliceTheWaterSourceLocation, lessThan, equal);
                ASSERT_OR_THROW(equal);
            }
    }, "compare test").countFailed();


    failed = failed or 0 != test::TestSuite({
            []() {
                Context empty;
                shared_ptr<Concepts> concepts = std::make_shared<Concepts>();
                concepts->addNode(Concept(Fruit::genericName, 10));
                Context goal(concepts);
                MostUsefulPathRanker ranker("agent unused", empty, goal);

                shared_ptr<Concepts> concepts2 = std::make_shared<Concepts>();
                concepts2->addNode(Concept(Fruit::genericName, 3));
                Context firstResult(concepts2);
                PtrChange changeA = std::make_shared<Change>(Properties::MOVABLE, firstResult, 0.4);
                ranker.pushEdge(changeA);
                ranker.processPath();
                ranker.popEdge();

                shared_ptr<Concepts> concepts3 = std::make_shared<Concepts>();
                concepts3->addNode(Concept(Fruit::genericName, 7));
                Context secondResult(concepts3);
                PtrChange changeB = std::make_shared<Change>(Properties::GIVABLE, secondResult, 0.7);
                ranker.pushEdge(changeB);
                ranker.pushEdge(changeB);
                ranker.processPath();
                ranker.popEdge();
                ranker.popEdge();

                ASSERT_OR_THROW(ranker.getBest().valid);
                ASSERT_EQUALS_OR_THROW(ranker.getBest().edges.size(), 2);
            }
    }, "useful plan test").countFailed();

    failed = failed or 0 != test::TestSuite({
            []() {
                shared_ptr<Concepts> fruit = std::make_shared<Concepts>();
                fruit->addNode(Concept(Fruit::genericName, 7));
                Context fruitContext(fruit);
                PtrChange fruitChange = std::make_shared<Change>(Properties::GIVABLE, fruitContext);

                shared_ptr<Concepts> tree = std::make_shared<Concepts>();
                tree->addNode(Concept(Tree::genericName, 5));
                Context treeContext(tree);
                PtrChange treeChange = std::make_shared<Change>(Properties::GIVABLE, treeContext);

                PtrChange changes = Change::build({fruitChange, treeChange});
                ASSERT_EQUALS_OR_THROW(changes->difference.size(), 2);
                ASSERT_EQUALS_OR_THROW(changes->trait, traits::names[Properties::COMPOUND]);
            },
            []() {
                shared_ptr<Concepts> fruit = std::make_shared<Concepts>();
                fruit->addNode(Concept(Fruit::genericName, 7));
                Context fruitContext(fruit);
                SortedConcepts fruitRequirement{Concept{Seed::genericName, 15}};
                PtrChange fruitChange = std::make_shared<Change>(Properties::GIVABLE,
                        fruitRequirement, fruitContext, 0.8);

                shared_ptr<Concepts> tree = std::make_shared<Concepts>();
                tree->addNode(Concept(Tree::genericName, 5));
                Context treeContext(tree);
                SortedConcepts treeRequirement{Concept{Fruit::genericName, 7}};
                PtrChange treeChange = std::make_shared<Change>(Properties::GIVABLE,
                        treeRequirement, treeContext, 0.6);

                PtrChange changes = Change::build({fruitChange, treeChange});
                ASSERT_EQUALS_OR_THROW(changes->difference.size(), 2);
                ASSERT_EQUALS_OR_THROW(changes->trait, traits::names[Properties::COMPOUND]);
                ASSERT_EQUALS_OR_THROW(changes->objects.size(), 0);
                ASSERT_EQUALS_OR_THROW(changes->certainty.get(), 0.8 * 0.6);
            },
            []() {
                shared_ptr<Concepts> fruit = std::make_shared<Concepts>();
                fruit->addNode(Concept(Fruit::genericName, 7));
                Context fruitContext(fruit);
                SortedConcepts fruitRequirement{Concept{Seed::genericName, 15}};
                PtrChange fruitChange = std::make_shared<Change>(Properties::GIVABLE,
                        fruitRequirement, fruitContext, 0.8);

                shared_ptr<Concepts> tree = std::make_shared<Concepts>();
                tree->addNode(Concept(Tree::genericName, 5));
                Context treeContext(tree);
                SortedConcepts treeRequirement{Concept{Water::genericName, 7}};
                PtrChange treeChange = std::make_shared<Change>(Properties::GIVABLE,
                        treeRequirement, treeContext, 0.6);

                PtrChange changes = Change::build({fruitChange, treeChange});
                ASSERT_EQUALS_OR_THROW(changes->difference.size(), 2);
                ASSERT_EQUALS_OR_THROW(changes->trait, traits::names[Properties::COMPOUND]);
                ASSERT_EQUALS_OR_THROW(changes->objects.size(), 0);
                ASSERT_EQUALS_OR_THROW(changes->certainty.get(), 0.8 * 0.6);
            },
            []() {
                Change pathTo_10(buildMovement(ALICE, POSITION_0_0, POSITION_1_0,
                        {Seed::genericName}, {POSITION_1_1}));
                Change waterSourceLocation(buildMovement(ALICE, POSITION_1_0, POSITION_1_1,
                        {POSITION_0_0}, {WaterSource::genericName}));
                PtrChange pathToWater = Change::build({make_shared<Change>(pathTo_10),
                        make_shared<Change>(waterSourceLocation)});

                Context goals;
                goals.concepts().addNode(Concept(WaterSource::genericName));
                shared_ptr<Character> character = make_shared<Character>(
                        Character::childrenEnergy, ALICE, goals);
                character->beTaught(*pathToWater);

                Map map(5, 5);
                Map::EntityIterator mapIterator = map.begin();
                mapIterator.getPosition(0, 0)->prepareToBeWalked(character);

                mapIterator.getPosition(0, 0)->prepareToBeWalked(std::make_shared<Seed>(3));
                mapIterator.getPosition(1, 1)->prepareToBeWalked(std::make_shared<WaterSource>());

                for (std::shared_ptr<Position> position : map.positions) {
                    position->finishMovement();
                }

                Engine engine(std::move(map));
                engine.iterate();
                ASSERT_EQUALS_OR_THROW(character->beAsked().changes().size(), 1);
                engine.iterate();
                ASSERT_EQUALS_OR_THROW(character->beAsked().changes().size(), 1);
                engine.iterate();
                Changes &changes = character->beAsked().changes();
                ASSERT_EQUALS_OR_THROW(changes.size(), 2);
                Changes::Iterator iterator = changes.begin();
                string compound = traits::names[Properties::COMPOUND];
                ASSERT_EQUALS_OR_THROW(iterator.operator*()->trait, compound);
                ++iterator;
                PtrChange &learnedChange = iterator.operator*();
                ASSERT_EQUALS_OR_THROW(learnedChange->trait, compound);
                ASSERT_EQUALS_OR_THROW(learnedChange->steps.size(), 2);
            }
    }, "compound change test").countFailed();

    return failed;
}
