/**
 * @file context.serialize.cpp.cpp
 * @author jmmut
 * @date 2017-04-02.
 */

#include <memory/Context.h>
#include <io/ContextParser.h>
#include <io/JsonContextPrinter.h>


int main(int argc, char **argv) {
    LOG_LEVEL(argc == 2 ? randomize::log::parseLogLevel(argv[1]) : LOG_WARN_LEVEL);
    ContextParser parser;
    const Context &context = parser.parse(std::cin);

    JsonContextPrinter printer;
    context.print(std::cout, printer);
    return 0;
}

