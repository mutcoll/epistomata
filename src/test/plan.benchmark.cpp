
#include <iostream>
#include <entities/Book.h>
#include <entities/WaterSource.h>
#include <entities/Seed.h>
#include <entities/Fruit.h>
#include <entities/Tree.h>
#include <io/JsonContextPrinter.h>
#include <utils/test/TestSuite.h>
#include "utils/SignalHandler.h"
#include "utils/time/TimeRecorder.h"
#include "managers/Map.h"
#include "entities/Character.h"

static const int SEED_LIFE = 3;
using namespace std;

int main(int argc, char **argv)
{
    // this prints in cout the stack trace if there is a segfault. try dereferencing a null in an inner function
    SigsegvPrinter::activate();
//    LOG_LEVEL(LOG_FATAL_LEVEL)
//    LOG_LEVEL(LOG_DEBUG_LEVEL)
    LOG_LEVEL(argc == 2? randomize::log::parseLogLevel(argv[1]) : LOG_DEBUG_LEVEL);
    randomize::utils::time::TimeRecorder t;

//    size_t iterations = argc == 2? std::stoul(argv[1]) : 1;
//    size_t x = 1000, y = 1000;
//    size_t n = 10000;

    Change grabWater(Properties::GRABBABLE,
            buildSortedConcepts({"alice", WaterSource::genericName}),
            buildContext({Water::genericName}), 1);

    Change pathTo_10(buildMovement("alice", "position_0_0", "position_1_0", {Seed::genericName},
            {"position_1_1"}));
    Change pathTo_11(buildMovement("alice", "position_1_0", "position_1_1", {}, {"position_2_1"}));
    Change waterSourceLocation(buildMovement("alice", "position_1_1", "position_2_1",
            {}, {WaterSource::genericName}));
    PtrChange pathToWater = Change::build({std::make_shared<Change>(pathTo_10),
            std::make_shared<Change>(pathTo_11),
            std::make_shared<Change>(waterSourceLocation)
    });

    Change pathReturnTo_11(buildMovement("alice", "position_2_1", "position_1_1",
            {WaterSource::genericName}, {"position_1_0"}));
    Change pathReturnTo_10(buildMovement("alice", "position_1_1", "position_1_0",
            {}, {"position_0_0"}));
    Change seedLocation(buildMovement("alice", "position_1_0", "position_0_0",
            {}, {Seed::genericName}));
    PtrChange pathToSeed = Change::build({std::make_shared<Change>(pathReturnTo_11),
            std::make_shared<Change>(pathReturnTo_10),
            std::make_shared<Change>(seedLocation)
    });

    Context edibleContext;
    edibleContext.concepts().addNode(Concept(Seed::genericName));
    edibleContext.subcontext(Concept("alice")).concepts().addNode(Concept(Character::energyName, 1));
    Change fruitToSeed(Properties::EDIBLE,
            buildSortedConcepts({"alice", Fruit::genericName}),
            edibleContext, 1);

    Change waterAndSeedToTree(Properties::GIVABLE,
            buildSortedConcepts({"alice", Seed::genericName}),
            buildContext({Tree::genericName}) - buildContext({Water::genericName}), 1);

    Change treeToFruit(Properties::NO_ACTION,
            {}, buildContext({Fruit::genericName}) - buildContext({Tree::genericName}), 1);

    Change emptyAction(Properties::ASKABLE, {}, {}, 1);

    std::shared_ptr<Character> character = std::make_shared<Character>(
            Character::childrenEnergy, "alice");
    std::shared_ptr<Seed> seed = std::make_shared<Seed>(SEED_LIFE);

    character->beTaught(fruitToSeed);
    character->beTaught(waterAndSeedToTree);
    character->beTaught(treeToFruit);
    character->beTaught(grabWater);
    character->beTaught(*pathToWater);
    character->beTaught(*pathToSeed);
    character->beTaught(emptyAction);

    Map world(6, 6);
    world.positions[0]->prepareToBeWalked(character);
    world.positions[0]->prepareToBeWalked(seed);
    world.positions[0]->finishMovement();

    shared_ptr<Position> position(world.positions[0]);


    t.start();
    Action action = character->action({position});
    t.stop();
    LOG_INFO("plan: [%s]", traits::names[action.trait].c_str());
    ASSERT_EQUALS_OR_THROW(action.trait, Properties::MOVABLE);

    LOG_INFO("%s", jsonPrint(character->beAsked().get()).c_str());
    return 0;
}
