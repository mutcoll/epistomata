/**
 * @file context.serialize.cpp.cpp
 * @author jmmut
 * @date 2017-04-02.
 */

#include <memory/Context.h>
#include <io/ContextParser.h>
#include <memory/Knowledge.h>
#include "utils/test/TestSuite.h"

void assertConceptFound(Context &parsed, const Concept &concept);
void assertChangeFound(Context &parsed, const PtrChange &change);

using randomize::utils::test::TestSuite;


int main(int argc, char **argv) {
    LOG_LEVEL(argc == 2? randomize::log::parseLogLevel(argv[1]) : LOG_WARN_LEVEL);
    TestSuite fromContext({
            [](){
                Changes changes;
                changes.addNode(std::make_shared<Change>(Properties::ASKABLE, Context{}));
                changes.addNode(std::make_shared<Change>(Properties::MOVABLE, Context{}));

                ASSERT_EQUALS_OR_THROW(changes.size(), 2);
            },
            [](){
                Changes changes;
                changes.addNode(std::make_shared<Change>(Properties::ASKABLE, Context{}));
                changes.addNode(std::make_shared<Change>(Properties::ASKABLE, Context{}));

                ASSERT_EQUALS_OR_THROW(changes.size(), 1);
            }
    }, "basic digrunset");
    return 0;
}

void assertConceptFound(Context &parsed, const Concept &concept) {
    std::pair<bool, Concepts::PtrNodeWrapper> found = parsed.concepts().findNode(concept);
    ASSERT_EQUALS_OR_THROW(found.first, true);
    bool lessThan, equal;
    found.second->node.isIdentical(concept, lessThan, equal);
    ASSERT_OR_THROW(equal);
}

void assertChangeFound(Context &parsed, const PtrChange &change) {
    std::pair<bool, Changes::PtrNodeWrapper> found = parsed.changes().findNode(change);
    ASSERT_EQUALS_OR_THROW(found.first, true);
    bool lessThan, equal;
    found.second->node->compare(*change, lessThan, equal);
    ASSERT_OR_THROW(equal);
    ASSERT_EQUALS_OR_THROW(found.second->node->certainty.get(), change->certainty.get());
}


