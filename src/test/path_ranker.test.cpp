/**
 * @file change.test.cpp
 * @author jmmut
 * @date 2016-11-09.
 */

#include <iostream>
#include <memory/Context.h>
#include <memory/Change.h>
#include <memory/Knowledge.h>
#include <entities/Seed.h>
#include <entities/WaterSource.h>
#include <entities/Character.h>
#include <entities/Book.h>
#include <managers/Map.h>
#include <utils/graphExplorer/GraphExplorer.h>
#include <utils/pathRanker/MostCertainPathRanker.h>
#include <io/TreeContextPrinter.h>
#include <entities/Fruit.h>
#include <utils/pathRanker/MostUsefulPathRanker.h>
#include <entities/Tree.h>
#include <managers/Engine.h>
#include "utils/test/TestSuite.h"
#include "utils/SignalHandler.h"

using namespace randomize::utils;
using namespace randomize::utils::graph;
using std::shared_ptr;
using std::make_shared;
using std::string;

const std::string ALICE = "alice";
const std::string FRUIT = "fruit";
const std::string TREE = "tree";
const std::string POSITION_0_0 = "position_0_0";
const std::string POSITION_1_0 = "position_1_0";
const std::string POSITION_1_1 = "position_1_1";
const std::string NOT_HUNGRY = "not hungry";
const std::string POSITION_2_1 = "position_2_1";
const std::string BOOK = "book";


namespace std {
  inline const string &to_string(const string &val) {
      return val;
  }
  inline string to_string(string &&val) {
      return val;
  }
};


/**
 * TODO: if you add more tests, try to put the TestSuite as a global variable, in several .cpp and
 * here one empty `main()`, and then compile them together.
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {
    SigsegvPrinter::activate();

    LOG_LEVEL(argc == 2? randomize::log::parseLogLevel(argv[1]) : LOG_WARN_LEVEL);

    bool failed = false;

    failed = failed or 0 != test::TestSuite({
            []() {
                shared_ptr<Changes> changes = std::make_shared<Changes>();
                shared_ptr<Concepts> concepts = std::make_shared<Concepts>();

                changes->addNode(std::make_shared<Change>(
                        traits::names[Properties::EDIBLE], buildContext({NOT_HUNGRY})));
                changes->addNode(std::make_shared<Change>(
                        traits::names[Properties::ASKABLE], buildContext({TREE})));
                Context aliceSubcontext({}, concepts, changes, {});

                Context situation;
                situation.subcontext()[Concept(ALICE)] = aliceSubcontext;

                MostUsefulPathRanker::EdgesIterable edgesIterable(situation, ALICE);
                std::vector<PtrChange> edges;
                for (PtrChange &edge : edgesIterable) {
                    edges.push_back(edge);
                }
                ASSERT_EQUALS_OR_THROW(edges.size(), 2);
            }
    }, "basic iterable test").countFailed();

    return failed;
}
