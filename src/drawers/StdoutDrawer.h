/**
 * @file StdoutDrawer.h
 * @author jmmut
 * @date 2016-10-07.
 */

#ifndef EPISTOMATA_STDOUTDRAWER_H
#define EPISTOMATA_STDOUTDRAWER_H


#include <sstream>
#include "Drawer.h"

class StdoutDrawer : public Drawer {
public:
    StdoutDrawer(long fixed);

    virtual void draw(Book &b) override;
    virtual void draw(Character &c) override;
    virtual void draw(Fruit &f) override;
    virtual void draw(Seed &d) override;
    virtual void draw(Tree &f) override;
    virtual void draw(Water &f) override;
    virtual void draw(WaterSource &w) override;

    virtual void draw(Position &p) override;

    virtual void draw(Map &map) override;

private:
    std::stringstream buffer;
    long fixed;
};


#endif //EPISTOMATA_STDOUTDRAWER_H
