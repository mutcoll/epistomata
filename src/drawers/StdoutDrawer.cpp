/**
 * @file StdoutDrawer.cpp
 * @author jmmut
 * @date 2016-10-07.
 */

#include <traits/Properties.h>
#include <iostream>
#include <algorithm>
#include "StdoutDrawer.h"
#include <managers/Map.h>
#include "entities/Book.h"
#include "entities/Fruit.h"
#include "entities/Character.h"
#include "entities/Seed.h"
#include "entities/Tree.h"

StdoutDrawer::StdoutDrawer(long fixed) : fixed(fixed) {}

void StdoutDrawer::draw(Map &map) {

    auto positionIt = map.positionBegin();
    for (size_t iY = 0; iY < map.sizeY; ++iY) {
        for (size_t iX = 0; iX < map.sizeX; ++iX) {
            for (std::shared_ptr<Entity> entity : *map.positions[positionIt.getIndex(iX, iY)]) {
                long positionBefore = buffer.tellp();
                entity->receiveDrawer(*this);
                long positionAfter = buffer.tellp();
                if (positionAfter - positionBefore > 0) {
                    buffer << " ";
                }
            }
            long cellSize = buffer.tellp();
            long toFill = std::max(cellSize, fixed);
            buffer << std::string(toFill - cellSize, ' ');
            std::cout << buffer.str() << "| ";
            buffer.clear();
            buffer.str("");
        }
        std::cout << std::endl;
    }
}

void StdoutDrawer::draw(Book &b) {
    Knowledge knowledge = b.beAsked();
    buffer << "b";
    for (auto &lesson : knowledge.concepts()) {
        buffer << lesson.getName()[0];
    }
    buffer << ",";
    for (auto &change : knowledge.changes()) {
        buffer << change->trait[0];
    }
}

void StdoutDrawer::draw(Character &c) {
    buffer << "c" << c.getName()[0] << c.getRemainingLife();
}

void StdoutDrawer::draw(Fruit &f) {
    buffer << "f" << f.getRemainingLife();
}

void StdoutDrawer::draw(Seed &s) {
    buffer << "s" << s.getRemainingLife() << (s.hasWater()? "w" : "");
}

void StdoutDrawer::draw(Tree &t) {
    buffer << "t" << t.getRemainingLife();
}

void StdoutDrawer::draw(Water &w) {
    buffer << "w";
}

void StdoutDrawer::draw(Position &p) {
    // no operation
}

void StdoutDrawer::draw(WaterSource &w) {
    buffer << "WW";
}
