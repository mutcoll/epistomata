/**
 * @file Drawer.h
 * @author jmmut
 * @date 2016-10-07.
 */

#ifndef EPISTOMATA_DRAWER_H
#define EPISTOMATA_DRAWER_H

class Map;

class Book;
class Character;
class Fruit;
class Seed;
class Tree;
class Water;
class WaterSource;
class Position;



class Drawer {
public:
    virtual void draw(Book &b) = 0;
    virtual void draw(Character &c) = 0;
    virtual void draw(Fruit &f) = 0;
    virtual void draw(Seed &d) = 0;
    virtual void draw(Tree &f) = 0;
    virtual void draw(Water &w) = 0;
    virtual void draw(WaterSource &w) = 0;
    virtual void draw(Position &p) = 0;

    virtual void draw(Map &map) = 0;
};


#endif //EPISTOMATA_DRAWER_H
