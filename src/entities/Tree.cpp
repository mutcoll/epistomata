/**
 * @file Tree.cpp
 * @author jmmut
 * @date 2016-10-11.
 */

#include "Tree.h"
#include "Fruit.h"

std::string Tree::genericName = "tree";

Tree::Tree(int energy) : Killable(energy), fruits(2) {}

const std::string & Tree::getName() {
    return genericName;
}

void Tree::receiveDrawer(Drawer &drawer) {
    drawer.draw(*this);
}

Properties Tree::getTraits() {
    return //Grabbable::getTraits() +
            Killable::getTraits();
}

//std::shared_ptr<Entity> Tree::beGrabbed() {
//    if (fruits > 0) {
//        LOG_DEBUG("%s is grabbed a %s", getName().c_str(), Fruit(0).getName().c_str());
//        --fruits;
//        int portions = 10;
//        return std::make_shared<Fruit>(portions);
//    } else {
//        LOG_DEBUG("%s can not be grabbed anymore", getName().c_str());
//        return nullptr;
//    }
//}

std::vector<std::shared_ptr<Entity>> Tree::reincarnate() {
    // TODO return wood
    int portions = 10;
    return {std::make_shared<Fruit>(portions), std::make_shared<Fruit>(portions)};
}
