/**
 * @file Tree.h
 * @author jmmut
 * @date 2016-10-11.
 */

#ifndef EPISTOMATA_TREE_H
#define EPISTOMATA_TREE_H


#include "traits/Killable.h"
#include "traits/Grabbable.h"

class Tree :// public Grabbable,
         public Killable {
public:
    static std::string genericName;
    Tree(int energy);

    virtual Properties getTraits() override;

    virtual const std::string & getName() override;
    virtual void receiveDrawer(Drawer &drawer) override;

    // trait requirement
//    virtual std::shared_ptr<Entity> beGrabbed() override;

    virtual std::vector<std::shared_ptr<Entity>> reincarnate() override;

private:
    size_t fruits;
};


#endif //EPISTOMATA_TREE_H
