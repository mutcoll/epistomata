/**
 * @file Book.cpp
 * @author jmmut
 * @date 2016-10-01.
 */

#include <iostream>
#include <utils/exception/StackTracedException.h>
#include <log/log.h>
#include "Book.h"
#include "traits/Visitor.h"

Book::Book() : Book("book") {}
Book::Book(const std::string &name) : name(name) {}


const std::string & Book::getName() {
    return name;
}

void Book::receiveDrawer(Drawer &drawer) {
    drawer.draw(*this);
}

Properties Book::getTraits() {
    return Askable::getTraits() + Teachable::getTraits();
}

Knowledge & Book::beAsked() {
    LOG_DEBUG("%s being asked", name.c_str())
    return knowledge;
}



