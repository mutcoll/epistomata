/**
 * @file Character.h
 * @author jmmut
 * @date 2016-09-30.
 */

#ifndef EPISTOMATA_CHARACTER_H
#define EPISTOMATA_CHARACTER_H


#include <traits/Edible.h>
#include <traits/Entity.h>
#include <traits/Teachable.h>
#include <traits/Killable.h>
#include <traits/Talkable.h>
#include <traits/Askable.h>
#include <traits/Visitor.h>
#include <traits/Movable.h>
#include <traits/Givable.h>
#include <traits/Grabbable.h>
#include <set>
#include <drawers/Drawer.h>
#include <memory/Planner.h>
#include <managers/Action.h>
#include "Position.h"

class Character
        :
                                public Visitor,
                //                public Talkable, public Movable,
                public Askable, public Teachable, public Givable,
                public Killable {
public:

    static int childrenEnergy;
    static std::string genericName;
    static const std::string energyName;
    Character(size_t energy);
    Character(size_t energy, std::string name);
    Character(size_t energy, std::string name, Context goals);

    virtual Properties getTraits() override {
        return Killable::getTraits()
//                + Talkable::getTraits()
//                + Movable::getTraits()
                + Teachable::getTraits()
                + Askable::getTraits()
                + Givable::getTraits()
                ;
    }

    virtual Action action(Positions environment) override;
    virtual const std::string & getName() override;
    virtual void receiveDrawer(Drawer &drawer) override;

    // traits requirements
    virtual Knowledge & beAsked() override;
//    virtual std::pair<int, int> beMoved() override;
//    virtual std::string beTalked(std::string message) override;
    virtual void beGiven(std::shared_ptr<Entity> object) override;


    // visitor requirements
//    virtual void ask(Askable &askable) override;
    virtual size_t eat(size_t portions) override;
//    virtual void talk(Talkable &talkable) override;
//    virtual void teach(Teachable &teachable) override;
    virtual void give(Givable &givable) override;
    virtual void grab(Grabbable &grabbable) override;

    Context &getGoals() {
        if (not knowledge.get().hasGoals()) {
            knowledge.get().setGoals(Context());
        }
        return knowledge.goals().operator*();
    }

private:
    std::string name;
    Properties manageableTraits;

    Planner planner;
    Context previousSituation;
    std::vector<Context> previousSituations;
    std::shared_ptr<Change> previousChange;

    Context getDefaultGoals(const std::string &name) const;
    std::shared_ptr<Entity> getInventoryHelper();
    void setInventoryHelper(std::shared_ptr<Entity>);
//    bool arrangeAction(std::vector<Position> &vector, std::shared_ptr<Entity> entity,
//            Properties::Traits trait);

    VecEntity identifyConcepts(const Positions &environment,
            const SortedConcepts &sortedConcepts) const;

    void learnFromLastAction(const std::vector<Context> &previousSituations,
            PtrChange &previousChange);

    void ask(std::string who, Knowledge &otherKnowledge);

    void addInternalPerception(Context &context);

    Context analyzeSituation(Positions environment);
    void memorizeSituation(Context situation);
    void addExperience(std::vector<Context> &previousSituations);
    std::vector<std::shared_ptr<Change>> createPlan(Context situation);
    Action firstRemainingAction(const Positions &environment,
            const std::vector<PtrChange> &plannedChange);

    Action getNextSubAction(const Positions &environment);
    PtrChange getFirstSubChange(PtrChange &plannedChange);
    Action getEmptyAction();

    bool doingCompoundAction;
    size_t compoundChangeIndex;
    bool currentChangeIsFinished;


    std::vector<std::shared_ptr<Change>> plan;

    bool didWeLearnSomethingNew(const std::shared_ptr<Change> &change, const Context &before,
            const Context &after, std::shared_ptr<Change> &step) const;

    void insertOrReassure(const PtrChange &newChange);
};


#endif //EPISTOMATA_CHARACTER_H
