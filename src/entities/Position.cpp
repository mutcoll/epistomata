/**
 * @file Position.cpp
 * @author jmmut
 * @date 2016-09-30.
 */

#include "Position.h"

const std::string Position::genericName = "position";

const std::string &Position::getName() {
    return name;
}

void Position::receiveDrawer(Drawer &drawer) {
}

Action Position::action(std::vector<std::shared_ptr<Position>> environment) {
    return {Properties::NO_ACTION, {}};
}

void Position::prepareToBeWalked(std::shared_ptr<Entity> walker) {
    comingEntities.push_back(walker);
}

void Position::prepareDeparture(std::shared_ptr<Entity> walker) {
    leavingEntities.push_back(walker);
}

void Position::finishMovement() {
    entities.insert(entities.end(), comingEntities.begin(), comingEntities.end());
    comingEntities.clear();

    auto newEnd = std::remove_if(entities.begin(), entities.end(),
            [this](std::shared_ptr<Entity> elem) {
                return std::find(leavingEntities.begin(), leavingEntities.end(), elem)
                        != leavingEntities.end();
            });
    entities.erase(newEnd, entities.end());
    leavingEntities.clear();
}

bool Position::willBeRemoved(PtrEntity entity) {
    return std::find(leavingEntities.begin(), leavingEntities.end(), entity)
            != leavingEntities.end();
}

void Position::clear() {
    entities.clear();
    leavingEntities.clear();
    comingEntities.clear();
}


Position::EntityIterator::EntityIterator(Position &position)
        : EntityIterator(0, position) {
}

Position::EntityIterator::EntityIterator(size_t index, Position &position)
        : index(position.entities.begin() + index), position(position) {
}

std::shared_ptr<Entity> Position::EntityIterator::operator*() { return index.operator*(); }

bool Position::EntityIterator::operator!=(Position::EntityIterator it) {
    return position.entities.begin() != it.position.entities.begin() || index != it.index;
}

void Position::EntityIterator::operator++() {
    ++index;
}

Position &Position::EntityIterator::getPosition() {
    return position;
}

std::vector<std::shared_ptr<Entity>>::iterator Position::EntityIterator::getIterator() {
    return index;
}

