/**
 * @file Position.h
 * @author jmmut
 * @date 2016-09-30.
 */

#ifndef EPISTOMATA_POSITION_H
#define EPISTOMATA_POSITION_H

#include <vector>
#include <memory>
#include <algorithm>
#include "traits/Walkable.h"
#include "traits/Visitor.h"

class Position : public Walkable {
public:
    const static std::string genericName;

    Position() : name(genericName) {
    }

    Position(const std::string &name) : name(name) {
    }

    virtual ~Position() override = default;

    Action action(std::vector<std::shared_ptr<Position>> environment) override;
    const std::string &getName() override;
    void receiveDrawer(Drawer &drawer) override;

    /** call finish movement, or this won't have inmediate effect! */
    void prepareToBeWalked(std::shared_ptr<Entity> walker) override;
    void prepareDeparture(std::shared_ptr<Entity> walker) override;
    void finishMovement() override;
    bool willBeRemoved(PtrEntity entity);
    void clear();


    class EntityIterator {
    public:
        EntityIterator(Position &position);
        EntityIterator(size_t index, Position &position);

        void operator++();
        bool operator!=(EntityIterator it);
        std::shared_ptr<Entity> operator*();

        Position &getPosition();
        std::vector<std::shared_ptr<Entity>>::iterator getIterator();

    private:
        std::vector<std::shared_ptr<Entity>>::iterator index;
        Position &position;
    };

    EntityIterator begin() { return EntityIterator(*this); }
    EntityIterator end() { return EntityIterator(entities.size(), *this); }
    size_t entitiesCount() { return entities.size(); }
    std::shared_ptr<Entity> entity(size_t index) { return entities[index]; }

    VecEntity findByName(std::string name) {
        VecEntity found;
        for (auto &&entity : entities) {
            if (entity->getName() == name) {
                found.push_back(entity);
            }
        }
        return found;
    }

private:
    std::string name;
    std::vector<std::shared_ptr<Entity>> entities;
    std::vector<std::shared_ptr<Entity>> comingEntities;
    std::vector<std::shared_ptr<Entity>> leavingEntities;
};

using Positions = std::vector<std::shared_ptr<Position>>;

#endif //EPISTOMATA_POSITION_H
