/**
 * @file Food.h
 * @author jmmut
 * @date 2016-09-30.
 */

#ifndef EPISTOMATA_FOOD_H
#define EPISTOMATA_FOOD_H


#include <drawers/Drawer.h>
#include "traits/Edible.h"
#include "traits/Killable.h"

class Fruit :
        public Edible,
        public Killable {
public:
    static std::string genericName;

    Fruit(size_t portions);
    Fruit(size_t portions, const std::string &name);

    virtual Properties getTraits() override;

    virtual const std::string & getName() override;
    virtual void receiveDrawer(Drawer &drawer) override;

    void beEaten(size_t portions) override;

    void beAttacked(size_t damage) override;
    virtual std::vector<std::shared_ptr<Entity>> reincarnate() override;

private:
    std::string name;
};

#endif //EPISTOMATA_FOOD_H
