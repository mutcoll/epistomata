/**
 * @file Seed.cpp
 * @author jmmut
 * @date 2016-10-11.
 */

#include "Seed.h"
#include "Water.h"
#include "Tree.h"
#include "traits/Visitor.h"

const std::string Seed::genericName = "seed";

Seed::Seed(int energy) : Killable(energy), waterNeeded(1) {}

Properties Seed::getTraits() {
    return //Givable::getTraits() +
            Killable::getTraits();
}

Action Seed::action(std::vector<std::shared_ptr<Position>> environment) {
    return {Properties::NO_ACTION, {}};
}

const std::string & Seed::getName() {
    return genericName;
}

void Seed::receiveDrawer(Drawer &drawer) {
    drawer.draw(*this);
}

void Seed::beGiven(std::shared_ptr<Entity> object) {
    if (object->getName() == Water::genericName) {
        LOG_DEBUG("%s is given %s", getName().c_str(), object->getName().c_str());
        waterNeeded--;
    }
}

void Seed::beAttacked(size_t damage) {
    if (waterNeeded <= 0) {
        Killable::beAttacked(damage);
    }
}

std::vector<std::shared_ptr<Entity>> Seed::reincarnate() {
    int energy = 10;
    LOG_DEBUG("%s reincarnates", getName().c_str());
    return {std::make_shared<Tree>(energy)};
}
