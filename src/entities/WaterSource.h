/**
 * @file WaterSource.h
 * @author jmmut
 * @date 2016-10-11.
 */

#ifndef EPISTOMATA_WATERSOURCE_H
#define EPISTOMATA_WATERSOURCE_H


#include <traits/Grabbable.h>
#include <entities/Water.h>

class WaterSource : public Grabbable {
public:
    const static std::string genericName;

    virtual Properties getTraits() override { return Grabbable::getTraits(); }

    virtual const std::string & getName() override { return genericName; }
    virtual void receiveDrawer(Drawer &drawer) override;

    virtual std::shared_ptr<Entity> beGrabbed() override;


private:
    bool killed;
};


#endif //EPISTOMATA_WATERSOURCE_H
