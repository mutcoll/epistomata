/**
 * @file Water.h
 * @author jmmut
 * @date 2016-10-11.
 */

#ifndef EPISTOMATA_WATER_H
#define EPISTOMATA_WATER_H


#include <traits/Grabbable.h>
#include <traits/Killable.h>

class Water : public Killable {
public:
    const static std::string genericName;

    Water();

    virtual Properties getTraits() override;

    virtual const std::string & getName() override;
    virtual void receiveDrawer(Drawer &drawer) override;

    virtual std::vector<std::shared_ptr<Entity>> reincarnate() override;

private:
    bool killed;
};


#endif //EPISTOMATA_WATER_H
