/**
 * @file Character.cpp
 * @author jmmut
 * @date 2016-09-30.
 */

#include <iostream>
#include <log/log.h>
#include <utils/exception/StackTracedException.h>
#include "Character.h"
#include "Tree.h"
#include "WaterSource.h"
#include <io/TreeContextPrinter.h>
#include <utils/pathRanker/MostUsefulPathRanker.h>
#include <io/JsonContextPrinter.h>
#include <cassert>
#include <utils/test/TestSuite.h>


const std::string Character::energyName = "energy";
int Character::childrenEnergy = 100;
std::string Character::genericName = "character";

Character::Character(size_t energy) : Character(energy, genericName) {}

Character::Character(size_t energy, std::string name)
        : Character(energy, name, getDefaultGoals(name)) {
}

Character::Character(size_t energy, std::string name, Context goals)
        : Killable(energy),
        name(name),
        manageableTraits(Properties(Properties::EDIBLE)
//                + Properties::TALKABLE
//                + Properties::ASKABLE
//                + Properties::TEACHABLE
                + Properties::GIVABLE
                + Properties::GRABBABLE
        ),
        doingCompoundAction(false),
        compoundChangeIndex(0),
        currentChangeIsFinished(true) {
    knowledge.get().setGoals(goals);

    setInventoryHelper(nullptr);
//    setInventoryHelper(std::make_shared<Water>());
}

Context Character::getDefaultGoals(const std::string &name) const {
    Context context;
    context.subcontext(Concept(name)).concepts().addNode(Concept(energyName, 200));
    return context;
}

Action Character::action(Positions environment) {
    Context situation = analyzeSituation(environment);
    memorizeSituation(situation);
    if (currentChangeIsFinished) {
        addExperience(previousSituations);
        plan = createPlan(situation);
    }
    return firstRemainingAction(environment, plan);
}

Context Character::analyzeSituation(Positions environment) {
    environment.push_back(getInventory());  // this is copying the inventory! don't modify
    Context situation = Knowledge::analyze(environment);
    addInternalPerception(situation);
    return situation;
}

void Character::addInternalPerception(Context &context) {
    context.subcontext()[Concept(name)].concepts().addNode(Concept(energyName, energy));
    //    context.subcontext()[Concept(name)] += knowledge.get();
}

void Character::memorizeSituation(Context situation) {
    previousSituations.push_back(situation);
}

/**
 * this has to be called when a change was finished, compound or not
 * for each step
 *   compute the actual difference
 *   if the difference is not the same as predicted by the change
 *     reduce certainty
 *     create the actual change and store in a temporal list
 *   else 
 *     increase its certainty
 *   
 * if not all the changes were correct 
 *   create new compound change
 * else
 *   update accumulated (increased) certainty
 * @param previousSituations 
 */
void Character::addExperience(std::vector<Context> &previousSituations) {
    std::vector<PtrChange> newChanges;
    bool newChangesLearnt = false;
    if (previousChange != nullptr) {
        if (previousChange->trait == traits::names[Properties::COMPOUND]) {
            std::vector<PtrChange> &previousSteps = previousChange->steps;
            assert(previousSituations.size() == previousSteps.size() + 1);
            for (size_t i = 0; i < previousSteps.size(); ++i) {
                std::shared_ptr<Change> &change = previousSteps[i];
                const Context &before = previousSituations[i];
                const Context &after = previousSituations[i + 1];
                std::shared_ptr<Change> step;
                bool somethingNew = didWeLearnSomethingNew(change, before, after, step);
                newChangesLearnt = newChangesLearnt or somethingNew;
                assert(step != nullptr);
                newChanges.push_back(step);
            }
            if (newChangesLearnt) {
                auto newChange = Change::build(newChanges);
                insertOrReassure(newChange);
            }
            previousChange->updateCertainty();
        } else {
            assert(previousSituations.size() == 2);
            const Context &before = previousSituations[0];
            const Context &after = previousSituations[1];
            std::shared_ptr<Change> newChange;
            if (didWeLearnSomethingNew(previousChange, before, after, newChange)) {
                insertOrReassure(newChange);
            }
        }
        previousSituations.front() = previousSituations.back();
        previousSituations.resize(1);
    }
//    learnFromLastAction(previousSituations, previousChange);
    planner.doKnowledgeMaintenance(knowledge);
}

/**
 * even if we did A and resulted B, B might already be part of the knowledge
 */
void Character::insertOrReassure(const PtrChange &newChange) {
    Changes::PtrNodeWrapper inserted;
    bool wasANewChange = knowledge.get().changes().addNode(newChange, inserted);
    if (not wasANewChange) {
        inserted->node->certainty.updateWithASuccess();
        LOG_DEBUG("already knew of the actual change: %s", jsonPrint(*newChange).c_str());
    } else {
        LOG_DEBUG("learnt new change: %s", jsonPrint(*newChange).c_str());
    }
}

bool Character::didWeLearnSomethingNew(const std::shared_ptr<Change> &change, const Context &before,
        const Context &after, std::shared_ptr<Change> &step) const {
    Context actualDifference = after - before;
    bool predictionWorked = (change->difference - actualDifference).isEmpty();
    if (predictionWorked) {
        LOG_DEBUG("last action worked as expected");
        change->certainty.updateWithASuccess();
        step = copyChange(*change);
        return false;
    }

    LOG_DEBUG("last action result was unexpected")
    change->certainty.updateWithAFailure();
    double EXPERIENCED_CHANGE_INITIAL_CERTAINTY = 0.8;
    step = std::make_shared<Change>(
            change->trait,
            change->objects,
            actualDifference,
            EXPERIENCED_CHANGE_INITIAL_CERTAINTY,
            change->steps);
    return true;
}

void Character::learnFromLastAction(const std::vector<Context> &previousSituations,
        PtrChange &previousChange) {
    const Context &currentSituation = previousSituations.back();
    const Context &initialSituation = previousSituations.front();
    if (previousChange != nullptr) {
        const std::pair<bool, std::shared_ptr<Changes::NodeWrapper>> &found =
                knowledge.get().changes().findNode(previousChange);
        if (found.first) {
            Change &change = found.second->node.operator*();
            Context difference = currentSituation - initialSituation;
            bool equal = (change.difference - difference).isEmpty();
            //            Concepts difference = minus(currentSituation.concepts(), previousSituation.concepts());
            //            bool equal = isEmptyConcepts(minus(change.difference.concepts(), difference));
            if (equal) {
                change.certainty.updateWithASuccess();
            } else {
                change.certainty.updateWithAFailure();
                double EXPERIENCED_CHANGE_INITIAL_CERTAINTY = 0.8;
                //                Context differenceContext(std::make_shared<Concepts>(difference));
                //                std::shared_ptr<Change> newChange = std::make_shared<Change>(change.trait,
                //                        change.objects, differenceContext, EXPERIENCED_CHANGE_INITIAL_CERTAINTY);
                std::shared_ptr<Change> newChange = std::make_shared<Change>(
                        change.trait,
                        change.objects,
                        difference,
                        EXPERIENCED_CHANGE_INITIAL_CERTAINTY,
                        change.steps);
                
                Changes::PtrNodeWrapper inserted;
                bool wasANewChange = knowledge.get().changes().addNode(newChange, inserted);
                if (not wasANewChange) {
                    inserted->node->certainty.updateWithASuccess();
                }
            }
        } else {
            knowledge.get().changes().addNode(previousChange);
            LOG_DEBUG("trying to update change, but couldn't find it, so inserting change");
        }
    }
}

std::vector<std::shared_ptr<Change>> Character::createPlan(Context situation) {
    LOG_DEBUG("situation to plan with: %s", treePrint(situation.concepts()).c_str());
    GraphExplorer::Path plan = planner.plan(getName(), knowledge, situation, getGoals());
    if (plan.valid) {
        std::vector<std::shared_ptr<Change>> &plannedChanges = plan.edges;
        currentChangeIsFinished = plannedChanges[0]->trait != traits::names[Properties::COMPOUND];

        std::stringstream ss;
        JsonContextPrinter().printChanges(ss, plannedChanges, 0);
        LOG_INFO("decision: %s, %lu steps, %f certainty, %f accomplishment",
                plannedChanges[0]->trait.c_str(),
                plannedChanges.size(),
                plan.certainty,
                plan.accomplishment);

        LOG_DEBUG("full strategy: \n%s\nsummary: %s", ss.str().c_str(),
                changesTraitsToString(plannedChanges).c_str());

        return plannedChanges;
    }
    return {};
}

Action Character::firstRemainingAction(const Positions &environment,
        const std::vector<PtrChange> &plannedChanges) {
    Action action;
    if (doingCompoundAction) {
        action = getNextSubAction(environment);
    } else if (plannedChanges.size() > 0) {
        PtrChange plannedChange = plannedChanges[0];
        const std::pair<bool, std::shared_ptr<Changes::NodeWrapper>> &found =
                knowledge.get().changes().findNode(plannedChange);
        ASSERT_OR_THROW_MSG(found.first, "don't know about change " + jsonPrint(*plannedChange));
        previousChange = found.second->node;
        if (plannedChange->trait == traits::names[Properties::COMPOUND]) {
            plannedChange = getFirstSubChange(plannedChange);
        }
        action.trait = Properties::parse(plannedChange->trait);
        action.objects = identifyConcepts(environment, plannedChange->objects);
    } else {
        action = getEmptyAction();
    }
    return action;
}

Action Character::getNextSubAction(const Positions &environment) {
    ++compoundChangeIndex;
    PtrChange &subAction = previousChange->steps[compoundChangeIndex];
    Action action;
    action.trait = Properties::parse(subAction->trait);
    action.objects = identifyConcepts(environment, subAction->objects);
    if (compoundChangeIndex == previousChange->steps.size() - 1) {
        doingCompoundAction = false;
        currentChangeIsFinished = true;
    }
    return action;
}

PtrChange Character::getFirstSubChange(PtrChange &plannedChange) {
    doingCompoundAction = true;
    compoundChangeIndex = 0;
    plannedChange = plannedChange->steps[compoundChangeIndex];
    return plannedChange;
}

Action Character::getEmptyAction() {
    Action action;
    action.trait = Properties::NO_ACTION;
    Context empty;
    SortedConcepts noConcepts;
    previousChange = std::make_shared<Change>(action.trait, noConcepts, empty, 1.0);
    return action;
}

VecEntity Character::identifyConcepts(const Positions &environment,
        const SortedConcepts &sortedConcepts) const {
    VecEntity identifiedEntities;

    for (const Concept &concept : sortedConcepts) {
        for (std::shared_ptr<Position> position : environment) {
            const VecEntity found = position->findByName(concept.getName());
            identifiedEntities.insert(identifiedEntities.end(), found.begin(), found.end());
        }
    }

    auto end = std::unique(identifiedEntities.begin(), identifiedEntities.end(),
            [](std::shared_ptr<Entity> firstEntity, std::shared_ptr<Entity> secondEntity) {
                return firstEntity->getName() == secondEntity->getName();
            });

    return VecEntity(identifiedEntities.begin(), end);
}

const std::string & Character::getName() {
    return name;
}


void Character::receiveDrawer(Drawer &drawer) {
    drawer.draw(*this);
}

// ----------------- relationships with other entities ------------------

// ----------------- Trait requirements --------------------

Knowledge & Character::beAsked() {
    LOG_DEBUG("%s being asked", name.c_str())
    return knowledge;
}

//std::pair<int, int> Character::beMoved() {
//    LOG_DEBUG("%s being moved", name.c_str())
//    return {0, 0};
//}
//
//bool Character::beKilled() {
//    if (--energy == 0) {
//        LOG_DEBUG("%s being killed", name.c_str())
//    }
//    return energy == 0;
//}
//
//std::string Character::beTalked(std::string message) {
//
//    LOG_DEBUG("%s being talked", name.c_str())
//    std::cout << name << ": I heard:" << message << std::endl;
//
//    std::string response;
//    for (const Knowledge::ptrToConcept &lesson : knowledge) {
//        response += " " + lesson->node.name;
//    }
//    std::cout << name << ":" << response << std::endl;
//
//    return response;
//}
//
void Character::beGiven(std::shared_ptr<Entity> object) {
    setInventoryHelper(object);
}
//
// ----------------- Visitor requirements --------------------

void Character::ask(std::string who, Knowledge &otherKnowledge) {
    LOG_DEBUG("%s asks", name.c_str())

    // put him in graph memory
    Concept whoConcept(who);
    knowledge.learn(whoConcept);

    // put him as askable in the change memory
    //    Change change(Properties::ASKABLE, {}, buildContext({who}), {});
    //    knowledge.learn(change);

    // put under him his knowledge
    if (who != getName()) {
        // do this only if I'm not asking myself (asking myself each time in n iterations
        // would make my knowledge n times self-recursed)
        knowledge.knowsThat(whoConcept) += otherKnowledge.get();
    }

    // learning process stub: I learn what I learnt others know
    knowledge.get() += otherKnowledge.get();
}

size_t Character::eat(size_t portions) {
    size_t portionsEaten = 0;
    if (portions > 0) {
        LOG_DEBUG("%s eats", name.c_str())
        portionsEaten = 2;
        energy += portionsEaten;
    }
    return portionsEaten;
}
//
//void Character::talk(Talkable &talkable) {
//    LOG_DEBUG("%s talks", name.c_str())
//    std::string message = "knowledge:";
//
//    for (const Knowledge::ptrToConcept &lesson : knowledge) {
//        message += " " + lesson->node.name;
//    }
//    std::string response = talkable.beTalked(message);
//}
//
//void Character::teach(Teachable &teachable) {
//    LOG_DEBUG("%s teaches", name.c_str())
//    teachable.beTaught(knowledge);
//}

//
void Character::give(Givable &givable) {
    std::shared_ptr<Entity> carry = getInventoryHelper();
    if (carry != nullptr) {
        LOG_DEBUG("%s gives %s to %s",
                getName().c_str(), carry->getName().c_str(), givable.getName().c_str());
        setInventoryHelper(nullptr);
        givable.beGiven(carry);
    } else {
        LOG_DEBUG("%s has nothing to give", getName().c_str());
    }
}

void Character::grab(Grabbable &grabbable) {
    std::shared_ptr<Entity> grabbed = grabbable.beGrabbed();
    if (grabbed != nullptr) {
        LOG_DEBUG("%s grabs %s", getName().c_str(), grabbable.getName().c_str());
        setInventoryHelper(grabbed);
    } else {
        LOG_DEBUG("%s grabs nothing", getName().c_str());
    }
}

/**
 * returns a pointer to entity that represents the inventory. It may be null if the inventory is
 * empty. using this getter and the corresponding setter, grants that the position doesn't have
 * null elements.
 */
std::shared_ptr<Entity> Character::getInventoryHelper() {
    Position::EntityIterator iterator = getInventory()->begin();
    if (not (iterator != getInventory()->end())) {
        return nullptr;
    }

    return iterator.operator*();
}

void Character::setInventoryHelper(std::shared_ptr<Entity> object) {
    if (getInventory() == nullptr) {
        setInventory(std::make_shared<Position>(getName() + "_inventory"));
    }
    getInventory()->clear();
    if (object != nullptr) {
        getInventory()->prepareToBeWalked(object);
        getInventory()->finishMovement();
    }
}




