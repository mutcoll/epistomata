/**
 * @file Seed.h
 * @author jmmut
 * @date 2016-10-11.
 */

#ifndef EPISTOMATA_SEED_H
#define EPISTOMATA_SEED_H


#include <traits/Killable.h>
#include <traits/Givable.h>

class Seed : public Givable, public Killable {
public:
    static const std::string genericName;
    Seed(int energy);

    virtual Properties getTraits() override;

    Action action(std::vector<std::shared_ptr<Position>> environment) override;

    virtual const std::string & getName() override;
    virtual void receiveDrawer(Drawer &drawer) override;

    // trait requirements
    virtual void beGiven(std::shared_ptr<Entity> object) override;
    void beAttacked(size_t damage) override;

    virtual std::vector<std::shared_ptr<Entity>> reincarnate() override;

    // other
    bool hasWater() { return waterNeeded == 0; }
private:
    int waterNeeded;
};


#endif //EPISTOMATA_SEED_H
