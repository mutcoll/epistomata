/**
 * @file Book.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_BOOK_H
#define EPISTOMATA_BOOK_H


#include <traits/Askable.h>
#include <traits/Teachable.h>
#include <drawers/Drawer.h>

class Book : public Askable, public Teachable {
public:
    Book();
    Book(const std::string &name);

    virtual Properties getTraits() override;

    virtual const std::string & getName() override;
    virtual void receiveDrawer(Drawer &drawer) override;

    // traits requirements
    virtual Knowledge & beAsked() override;

private:
    std::string name;
};


#endif //EPISTOMATA_BOOK_H
