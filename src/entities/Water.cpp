/**
 * @file Water.cpp
 * @author jmmut
 * @date 2016-10-11.
 */

#include "Water.h"

const std::string Water::genericName = "water";

Water::Water() : Killable(1), killed(false) {}

Properties Water::getTraits() {
    return //Grabbable::getTraits() +
            Killable::getTraits();
}

const std::string & Water::getName() {
    return genericName;
}

void Water::receiveDrawer(Drawer &drawer) {
    drawer.draw(*this);
}

//std::shared_ptr<Entity> Water::beGrabbed() {
//    killed = true;
//    energy = 0;
//    LOG_DEBUG("%s is grabbed", getName().c_str());
//    return std::make_shared<Water>();
//}

std::vector<std::shared_ptr<Entity>> Water::reincarnate() {
    return {};
}

