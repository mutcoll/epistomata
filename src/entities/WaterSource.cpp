/**
 * @file WaterSource.cpp
 * @author jmmut
 * @date 2016-12-18.
 */

#include "WaterSource.h"
#include "drawers/Drawer.h"


const std::string WaterSource::genericName = "water source";


std::shared_ptr<Entity> WaterSource::beGrabbed() {
    return std::make_shared<Water>();
}

void WaterSource::receiveDrawer(Drawer &drawer) {
    drawer.draw(*this);
}
