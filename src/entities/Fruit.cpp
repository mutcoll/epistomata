/**
 * @file Food.cpp
 * @author jmmut
 * @date 2016-09-30.
 */

#include <iostream>
#include "Fruit.h"
#include "Seed.h"


std::string Fruit::genericName = "fruit";
const int SEED_LIFE = 3;

Fruit::Fruit(size_t portions) : Fruit(portions, genericName) {}
Fruit::Fruit(size_t portions, const std::string &name) : Edible(portions),
        Killable(portions), name(name) {}


const std::string & Fruit::getName() {
    return name;
}

void Fruit::receiveDrawer(Drawer &drawer) {
    drawer.draw(*this);
}

Properties Fruit::getTraits() {
    return Killable::getTraits()
             + Edible::getTraits()
            ;
}


std::vector<std::shared_ptr<Entity>> Fruit::reincarnate() {
    LOG_DEBUG("%s reincarnates", getName().c_str());
    return {std::make_shared<Seed>(SEED_LIFE)};
}

void Fruit::beAttacked(size_t damage) {
    // no action, you can only kill a fruit eating it
}

void Fruit::beEaten(size_t portions) {
    Edible::beEaten(portions);
    energy = this->portions;
}

