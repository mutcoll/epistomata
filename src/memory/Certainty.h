/**
 * @file Certainty.h
 * @author jmmut
 * @date 2017-02-27.
 */

#ifndef EPISTOMATA_CERTAINTY_H
#define EPISTOMATA_CERTAINTY_H


#include <cstddef>

class Certainty {
public:
    Certainty(double initialProbability);
    Certainty(size_t failures, size_t successes, double initialProbability);
    Certainty(const Certainty &other) = default;

    void updateWithAFailure();
    void updateWithASuccess();

    double get() const;
private:
    size_t failures, successes;
    double initialProbability;
};


#endif //EPISTOMATA_CERTAINTY_H
