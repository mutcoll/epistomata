/**
 * @file Context.cpp
 * @author jmmut
 * @date 2016-11-13.
 */

#include <algorithm>
#include "Context.h"
#include <io/ContextPrinter.h>
#include <io/DotContextPrinter.h>
#include <io/TreeContextPrinter.h>
#include <utils/test/TestSuite.h>
#include <io/JsonContextPrinter.h>
#include "Change.h"

namespace std {
size_t hash<shared_ptr<Change>>::operator()(const std::shared_ptr<Change> &ptrChange) const {
    return ptrChange->hash();
}
} // std

//bool changeLessThan(Changes::PtrNodeWrapper first, Changes::PtrNodeWrapper second) {
//    return first->node.operator*() < second->node.operator*();
//}

bool compareConcept(Concepts::PtrNodeWrapper first, Concepts::PtrNodeWrapper second, bool &equal) {
    bool lessThan;
    first->node.isSimilar(second->node, lessThan, equal);
    return lessThan;
}

bool compareChange(Changes::PtrNodeWrapper first, Changes::PtrNodeWrapper second, bool &equal) {
    bool lessThan;
    first->node.operator*().compare(second->node.operator*(), lessThan, equal);
    return lessThan;
}
bool compare(const PtrChange &first, const PtrChange &second, bool &equal) {
    bool lessThan;
    first->compare(second.operator*(), lessThan, equal);
    return lessThan;
}

Context::Context() : Context(std::make_shared<Concepts>(compareConcept)) {
}

Context::Context(const std::shared_ptr<Concepts> &concepts)
        : Context({}, concepts, std::make_shared<Changes>(), nullptr) {
}
Context::Context(std::shared_ptr<Concepts> &&concepts)
        : Context({}, std::move(concepts), std::make_shared<Changes>(), nullptr) {
}

Context::Context(const std::map<Concept, Context> &subcontext,
        const std::shared_ptr<Concepts> &concepts, const std::shared_ptr<Changes> &changes,
        const std::shared_ptr<Context> &goals)
        : subcontext_(subcontext), concepts_(copyConcepts(concepts)),
        changes_(copyChanges(changes)), goals_(copyGoals(goals)) {
}

Context::Context(std::map<Concept, Context> &&subcontext, std::shared_ptr<Concepts> &&concepts,
        std::shared_ptr<Changes> &&changes, std::shared_ptr<Context> &&goals)
        : subcontext_(std::move(subcontext)), concepts_(std::move(concepts)),
        changes_(std::move(changes)), goals_(std::move(goals)) {
}

Context::Context(const Context &other)
        : Context(other.subcontext_, other.concepts_, other.changes_, other.goals_) {
}

//Context::Context(Context &&other)
//        : subcontext_(std::move(other.subcontext_)), concepts_(std::move(other.concepts_)),
//        changes_(std::move(other.changes_)), goals_(std::move(other.goals_)) {
//}

std::shared_ptr<Concepts> Context::copyConcepts(const std::shared_ptr<Concepts> &concepts) const {
    if (concepts == nullptr) {
        throw randomize::utils::exception::StackTracedException("trying to copy nullptr concepts");
    }
    return std::make_shared<Concepts>(*concepts);
}

std::shared_ptr<Changes> Context::copyChanges(const std::shared_ptr<Changes> &changes) const {
    if (changes == nullptr) {
        throw randomize::utils::exception::StackTracedException("trying to copy nullptr changes");
    }
    return std::make_shared<Changes>(*changes);
}

std::shared_ptr<Context> Context::copyGoals(const std::shared_ptr<Context> &goals) const {
    return goals == nullptr ? nullptr : std::make_shared<Context>(*goals);
}

Context &Context::operator=(const Context &other) {
    subcontext_ = other.subcontext();
    concepts_ = copyConcepts(other.concepts_);
    changes_ = copyChanges(other.changes_);
    goals_ = copyGoals(other.goals_);
    return *this;
}

//Context &Context::operator+=(const Context &other) {
//    concepts().addGraph(other.concepts());
//    for (const Changes::PtrNodeWrapper &change : other.changes()) {
//        changes().addNode(copyChange(change->node.operator*()));
//    }
//     //TODO goals?
//    for (const std::pair<const Concept, Context> &that : other.subcontext_) {
//        subcontext()[that.first] += that.second;
//    }
//
//    return *this;
//}

Context &Context::operator+=(const Context &other) {
    for (const auto &otherSubcontextEntry : other.subcontext()) {
        const std::map<Concept, Context>::iterator &iterator = subcontext()
                .find(otherSubcontextEntry.first);
        if (iterator != subcontext().end()) {
            iterator->second += otherSubcontextEntry.second;
        } else {
            subcontext().insert(otherSubcontextEntry);
        }
    }

    for (const PtrChange &change : other.changes()) {
        changes().addNode(copyChange(change.operator*()));
    }

    concepts() = sum(concepts(), other.concepts());

    if (other.hasGoals()) {
        if (hasGoals()) {
            goals().operator*() += other.goals().operator*();
        } else {
            setGoals(other.goals().operator*());
        }
    }

    return *this;
}


Context Context::operator-() const {
    Context result;
    Concepts reversed(reverse(this->concepts()));
    std::swap(result.concepts(), reversed);
    return result;
}

Context Context::operator-(const Context &other) const {
    std::map<Concept, Context> newSubcontext(subcontext());
    for (const auto &otherSubcontextEntry : other.subcontext()) {
        const std::map<Concept, Context>::iterator &iterator = newSubcontext
                .find(otherSubcontextEntry.first);
        if (iterator != newSubcontext.end()) {
            iterator->second = iterator->second - otherSubcontextEntry.second;
        } else {
            Context otherSubSubContext(-otherSubcontextEntry.second);
            if (otherSubSubContext.size() > 0) {
                newSubcontext.emplace(otherSubcontextEntry.first, otherSubSubContext);
            }
        }
    }
    Changes newChanges(changes() - other.changes());
    Concepts newConcepts(minus(concepts(), other.concepts()));


    std::shared_ptr<Context> newGoals;
    if (other.hasGoals()) {
        if (hasGoals()) {
            newGoals = std::make_shared<Context>(goals().operator*() - other.goals().operator*());
        } else {
            newGoals = std::make_shared<Context>(-other.goals().operator*());
        }
    } else {
        newGoals = copyGoals(goals());
    }

    return Context(std::move(newSubcontext), std::make_shared<Concepts>(newConcepts),
            std::make_shared<Changes>(newChanges), std::move(newGoals));
}

Context Context::sumIfPositive(const Context &other, bool &positive) const {
    bool stopped;
    Concepts newConcepts(sumOrStop(concepts(), other.concepts(), stopped));
    positive = not stopped;
    if (stopped) {
        return Context();
    }

    return continueSum(other, std::move(newConcepts));
}

Context Context::continueSum(const Context &other, Concepts &&newConcepts) const {
    std::map<Concept, Context> newSubcontext(subcontext());
    for (const auto &otherSubcontextEntry : other.subcontext()) {
        const std::map<Concept, Context>::iterator &iterator =
                newSubcontext.find(otherSubcontextEntry.first);
        if (iterator != newSubcontext.end()) {
            iterator->second += otherSubcontextEntry.second;
        } else {
            newSubcontext.emplace(otherSubcontextEntry.first, otherSubcontextEntry.second);
        }
    }
    Changes newChanges(changes());
    newChanges.addGraph(other.changes());

    std::shared_ptr<Context> newGoals;
    if (other.hasGoals()) {
        if (hasGoals()) {
            newGoals = std::make_shared<Context>(goals().operator*() + other.goals().operator*());
        } else {
            newGoals = std::make_shared<Context>(other.goals().operator*());
        }
    } else {
        newGoals = copyGoals(goals());
    }

    return Context(std::move(newSubcontext), std::make_shared<Concepts>(std::move(newConcepts)),
            std::make_shared<Changes>(std::move(newChanges)), move(newGoals));
}

Context Context::operator+(const Context &other) const {
    Concepts newConcepts(sum(concepts(), other.concepts()));
    return continueSum(other, std::move(newConcepts));
}

Context Context::operator^(const Context &other) const {
    return *this - (*this - other);
}

void Context::compare(const Context &other, bool &equal, bool &lessThan) const {
    if (this->size() < other.size()) {
        equal = false;
        lessThan = true;
    } else if (this->size() > other.size()) {
        equal = false;
        lessThan = false;
    } else { // they have the same size
        lexicographicallyCompare(other, equal, lessThan);
    }
}
template<typename T>
void compareSizes(const T &first, const T &second, bool &equal, bool &lessThan)  {
    lessThan = first.size() < second.size();
    equal = first.size() == second.size();
}

void Context::lexicographicallyCompare(const Context &other, bool &equal, bool &lessThan) const {
    compareSizes(this->concepts(), other.concepts(), equal, lessThan);
    if (not equal) {
        return;
    } else {
        lexicographicallyCompareConcepts(other, equal, lessThan);
        if (not equal) {
            return;
        }
    }

    compareSizes(this->changes(), other.changes(), equal, lessThan);
    if (not equal) {
        return;
    } else {
        lexicographicallyCompareChanges(other, equal, lessThan);
        if (not equal) {
            return;
        }
    }

    compareSizes(this->subcontext(), other.subcontext(), equal, lessThan);
    if (not equal) {
        return;
    } else {
        lexicographicallyCompareSubcontext(other, equal, lessThan);
        if (not equal) {
            return;
        }
    }

    lexicographicallyCompareGoals(other, equal, lessThan);
}

void Context::lexicographicallyCompareConcepts(
        const Context &other, bool &equal, bool &lessThan) const {
    ASSERT_EQUALS_OR_THROW_MSG(concepts().size(), other.concepts().size(), "precondition violated");
    Concepts::iterator iterThis = concepts().begin();
    Concepts::iterator iterOther = other.concepts().begin();
    equal = true;
    lessThan = false;
    bool remaining = iterThis != concepts().end();
    while (equal and remaining) {
        Concept &thisConcept = iterThis.operator*();
        Concept &otherConcept = iterOther.operator*();
        thisConcept.isIdentical(otherConcept, lessThan, equal);
        ++iterThis;
        ++iterOther;
        remaining = iterThis != concepts().end();
    }
}

void Context::lexicographicallyCompareChanges(
        const Context &other, bool &equal, bool &lessThan) const {
    ASSERT_EQUALS_OR_THROW_MSG(changes().size(), other.changes().size(), "precondition violated");
    Changes::iterator iterThis = changes().begin();
    Changes::iterator iterOther = other.changes().begin();
    equal = true;
    lessThan = false;
    bool remaining = iterThis != changes().end();
    while (equal and remaining) {
        Change &thisChange = iterThis.operator*().operator*();
        Change &otherChange = iterOther.operator*().operator*();
        thisChange.compare(otherChange, lessThan, equal);
        ++iterThis;
        ++iterOther;
        remaining = iterThis != changes().end();
    }
}

void Context::lexicographicallyCompareSubcontext(
        const Context &other, bool &equal, bool &lessThan) const {
    ASSERT_EQUALS_OR_THROW_MSG(subcontext().size(), other.subcontext().size(), "precondition violated");
    auto iterThis = subcontext().begin();
    auto iterOther = other.subcontext().begin();
    equal = true;
    lessThan = false;
    bool remaining = iterThis != subcontext().end();
    while (equal and remaining) {
        const Concept thisConcept = iterThis.operator*().first;
        const Concept otherConcept = iterOther.operator*().first;
        thisConcept.isIdentical(otherConcept, lessThan, equal);
        ++iterThis;
        ++iterOther;
        remaining = iterThis != subcontext().end();
    }

    iterThis = subcontext().begin();
    iterOther = other.subcontext().begin();
    equal = true;
    lessThan = false;
    remaining = iterThis != subcontext().end();
    while (equal and remaining) {
        Context thisSubcontext = iterThis.operator*().second;
        Context otherSubcontext = iterOther.operator*().second;
        thisSubcontext.compare(otherSubcontext, equal, lessThan);
        ++iterThis;
        ++iterOther;
        remaining = iterThis != subcontext().end();
    }
}

void Context::lexicographicallyCompareGoals(
        const Context &other, bool &equal, bool &lessThan) const {
    if (hasGoals() and other.hasGoals()) {
        goals()->lexicographicallyCompare(other.goals().operator*(), equal, lessThan);
    } else {
        lessThan = other.hasGoals() ;
        equal = hasGoals() == other.hasGoals();
    }
}


bool Context::isEmpty() const {
    bool conceptsEmpty = true;
    if (concepts().size() != 0) {
        for (const auto &concept : concepts()) {
            if (concept.getCount() != 0) {
                conceptsEmpty = false;
                break;
            }
        }
    }
    bool changesEmpty = changes().size() == 0;
    bool goalsEmpty = not hasGoals();

    bool subcontextEmpty = true;
    for(auto subcontextEntry: subcontext()) {
        if (not subcontextEntry.second.isEmpty()){
            subcontextEmpty = false;
            break;
        }
    }
    return conceptsEmpty and changesEmpty and goalsEmpty and subcontextEmpty;
}

size_t Context::size() const {
    size_t size = 0;
    if (hasGoals()) {
        size += goals()->size();
    }
    for (const auto &item : subcontext()) {
        size += item.second.size();
    }
    return size + concepts().size() + changes().size();
}

size_t Context::deepSize() const {
    size_t size = 0;
    if (hasGoals()) {
        size += goals()->size();
    }
    for (const auto &entry : subcontext()) {
        size += entry.second.size();
    }
    return size + concepts().size() + changes().size();
}

std::string Context::list() const {
    std::string contents = "concepts: ";
    for (const Concept &node : concepts()) {
        contents += node.getName() + ", ";
    }
    contents += "\nchanges: ";
    for (const PtrChange &node : changes()) {
        contents += node->trait + ", ";
    }
    contents += "\nsubcontext: ";
    for (auto entry : subcontext()) {
        contents += entry.first.getName() + ", ";
    }
    return contents;
}

std::string Context::stringTreePrint() const {
    TreeContextPrinter printer;
    return this->stringPrint(printer);
}
std::string Context::stringJsonPrint() const {
    JsonContextPrinter printer;
    return this->stringPrint(printer);
}
std::string Context::stringDotPrint() const {
    DotContextPrinter printer;
    return this->stringPrint(printer);
}

std::string Context::stringPrint(ContextPrinter &printer) const {
    std::stringstream ss;
    this->print(ss, printer);
    return ss.str();
}
std::ostream &Context::treePrint(std::ostream &out) const {
    TreeContextPrinter printer;
    this->print(out, printer);
    return out;
}

std::ostream &Context::dotPrint(std::ostream &out) const {
    DotContextPrinter printer;
    this->print(out, printer);
    return out;
}

std::ostream &Context::print(std::ostream &out) const {
    return dotPrint(out);
}

std::ostream &Context::print(std::ostream &out, ContextPrinter &printer) const {
    printer.print(out, *this);
    return out;
}

bool Context::isSubsetOf(const Context &other) const {
    bool conceptsAreSubset = conceptsAreSubsetOf(concepts(), other.concepts());
    if (not conceptsAreSubset) {
        return false;
    }
    bool changesAreSubset = (other.changes() ^ changes()).size() == changes().size();
    if (not changesAreSubset) {
        return false;
    }
    bool subcontextIsSubset = true;
    for (const auto &subcontextPair : subcontext()) {
        const Concept &subcontextName = subcontextPair.first;
        const Context &ourSubcontext = subcontextPair.second;

        const auto &iterator = other.subcontext().find(subcontextName);
        bool contains = iterator != other.subcontext().end();
        subcontextIsSubset = subcontextIsSubset
                and contains and ourSubcontext.isSubsetOf(iterator->second);

        if (not subcontextIsSubset) {
            return false;
        }
    }

    bool goalsAreSubset;
    if (hasGoals()) {
        if (other.hasGoals()) {
            goalsAreSubset = goals()->isSubsetOf(other.goals().operator*());
        } else {
            goalsAreSubset = goals()->size() == 0;
        }
    } else {
        goalsAreSubset = true;
    }
    return goalsAreSubset;
}

size_t Context::countConnections() const {
    size_t count = 0;
    for (const auto &subcontextEntry : subcontext()) {
        count += subcontextEntry.second.countConnections();
    }
    if (hasGoals()) {
        count += goals()->countConnections();
    }
    return count + concepts().countConnections() + changes().countConnections();
}

Concepts joinOrStop(const Concepts &concepts, const Concepts &otherConcepts,
        std::function<Concept(const Concept &first, const Concept &second, bool &stopped)> joinFunction,
        std::function<Concept(const Concept &first, bool &stopped)> onlyInConcepts,
        std::function<Concept(const Concept &first, bool &stopped)> onlyInOtherConcepts,
        bool &stopped) {
    auto thisBegin = concepts.begin();
    auto thisEnd = concepts.end();
    auto otherBegin = otherConcepts.begin();
    auto otherEnd = otherConcepts.end();
    Concepts result;
    bool equal, lessThan;
    while (thisBegin != thisEnd && otherBegin != otherEnd) {
        thisBegin.operator*().isSimilar(*otherBegin, lessThan, equal);
        if (lessThan) {
            // present in this, not in other: add
            auto && node = onlyInConcepts(thisBegin.operator*(), stopped);
            if (stopped) {
                return Concepts();
            }
            result.addNode(node);
            ++thisBegin;
        } else if (not equal) {
            // present in other, not in this: add
            auto &&node = onlyInOtherConcepts(otherBegin.operator*(), stopped);
            if (stopped) {
                return Concepts();
            }
            result.addNode(node);
            ++otherBegin;
        } else {
            // present in both this and other: join
            auto &&node = joinFunction(thisBegin.operator*(), otherBegin.operator*(), stopped);
            if (stopped) {
                return Concepts();
            }
            result.addNode(node);
            ++thisBegin;
            ++otherBegin;
        }
    }

    while (thisBegin != thisEnd) {
        auto && node = onlyInConcepts(thisBegin.operator*(), stopped);
        if (stopped) {
            return Concepts();
        }
        result.addNode(node);
        ++thisBegin;
    }

    while (otherBegin != otherEnd) {
        auto &&node = onlyInOtherConcepts(otherBegin.operator*(), stopped);
        if (stopped) {
            return Concepts();
        }
        result.addNode(node);
        ++otherBegin;
    }
    return result;
}

Concepts join(const Concepts &concepts, const Concepts &otherConcepts,
        std::function<Concept(const Concept &first, const Concept &second)> joinFunction,
        std::function<Concept(const Concept &first)> onlyInConcepts,
        std::function<Concept(const Concept &first)> onlyInOtherConcepts) {
    auto thisBegin = concepts.begin();
    auto thisEnd = concepts.end();
    auto otherBegin = otherConcepts.begin();
    auto otherEnd = otherConcepts.end();
    Concepts result;
    bool equal, lessThan;
    while (thisBegin != thisEnd && otherBegin != otherEnd) {
        thisBegin.operator*().isSimilar(*otherBegin, lessThan, equal);
        if (lessThan) {
            // present in this, not in other: add
            result.addNode(onlyInConcepts(thisBegin.operator*()));
            ++thisBegin;
        } else if (not equal) {
            // present in other, not in this: add
            result.addNode(onlyInOtherConcepts(otherBegin.operator*()));
            ++otherBegin;
        } else {
            // present in both this and other: join
            result.addNode(joinFunction(thisBegin.operator*(), otherBegin.operator*()));
            ++thisBegin;
            ++otherBegin;
        }
    }

    while (thisBegin != thisEnd) {
        result.addNode(onlyInConcepts(thisBegin.operator*()));
        ++thisBegin;
    }

    while (otherBegin != otherEnd) {
        result.addNode(onlyInOtherConcepts(otherBegin.operator*()));
        ++otherBegin;
    }
    return result;
}

bool analyze(const Concepts &first, const SortedConcepts &second,
        std::function<bool(const Concept &first, const Concept &second)> joinFunction,
        std::function<bool(const Concept &first)> onlyInFirst,
        std::function<bool(const Concept &second)> onlyInSecond) {

    SortedConcepts sameOrderConcepts(second.begin(), second.end());
    std::sort(sameOrderConcepts.begin(), sameOrderConcepts.end(),
            [](const Concept &first, const Concept &second) {
                bool equal, lessThan;
                first.isSimilar(second, lessThan, equal);
                return lessThan;
            });

    auto thisBegin = first.begin();
    auto thisEnd = first.end();
    auto otherBegin = sameOrderConcepts.begin();
    auto otherEnd = sameOrderConcepts.end();
    Concepts result;
    bool equal;

    while (thisBegin != thisEnd && otherBegin != otherEnd) {
        bool lessThan;
        thisBegin.operator*().isSimilar(otherBegin.operator*(), lessThan, equal);
        if (lessThan) {
            // present in this, not in other
            if (onlyInFirst(thisBegin.operator*())) {
                return true;
            }
            ++thisBegin;
        } else if (not equal) {
            // present in other, not in this
            if (onlyInSecond(otherBegin.operator*())) {
                return true;
            }
            ++otherBegin;
        } else {
            // present in both this and other: join
            if (joinFunction(thisBegin.operator*(), otherBegin.operator*())) {
                return true;
            }
            ++thisBegin;
            ++otherBegin;
        }
    }

    while (thisBegin != thisEnd) {
        if (onlyInFirst(thisBegin.operator*())) {
            return true;
        }
        ++thisBegin;
    }

    while (otherBegin != otherEnd) {
        if (onlyInSecond(otherBegin.operator*())) {
            return true;
        }
        ++otherBegin;
    }
    return false;
}


bool isPositiveConcepts(const Concepts &concepts) {
    for (const auto & concept : concepts) {
        if (not concept.isPositive()) {
            return false;
        }
    }
    return true;
}
bool isStrictlyPositiveConcepts(const Concepts &concepts) {
    for (const auto & concept : concepts) {
        if (not concept.isStrictlyPositive()) {
            return false;
        }
    }
    return true;
}
bool isEmptyConcepts(const Concepts &concepts) {
    for (const Concept & concept : concepts) {
        if (concept.getCount() != 0) {
            return false;
        }
    }
    return true;
}

bool conceptsAreSubsetOf(const Concepts &concepts, const Concepts &otherConcepts) {
    return isPositiveConcepts(minus(otherConcepts, concepts));
}

bool conceptsAreSubsetOf(const SortedConcepts &subset, const Concepts &set) {
    auto stopIfSetConceptIsLessThanSubsetConcept = [](
            const Concept &setConcept, const Concept &subsetConcept){
        return (subsetConcept - setConcept).isStrictlyPositive();
    };
    auto dontStopForSetConcepts = [](const Concept &setConcept){
        return false;
    };
    auto stopIfSubsetConceptIsNotInSet = [](const Concept &subsetConcept){
        return subsetConcept.isPositive();
    };
    bool stopped = analyze(set, subset,
            stopIfSetConceptIsLessThanSubsetConcept,
            dontStopForSetConcepts,
            stopIfSubsetConceptIsNotInSet);
    return not stopped;
}

Concepts sum(const Concepts &concepts, const Concepts &otherConcepts) {
    return join(concepts, otherConcepts,
            std::plus<Concept>(),
            std::_Identity<Concept>(),
            std::_Identity<Concept>()
    );
}

Concepts sumOrStop(const Concepts &concepts, const Concepts &otherConcepts, bool &generalStopped) {
    return joinOrStop(concepts, otherConcepts,
            [](const Concept &first, const Concept &second, bool &stopped) {
                auto temp(first + second);
                stopped = not temp.isPositive();
                return temp;
            },
            [](const Concept &first, bool &stopped) {
                stopped = not first.isPositive();
                return first;
            },
            [](const Concept &second, bool &stopped) {
                stopped = not second.isPositive();
                return second;
            },
            generalStopped
    );
}

Concepts minus(const Concepts &concepts, const Concepts &otherConcepts) {
    return join(
            concepts,
            otherConcepts,
            std::minus<Concept>(),
            std::_Identity<Concept>(),
            std::negate<Concept>()
    );
}

bool Context::isPositive() const {
    // TODO changes? subcontext?
    return isPositiveConcepts(concepts());
}

double Context::fulfillmentBy(const Context &other) const {
    size_t count = concepts().size();
    double sum = 0;
    for (const auto &concept : concepts()) {
        if (concept.getCount() > 0) {
            const std::pair<bool, Concepts::PtrNodeWrapper> &found = other.concepts().findNode(
                    concept);
            if (found.first) {
                sum += found.second->node.getCount() / concept.getCount();
            }
        }
    }
    for (auto &&subcontextEntry : subcontext()) {
        ++count;
        const auto &foundInOther = other.subcontext().find(subcontextEntry.first);
        if (foundInOther != other.subcontext().end()) {
            sum += subcontextEntry.second.fulfillmentBy(foundInOther->second);
        }
    }
    return sum / count;
}

size_t Context::hash() const {
    size_t hashed = 0;
    for (const Concept &concept : concepts()) {
        hashed = hashed * 31 + concept.hash();
    }

    for (const PtrChange &change : changes()) {
        hashed = hashed * 31 + change->hash();
    }

    for (const std::pair<const Concept, Context> &aSubcontext : subcontext()) {
        hashed = hashed * 31 + aSubcontext.first.hash();
        hashed = hashed * 31 + aSubcontext.second.hash();
    }

    if (hasGoals()) {
        hashed = hashed * 31 + goals()->hash();
    }

    return hashed;
}


Concepts reverse(const Concepts &concepts) {
    Concepts result;
    for (const Concept &concept : concepts) {
        result.addNode(-concept);
    }
    return result;
}


std::ostream &operator<<(std::ostream &out, Context &memory) {
    return memory.treePrint(out);
}
