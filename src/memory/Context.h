/**
 * @file Context.h
 * @author jmmut
 * @date 2016-10-22.
 */

#ifndef MIKAI_RECURSIVEMEMORY_H
#define MIKAI_RECURSIVEMEMORY_H


#include <sstream>
#include <map>
#include <algorithm>
#include <memory>
#include "utils/Digraset2.h"
#include "utils/Digrunset.h"
#include "log/log.h"
#include "Concept.h"

//template<typename Entity, typename Memory>
//struct Context {
//    Memory<Entity> knows;
//    std::map<Entity, Context<Entity, Memory>> subcontext;
//};

//struct Context;
class Change;
class ContextPrinter;

using PtrChange = std::shared_ptr<Change>;
using WptrChange = std::weak_ptr<Change>;
using SortedConcepts = std::vector<Concept>;

namespace std {
template<>
struct hash<PtrChange> {
    size_t operator()(const PtrChange &ptrChange) const;
};
}

using Concepts = randomize::utils::graph::Digraset<Concept>;
using Changes = randomize::utils::graph::Digrunset<PtrChange>;

class Context {
public:
    Context();

    Context(const std::shared_ptr<Concepts> &concepts);
    Context(std::shared_ptr<Concepts> &&concepts);

    Context(const std::map<Concept, Context> &subcontext, const std::shared_ptr<Concepts> &concepts,
            const std::shared_ptr<Changes> &changes, const std::shared_ptr<Context> &goals);

    Context(std::map<Concept, Context> &&subcontext, std::shared_ptr<Concepts> &&concepts,
            std::shared_ptr<Changes> &&changes, std::shared_ptr<Context> &&goals);

    Context(const Context &other);
    Context(Context &&other) = default;
    Context &operator=(const Context &other);
    Context &operator=(Context &&other) = default;

    Concepts &concepts() const {
        return *concepts_;
    }
    Concepts &concepts() {
        return *concepts_;
    }

    const Changes &changes() const {
        return *changes_;
    }
    Changes &changes() {
        return *changes_;
    }

    const std::shared_ptr<Context> goals() const {
        return goals_;
    }

    void setGoals(Context other) {
        goals_ = std::make_shared<Context>(other);
    }

    bool hasGoals() const {
        return goals_ != nullptr;
    }

    const std::map<Concept, Context> &subcontext() const {
        return subcontext_;
    }
    std::map<Concept, Context> &subcontext() {
        return subcontext_;
    }

    Context &subcontext(const Concept &who) {
        return subcontext_[who];
    }

    Context sumIfPositive(const Context &other, bool &positive) const;
    Context &operator+=(const Context &other);
    Context operator-() const;
    Context operator+(const Context &other) const;
    Context operator-(const Context &other) const;

    /** similar to set intersection */
    Context operator^(const Context &other) const;

    double fulfillmentBy(const Context &other) const;
    bool isSubsetOf(const Context &other) const;
//    bool isSupersetOf(const Context &other) const {
//        return (other - *this).isEmpty();
//    }

//    bool isStrictSubsetOf(const Context &other) const {
//        return this->isSubsetOf(other) ?
//                not this->isSupersetOf(other)
//                : false;
//    }

    bool isPositive() const;

    void compare(const Context &other, bool &equal, bool &lessThan) const;
    size_t hash() const;
    bool isEmpty() const;
    size_t size() const;
    size_t deepSize() const;
    size_t countConnections() const;

    std::string list() const;
    std::string stringJsonPrint() const;
    std::string stringTreePrint() const;
    std::string stringDotPrint() const;
    std::string stringPrint(ContextPrinter &printer) const;
    std::ostream &print(std::ostream &out, ContextPrinter &printer) const;
    std::ostream &print(std::ostream &out) const;
    std::ostream &dotPrint(std::ostream &out) const;
    std::ostream &treePrint(std::ostream &out) const;

private:
    std::map<Concept, Context> subcontext_;
    std::shared_ptr<Concepts> concepts_;
    std::shared_ptr<Changes> changes_;
    std::shared_ptr<Context> goals_;

    void lexicographicallyCompare(const Context &other, bool &equal, bool &lessThan) const;
    void lexicographicallyCompareConcepts(const Context &other, bool &equal, bool &lessThan) const;
    void lexicographicallyCompareChanges(const Context &context, bool &equal, bool &than) const;
    void lexicographicallyCompareSubcontext(const Context &context, bool &equal, bool &than) const;
    void lexicographicallyCompareGoals(const Context &context, bool &equal, bool &lessThan) const;

    std::shared_ptr<Context> copyGoals(const std::shared_ptr<Context> &goals) const;
    std::shared_ptr<Changes> copyChanges(const std::shared_ptr<Changes> &changes) const;
    std::shared_ptr<Concepts> copyConcepts(const std::shared_ptr<Concepts> &concepts) const;

    Context continueSum(const Context &other, Concepts &&newConcepts) const;
};

std::ostream &operator<<(std::ostream& out, Context &memory);

bool compareConcept(Concepts::PtrNodeWrapper first, Concepts::PtrNodeWrapper second, bool &equal);

bool compareChange(Changes::PtrNodeWrapper first, Changes::PtrNodeWrapper second, bool &equal);

bool conceptsAreSubsetOf(const Concepts &concepts, const Concepts &otherConcepts);
bool conceptsAreSubsetOf(const SortedConcepts &subset, const Concepts &set);

Concepts minus(const Concepts &concepts, const Concepts &otherConcepts);
Concepts minus(const Concepts &concepts, const SortedConcepts &sortedConcepts);
Concepts reverse(const Concepts &concepts);
Concepts sumOrStop(const Concepts &concepts, const Concepts &otherConcepts, bool &stopped);
Concepts sum(const Concepts &concepts, const Concepts &otherConcepts);
bool isPositiveConcepts(const Concepts &concepts);
bool isStrictlyPositiveConcepts(const Concepts &concepts);
bool isEmptyConcepts(const Concepts &concepts);


#endif //MIKAI_RECURSIVEMEMORY_H
