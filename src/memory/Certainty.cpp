/**
 * @file Certainty.cpp
 * @author jmmut
 * @date 2017-02-27.
 */

#include "Certainty.h"

Certainty::Certainty(double initialProbability)
        : failures(0), successes(0), initialProbability(initialProbability) {
}

Certainty::Certainty(size_t failures, size_t successes, double initialProbability)
        : failures(failures), successes(successes), initialProbability(initialProbability) {
}

void Certainty::updateWithAFailure() {
    failures++;
}

void Certainty::updateWithASuccess() {
    successes++;
}

double Certainty::get() const {
    size_t tries = failures + successes + 1;
    return (initialProbability + successes ) / tries;
}
