/**
 * @file Planner.h
 * @author jmmut
 * @date 2016-11-27.
 */

#ifndef EPISTOMATA_PLANNER_H
#define EPISTOMATA_PLANNER_H


#include <vector>
#include <entities/Position.h>
#include <memory/Concept.h>
#include <utils/graphExplorer/GraphExplorer.h>
#include "Knowledge.h"


class Planner {
public:
    GraphExplorer::Path plan(std::string agent, Knowledge &knowledge,
            Context context, const Context &goal);

    void doKnowledgeMaintenance(Knowledge &knowledge);

private:
    int maxRecursionLevel = 10;
};


#endif //EPISTOMATA_PLANNER_H
