/**
 * @file Knowledge.cpp
 * @author jmmut
 * @date 2016-09-30.
 */

#include <entities/Position.h>
#include "memory/Knowledge.h"

Knowledge::Knowledge() {}

Knowledge::Knowledge(const Context &me) : me(me) {}

void Knowledge::learn(Change change) {
    me.changes().addNode(copyChange(change));
}

Concept Knowledge::learn(Concept &concept) {
    me.concepts().addNode(concept);
    return concept;
}

//Context buildContext(const std::string &entityName) {
//    return buildContext(std::vector<std::string>{entityName});
//}


Context buildContext(std::initializer_list<std::string> entities) {
    std::shared_ptr<Concepts> concepts = std::make_shared<Concepts>(compareConcept);
    for (const std::string &entity : entities) {
        concepts->addNode(Concept(entity));
    }
    Context context(concepts);
    return context;
}

SortedConcepts buildSortedConcepts(std::initializer_list<std::string> entities) {
    SortedConcepts sortedConcepts;
    for (const auto &entityName : entities) {
        sortedConcepts.emplace_back(entityName);
    }
    return sortedConcepts;
}

Context buildContext(const std::vector<std::string> &entities) {
    std::shared_ptr<Concepts> concepts = std::make_shared<Concepts>();
    for (const std::string &entity : entities) {
        concepts->addNode(Concept(entity));
    }
    Context context(concepts);
    return context;
}

Change buildMovement(std::string who, std::string from, std::string to,
        std::initializer_list<std::string> disappear, std::initializer_list<std::string> appear) {

    std::vector<std::string> disappearVector;
    disappearVector.assign(disappear);
    disappearVector.push_back(to);

    std::vector<std::string> appearVector;
    appearVector.assign(appear);
    appearVector.push_back(from);

    return Change(Properties::MOVABLE, buildSortedConcepts({who, to}),
            buildContext(appearVector) - buildContext(disappearVector), 1);
}

Context Knowledge::analyze(Positions actualSituation) {
    Context rm;
    for (auto &position : actualSituation) {
        for (auto &&entity : *position) {
            Concept oneInstance(entity->getName());
            const std::pair<bool, Concepts::PtrNodeWrapper> &found =
                    rm.concepts().findNode(oneInstance);
            if (found.first) {
                Concept instancesCount(oneInstance + found.second->node);
                rm.concepts().removeNode(found.second);
                rm.concepts().addNode(instancesCount);
            } else {
                rm.concepts().addNode(oneInstance);
            }
        }
    }
    return rm;
}

Change Knowledge::refineChange(Change expected, Change actual) {
    throw randomize::utils::exception::StackTracedException("unimplemented");
}
