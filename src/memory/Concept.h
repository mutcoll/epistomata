/**
 * @file Concept.h
 * @author jmmut
 * @date 2016-10-26.
 */

#ifndef EPISTOMATA_CONCEPT_H
#define EPISTOMATA_CONCEPT_H


#include <traits/Properties.h>
#include <memory/Certainty.h>

static const double EPSILON = 0.00001;

class Concept {
public:
    enum class Type { INSTANCE, PROPERTIES };

    explicit Concept(const std::string &name, double count = 1);
    explicit Concept(const Properties::Traits &trait, double count = 1);

    const std::string &getName() const;
    Properties::Traits getTrait() const;
    Type getType() const;
    double getCount() const;

    bool operator<(const Concept &other) const;
    void isSimilar(const Concept &other, bool &lessThan, bool &equal) const;
    void isIdentical(const Concept &other, bool &lessThan, bool &equal) const;
    size_t hash() const;
    Concept operator+(const Concept &other) const;
    Concept operator-() const;
    Concept operator-(const Concept &other) const;

    bool isPositive() const { return getCount() >= 0; }
    bool isStrictlyPositive() const { return getCount() > EPSILON; }

private:
    std::string name;
    Properties::Traits trait;
    Type type;
    double count;

    Concept(const std::string &name, Properties::Traits trait, Type type, double count);
};

std::ostream &operator<<(std::ostream &out, const Concept &concept);


namespace std {
template<>
struct hash<Concept> {
    inline size_t operator()(const Concept &concept) const {
        return concept.hash();
    }
};
}

#endif //EPISTOMATA_CONCEPT_H
