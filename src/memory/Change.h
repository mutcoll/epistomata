/**
 * @file Change.h
 * @author jmmut
 * @date 2016-11-13.
 */

#ifndef EPISTOMATA_CHANGE_H
#define EPISTOMATA_CHANGE_H


#include <traits/Properties.h>
#include "Context.h"
#include "Certainty.h"

static const double DEFAULT_CERTAINTY = 1.0;


void compareSortedConcepts(const SortedConcepts &first, const SortedConcepts &second,
        bool &lessThan, bool &equal);

void lexicographicallyCompareSortedConcepts(const SortedConcepts &first,
        const SortedConcepts &second, bool &equal, bool &lessThan);

struct Change {
    const std::string trait;
    const SortedConcepts objects;    // syntactical object: direct object, subject. of whether it disappears or not
    const Context difference;
    Certainty certainty;
    std::vector<PtrChange> steps;
    bool haveCachedHash;
    size_t cachedHash;

    static PtrChange build(std::vector<PtrChange> changes);

    Change(Properties::Traits trait, const Context &difference);

    Change(Properties::Traits trait, const Context &difference, double certainty);
    Change(Properties::Traits trait, Context &&difference, double certainty);

    Change(Properties::Traits trait, const SortedConcepts &objects, const Context &difference,
            const Certainty &certainty);

    Change(const std::string &trait, const Context &difference);

    Change(const std::string &trait, const Context &difference, double certainty);

    Change(const std::string &trait, const SortedConcepts &objects, const Context &difference,
            const Certainty &certainty);

    Change(const std::string &trait, const SortedConcepts &objects, const Context &difference,
            const Certainty &certainty, const std::vector<PtrChange> &steps);

    Change(const Change &change);

    bool operator<(const Change &other) const {
        bool equal, lessThan;
        compare(other, lessThan, equal);
        return lessThan;
    }

    void compare(const Change &other, bool &lessThan, bool &equal) const;
    size_t hash();

    /**
     * tries to apply this change into the parameter RecursiveMemory.
     * To unify two nodes, they must be called the same: an edible won't unify with a fruit.
     * @param from where to apply the change represented by *this
     * @return if applicable, the resulting RecMem and true, otherwise, an empty RecMem and false
     */
    Context applyIn(const Context &from, bool &applicable) const;
    void applyTo(Context &from, bool &applicable) const;
    Context unapplyIn(const Context &recursiveMemory, bool &applicable) const;
    Change reverse() const;

    Context applyInIfPositive(const Context &from, bool &applicable) const;

    void updateCertainty();

};


inline std::ostream &operator<<(std::ostream &out, const Change &change) {
    return out << change.trait;
}

PtrChange copyChange(const Change &other);
std::string changesTraitsToString(std::vector<std::shared_ptr<Change>> changes);

#endif //EPISTOMATA_CHANGE_H
