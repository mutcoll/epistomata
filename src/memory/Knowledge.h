/**
 * @file Knowledge.h
 * @author jmmut
 * @date 2016-09-30.
 */

#ifndef EPISTOMATA_KNOWLEDGE_H
#define EPISTOMATA_KNOWLEDGE_H


#include <map>
#include "traits/Properties.h"
#include "traits/Entity.h"
#include "memory/Context.h"
#include "memory/Change.h"


class Knowledge {
public:
    using Graph = randomize::utils::graph::Digraset<Concept>;
    using ptrToConcept = randomize::utils::graph::Digraset<Concept>::PtrNodeWrapper;
    static const bool ON_FIRST_ENCOUNTER_LEARN_AS_UNKNOWN = true;
    Knowledge();

    explicit Knowledge(const Context &me);

    Concept learn(Concept &concept);

    void learn(Change change);
    /**
     * TODO this does not take into account where are the entities, e.g. in or outside the inventory
     */
    static Context analyze(std::vector<std::shared_ptr<Position>> actualSituation);

    static Change buildChange(Properties::Traits action,
            Context before, Context after);
    static Change refineChange(Change expected, Change actual);

    Knowledge &operator+=(const Knowledge &other) {
        me += other.me;
        return *this;
    }

    Concepts &concepts() {
        return me.concepts();
    }

    Changes &changes() {
        return me.changes();
    }

    std::map<Concept, Context> &knowsThat() {
        return me.subcontext();
    }

    Context &knowsThat(Concept &who) {
        return me.subcontext(who);
    }

    std::shared_ptr<Context> goals() {
        return me.goals();
    }

    Context &get() {
        return me;
    }
private:
    Context me;
};

Context buildContext(std::initializer_list<std::string> entities);
Context buildContext(const std::vector<std::string> &entities);
SortedConcepts buildSortedConcepts(std::initializer_list<std::string> entities);

Change buildMovement(std::string who, std::string from, std::string to,
        std::initializer_list<std::string> disappear, std::initializer_list<std::string> appear);

#endif //EPISTOMATA_KNOWLEDGE_H
