/**
 * @file Planner.cpp
 * @author jmmut
 * @date 2016-11-27.
 */

#include <utils/graphExplorer/IterativeDepthCertaintyGraphExplorer.h>
#include <utils/pathRanker/MostUsefulPathRanker.h>
#include "Planner.h"

GraphExplorer::Path Planner::plan(
        std::string agent, Knowledge &knowledge, Context context, const Context &goal) {
    context.subcontext()[Concept(agent)].changes().addGraph(knowledge.changes());

//    std::shared_ptr<GraphExplorer> graphExplorer = std::make_shared<IterativeDepthGraphExplorer>();
    MostUsefulPathRanker ranker(agent, context, goal);
    WalkableGraph graph(agent);
    IterativeDepthCertaintyGraphExplorer explorer(graph, ranker);

    GraphExplorer &graphExplorer = explorer;
    return graphExplorer.findPath(context);
}

void Planner::doKnowledgeMaintenance(Knowledge &knowledge) {
    Changes &changes = knowledge.get().changes();
    for (PtrChange requiresObject : changes) {
        for (const Concept &objectRequired : requiresObject->objects) {
            for (PtrChange &makesAppear : changes) {
                const std::pair<bool, Concepts::PtrNodeWrapper> &found =
                        makesAppear->difference.concepts().findNode(objectRequired);
                if (found.first and found.second->node.isStrictlyPositive()) {
                    changes.addConnection(makesAppear, requiresObject);
                }
            }
        }
    }
}

