/**
 * @file Change.cpp
 * @author jmmut
 * @date 2016-11-13.
 */

#include "Change.h"
#include "utils/test/TestSuite.h"
#include "Context.h"

Change::Change(Properties::Traits trait, const Context &difference)
        : Change(traits::names[trait], difference) {
}

Change::Change(Properties::Traits trait, const Context &difference, double certainty)
        : Change(traits::names[trait], difference, certainty) {
}

Change::Change(Properties::Traits trait, Context &&difference, double certainty)
        : trait(traits::names[trait]), objects(), difference(std::move(difference)),
        certainty(certainty), steps(), haveCachedHash(false) {
}

Change::Change(Properties::Traits trait, const SortedConcepts &objects, const Context &difference,
        const Certainty &certainty)
        : Change(traits::names[trait], objects, difference, certainty) {
}

Change::Change(const std::string &trait, const Context &difference)
        : Change(trait, difference, DEFAULT_CERTAINTY) {
}

Change::Change(const std::string & trait, const Context &difference, double certainty)
        : trait(trait), difference(difference), certainty(certainty), haveCachedHash(false) {
}
Change::Change(const std::string & trait, const SortedConcepts &objects, const Context &difference,
        const Certainty &certainty)
        : Change(trait, objects, difference, certainty, {}) {
}

Change::Change(const std::string & trait, const SortedConcepts &objects, const Context &difference,
        const Certainty &certainty, const std::vector<PtrChange> &steps)
        : trait(trait), objects(objects), difference(difference), certainty(certainty),
        steps(steps), haveCachedHash(false) {
}

Change::Change(const Change &change)
        : Change(change.trait, change.objects, change.difference, change.certainty, change.steps) {
}

PtrChange Change::build(std::vector<PtrChange> changes) {
    Context difference;
    double certainty = 1;
    for (auto && change : changes) {
        difference += change->difference;
        certainty *= change->certainty.get();
    }
    SortedConcepts objects;
    return std::make_shared<Change>(traits::names[Properties::COMPOUND], objects, difference,
            certainty, changes);
}

Context Change::applyIn(const Context &from, bool &applicable) const {
    if (conceptsAreSubsetOf(objects, from.concepts())) {
        Context applied(from + difference);
        return std::move(applied);
    } else {
        applicable = false;
    }
    return std::move(Context());
}

Context Change::applyInIfPositive(const Context &from, bool &applicable) const {
    if (conceptsAreSubsetOf(objects, from.concepts())) {
        Context applied(from.sumIfPositive(difference, applicable));
        return std::move(applied);
    } else {
        applicable = false;
        return std::move(Context());
    }
}

void Change::applyTo(Context &from, bool &applicable) const {
    if (conceptsAreSubsetOf(objects, from.concepts())) {
        from += difference;
        applicable = from.isPositive();
    } else {
        applicable = false;
    }
}

Context Change::unapplyIn(const Context &recursiveMemory, bool &applicable) const {
    return std::move(reverse().applyIn(recursiveMemory, applicable));
}

Change Change::reverse() const {
    throw randomize::utils::exception::StackTracedException("unimplemented method");
}

void compareSortedConcepts(const SortedConcepts &first, const SortedConcepts &second,
        bool &lessThan, bool &equal) {
    if (first.size() < second.size()) {
        equal = false;
        lessThan = true;
    } else if (first.size() > second.size()) {
        equal = false;
        lessThan = false;
    } else { // they have the same size
        lexicographicallyCompareSortedConcepts(first, second, equal, lessThan);
    }
}

void Change::compare(const Change &other, bool &lessThan, bool &equal) const {
    if (trait != other.trait) {
        lessThan = trait < other.trait;
        equal = false;
        return;
    }

    compareSortedConcepts(objects, other.objects, lessThan, equal);
    if (not equal) {
        return;
    }

    difference.compare(other.difference, equal, lessThan);
    if (not equal) {
        return;
    }

    // both changes_ are equal
    equal = true;
    lessThan = false;
    return;
}

size_t Change::hash() {
    if (haveCachedHash) {
        return cachedHash;
    } else {
        size_t hashed = std::hash<std::string>()(trait);
        for (const Concept &object : objects) {
            hashed = 31 * hashed + object.hash();
        }
        hashed = 31 * hashed + difference.hash();

        for (const PtrChange &step : steps) {
            hashed = 31 * hashed + step->hash();
        }
        haveCachedHash = true;
        cachedHash = hashed;
        return hashed;
    }
}

void Change::updateCertainty() {
    if (Properties::is(trait, Properties::COMPOUND)) {
        double newCertainty = 1;
        for (const auto &step : steps) {
            newCertainty *= step->certainty.get();
        }
        certainty = newCertainty;
    }
}

void lexicographicallyCompareSortedConcepts(const SortedConcepts &first,
        const SortedConcepts &second, bool &equal, bool &lessThan) {
    ASSERT_EQUALS_OR_THROW_MSG(first.size(), second.size(), "precondition violated");
    SortedConcepts::const_iterator iterThis = first.begin();
    SortedConcepts::const_iterator iterOther = second.begin();
    equal = true;
    lessThan = false;
    bool remaining = iterThis != first.end();
    while (equal and remaining) {
        const Concept &thisConcept = iterThis.operator*();
        const Concept &otherConcept = iterOther.operator*();
        thisConcept.isIdentical(otherConcept, lessThan, equal);
        ++iterThis;
        ++iterOther;
        remaining = iterThis != first.end();
    }
}

std::string changesTraitsToString(std::vector<std::shared_ptr<Change>> changes) {
    std::string strategy;
    bool first = true;
    for (auto &&plannedChange : changes) {
        if (first) {
            first = false;
        } else {
            strategy += " ";
        }
        strategy += plannedChange->trait;
        if (Properties::is(plannedChange->trait, Properties::COMPOUND)) {
            strategy += "(" + changesTraitsToString(plannedChange->steps) + ")";
        }
    }
    return strategy;
}

PtrChange copyChange(const Change &other) {
    return std::make_shared<Change>(other);
}


