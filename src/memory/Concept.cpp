/**
 * @file Concept.cpp
 * @author jmmut
 * @date 2016-10-29.
 */

#include <sstream>
#include <utils/exception/StackTracedException.h>
#include "Concept.h"

Concept::Concept(const std::string &name, double count)
        : name(name), trait(Properties::UNKNOWN), type(Type::INSTANCE), count(count) {}

Concept::Concept(const Properties::Traits &trait, double count)
        : name(""), trait(trait), type(Type::PROPERTIES), count(count) {}

Concept::Concept(const std::string &name, Properties::Traits trait, Concept::Type type,
        double count) : name(name), trait(trait), type(type), count(count) {}

bool Concept::operator<(const Concept &other) const {
    bool lessThan = false, equal = false;
    isSimilar(other, lessThan, equal);
    return lessThan;
}

void Concept::isSimilar(const Concept &other, bool &lessThan, bool &equal) const {
    if (type != other.type) {
        lessThan = type < other.type;
        equal = false;
    } else {
        int compared;
        switch (type) {
        case Type::INSTANCE:
            if (name.size() == other.name.size()) {
                compared = name.compare(other.name);
                lessThan = compared < 0;
                equal = compared == 0;
            } else {
                lessThan = name.size() < other.name.size();
                equal = false;
            }
            break;
        case Type::PROPERTIES:
            lessThan = trait < other.trait;
            equal = trait == other.trait;
            break;
        }
    }
}

void Concept::isIdentical(const Concept &other, bool &lessThan, bool &equal) const {
    isSimilar(other, lessThan, equal);
    if (equal) {
        lessThan = count < other.count;
        equal = count == other.count;
    }
}

Concept Concept::operator+(const Concept &other) const {
    bool lessThan = false;
    bool equal = false;
    isSimilar(other, lessThan, equal);
    if (equal) {
        return Concept(name, trait, type, count + other.count);
    } else {
        std::stringstream ss;
        ss << "arithmetic operation on different concepts: " << *this << " and " << other;
        throw randomize::utils::exception::StackTracedException(ss.str());
    }
}

Concept Concept::operator-() const {
    return Concept(name, trait, type, -count);
}

Concept Concept::operator-(const Concept &other) const {
    return *this + (-other);
}

const std::string &Concept::getName() const {
    return name;
}

Properties::Traits Concept::getTrait() const {
    return trait;
}

Concept::Type Concept::getType() const {
    return type;
}

double Concept::getCount() const {
    return count;
}

size_t Concept::hash() const {
    size_t hashed;
    if (type == Type::PROPERTIES) {
        hashed = 31 * std::hash<Properties::Traits>()(trait);
    } else {
        hashed = std::hash<std::string>()(name);
    }
    hashed = 31 * hashed + size_t(count < 0 ? count * -2 : count);
    return hashed;
}

std::ostream &operator<<(std::ostream &out, const Concept &concept) {
    switch (concept.getType()) {
    case Concept::Type::INSTANCE:
        out << concept.getName();
        break;
    case Concept::Type::PROPERTIES:
        out << Properties(concept.getTrait());
        break;
    }
    return out;
}
