/**
 * @file Goals.h
 * @author jmmut
 * @date 2016-12-13.
 */

#ifndef EPISTOMATA_GOALS_H
#define EPISTOMATA_GOALS_H

#include "Context.h"

class Goals {
    Context goals;
};


#endif //EPISTOMATA_GOALS_H
