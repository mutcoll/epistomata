/**
 * @file Edible.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_EDIBLE_H
#define EPISTOMATA_EDIBLE_H

#include <memory>
#include "utils/exception/StackTracedException.h"
#include "log/log.h"
#include "Entity.h"
#include "Visitor.h"

class Edible : virtual public Entity {
public:
    Edible(size_t portions) : portions(portions) {}

    virtual ~Edible() override = default;
    virtual Properties getTraits() override {
        return Properties::EDIBLE;
    }


    virtual void beEaten(size_t portions) {
        LOG_DEBUG("%s being eaten", getName().c_str())
        if (portions > this->portions) {
            throw randomize::utils::exception::StackTracedException(
                    "not enough portions: asked for " + std::to_string(portions)
                            + " but only have " + std::to_string(this->portions));
        } else {
            this->portions -= portions;
        }
    }

    size_t remaining() { return portions; }

protected:
    size_t portions;
};


#endif //EPISTOMATA_EDIBLE_H
