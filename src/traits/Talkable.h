/**
 * @file Talkable.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_TALKABLE_H
#define EPISTOMATA_TALKABLE_H

#include <iostream>
#include "log/log.h"
#include "Entity.h"

class Talkable : virtual public Entity {
public:
    virtual ~Talkable() override = default;
    virtual Properties getTraits() override {
        return Properties::TALKABLE;
    }

    virtual std::string beTalked(std::string message) = 0;
};


#endif //EPISTOMATA_TALKABLE_H
