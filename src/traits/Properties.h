/**
 * @file Properties.h
 * @author jmmut
 * @date 2016-09-30.
 */

#ifndef EPISTOMATA_PROPERTIES_H
#define EPISTOMATA_PROPERTIES_H


#include <string>
#include <map>
#include <iostream>
#include <functional>

class Properties {
public:
    enum Traits {
        UNKNOWN =   0,
        NO_ACTION = 1 << 0,
        TALKABLE =  1 << 1,
        GRABBABLE = 1 << 2,
        EDIBLE =    1 << 3,
        MOVABLE =   1 << 4,
        KILLABLE =  1 << 5,
        TEACHABLE = 1 << 6,
        GIVABLE =   1 << 7,
        WALKABLE =  1 << 8,
        ASKABLE =   1 << 9,
        COMPOUND =   1 << 10,
    };

    Properties() : traits(UNKNOWN) {}

    Properties(Traits traits) : traits(traits) {}

    bool is(Traits property) const { return property & traits || property == traits; }

    Properties operator|(Properties other) { return Properties(traits | other.traits); }
    Properties operator+(Properties other) { return *this | other; }
    void forEach(std::function<void(const Traits &trait)> userFunc) const;

    static bool is(Traits first, Traits second) { return Properties(first).is(second); }
    static bool is(std::string first, Traits second);
    static Properties::Traits parse(std::string trait);

    unsigned int traits;
private:
    explicit Properties(unsigned int traits) : traits(traits) {}

};


namespace traits {
    extern std::map<enum Properties::Traits, std::string> names;
}

std::ostream &operator<<(std::ostream &out, const Properties &properties);

namespace std {
template<>
struct hash<Properties::Traits> {
    inline size_t operator()(const Properties::Traits &trait) const {
        return std::hash<unsigned int>()(trait);
    }
};
}
#endif //EPISTOMATA_PROPERTIES_H
