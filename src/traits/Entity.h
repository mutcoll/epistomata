/**
 * @file Entity.h
 * @author jmmut
 * @date 2016-09-30.
 */

#ifndef EPISTOMATA_ENTITY_H
#define EPISTOMATA_ENTITY_H


#include <memory>
#include <vector>
#include <managers/Action.h>
#include "utils/exception/StackTracedException.h"
#include "traits/Properties.h"
#include "drawers/Drawer.h"
#include "traits/Visitor.h"

class Position;

class Entity {
public:
    virtual ~Entity() = default;
    virtual Properties getTraits() = 0;

    virtual Action action(std::vector<std::shared_ptr<Position>> environment) {
        return {Properties::NO_ACTION, {}};
    }
    virtual const std::string & getName() = 0;
    virtual void receiveDrawer(Drawer &drawer) = 0;

    virtual void traitError(Properties::Traits trait) {
        throw randomize::utils::exception::StackTracedException(
                "logic error: Can't handle a " + getName() + " as a " + traits::names[trait]
                        + ", check its receiveVisitor");
    }
};

using PtrEntity = std::shared_ptr<Entity>;
using VecEntity = std::vector<PtrEntity>;
using Entities = VecEntity;


#endif //EPISTOMATA_ENTITY_H
