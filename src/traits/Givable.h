/**
 * @file Givable.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_GIVABLE_H
#define EPISTOMATA_GIVABLE_H

#include <iostream>
#include "log/log.h"
#include "Entity.h"

class Givable : virtual public Entity {
public:
    virtual ~Givable() override = default;

    virtual Properties getTraits() override {
        return Properties::GIVABLE;
    }

    virtual std::shared_ptr<Position> getInventory() { return inventory; }

    virtual void beGiven(std::shared_ptr<Entity> object) = 0;
protected:
    void setInventory(const std::shared_ptr<Position> &inventory) {
        Givable::inventory = inventory;
    }

private:
    std::shared_ptr<Position> inventory;
};


#endif //EPISTOMATA_GIVABLE_H
