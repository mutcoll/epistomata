/**
 * @file Askable.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_ASKABLE_H
#define EPISTOMATA_ASKABLE_H


#include <memory/Concept.h>
#include <memory/Context.h>
#include <memory/Knowledge.h>
#include "utils/Digraset2.h"
#include "Entity.h"

class Askable : virtual public Entity {
public:
    virtual ~Askable() override = default;
    virtual Properties getTraits() override {
        return Properties::ASKABLE;
    }

    virtual Knowledge & beAsked() = 0;
};


#endif //EPISTOMATA_ASKABLE_H
