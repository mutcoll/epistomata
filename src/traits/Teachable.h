/**
 * @file Teachable.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_TEACHABLE_H
#define EPISTOMATA_TEACHABLE_H


#include <traits/Entity.h>
#include <memory/Knowledge.h>
#include <log/log.h>

class Teachable : virtual public Entity {
public:
    virtual ~Teachable() override = default;
    virtual Properties getTraits() override {
        return Properties::TEACHABLE;
    }

    //    virtual void beTaught(std::string name, Properties properties) {
    //        LOG_DEBUG("%s being taught about %s", getName().c_str(), name.c_str())
    //        knowledge.learn(name, properties);
    //    }
    //
    virtual void beTaught(const Change &change) {
        this->knowledge.learn(change);
    }
    virtual void beTaught(const Knowledge &knowledge) {
        //LOG_DEBUG("%s being taught about %s", getName().c_str(), name.c_str())
        this->knowledge += knowledge;
    }
protected:
    Knowledge knowledge;
};


#endif //EPISTOMATA_TEACHABLE_H
