/**
 * @file Grabbable.h
 * @author jmmut
 * @date 2016-10-11.
 */

#ifndef EPISTOMATA_GRABBABLE_H
#define EPISTOMATA_GRABBABLE_H


#include <traits/Entity.h>

class Grabbable : virtual public Entity {
public:
    virtual ~Grabbable() override = default;

    virtual Properties getTraits() override { return Properties::GRABBABLE; }

    virtual std::shared_ptr<Entity> beGrabbed() = 0;
};


#endif //EPISTOMATA_GRABBABLE_H
