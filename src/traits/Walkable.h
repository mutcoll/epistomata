/**
 * @file Walkable.h
 * @author jmmut
 * @date 2016-11-30.
 */

#ifndef EPISTOMATA_WALKABLE_H
#define EPISTOMATA_WALKABLE_H

#include "Entity.h"
#include "Properties.h"

class Walkable : virtual public Entity {
public:
    virtual ~Walkable() override = default;

    virtual Properties getTraits() override {
        return Properties::WALKABLE;
    }

    virtual void prepareToBeWalked(std::shared_ptr<Entity> walker) = 0;
    virtual void prepareDeparture(std::shared_ptr<Entity> walker) = 0;
    virtual void finishMovement() = 0;
    virtual size_t entitiesCount() = 0;
    virtual std::shared_ptr<Entity> entity(size_t index) = 0;

};

#endif //EPISTOMATA_WALKABLE_H
