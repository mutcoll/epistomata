/**
 * @file Visitor.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_VISITOR_H
#define EPISTOMATA_VISITOR_H

#include "log/log.h"

class Talkable;
class Edible;
class Movable;
//class Walkable;
//class Readable;
class Grabbable;
class Givable;
class Killable;
class Askable;
class Teachable;
class Walkable;

class Knowledge;


class Visitor {
public:
    virtual ~Visitor() = default;

    virtual size_t eat(size_t portions) { LOG_DEBUG("unimplemented visitor method eat"); return 0; }
    virtual void talk(Talkable &talkable) { LOG_DEBUG("unimplemented visitor method talk"); }
//    virtual void move(Movable &movable) { LOG_DEBUG("unimplemented visitor method move"); }
//    virtual void kill(bool killed) { LOG_DEBUG("unimplemented visitor method kill"); }
    virtual void ask(std::string who, Knowledge &knowledge) { LOG_DEBUG("unimplemented visitor method ask"); }
    virtual void teach(Teachable &teachable) { LOG_DEBUG("unimplemented visitor method teach"); }
    virtual void walk(Walkable &walkable) { LOG_DEBUG("unimplemented visitor method walk"); }
//    virtual void step(Walkable &walkable) = 0;
//    virtual void read(Readable &readable) = 0;
    virtual void give(Givable &givable) { LOG_DEBUG("unimplemented visitor method give"); }
    virtual void grab(Grabbable &grabbable) { LOG_DEBUG("unimplemented visitor method grab"); }
};


#endif //EPISTOMATA_VISITOR_H
