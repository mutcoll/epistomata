/**
 * @file Killable.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_KILLABLE_H
#define EPISTOMATA_KILLABLE_H


#include <traits/Entity.h>

/**
 * problems
 * |_ doing the reincarnation dependent of death:
 * ||_ trees can not give fruit until they die
 * |
 * |_ doing reincarnation INdependent of death:
 *  |_ how does the engine know that it has to add some entities to the map?
 *   |_ the program lacks an event system
 *   ||_ probably it would make every entity know about the event system to post notifications
 *   |_ checking for every entity if it gave new entities may be costly
 */
class Killable : virtual public Entity {
public:
    Killable(int energy) : energy(energy) {}

    virtual ~Killable() override = default;

    virtual Properties getTraits() override {
        return Properties::KILLABLE;
    }

    virtual void beAttacked(size_t damage) { energy -= damage; }
    virtual bool beKilled() { return energy <= 0; }
    virtual int getRemainingLife() { return energy; }
    // TODO: make reincarnation possible without dying. look class comments above
    virtual Entities reincarnate() { return {}; }
protected:
    int energy;
};


#endif //EPISTOMATA_KILLABLE_H
