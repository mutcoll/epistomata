/**
 * @file Movable.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_MOVABLE_H
#define EPISTOMATA_MOVABLE_H



class Movable : virtual public Entity {
public:
    virtual ~Movable() override = default;
    virtual Properties getTraits() override {
        return Properties::MOVABLE;
    }

    virtual std::pair<int, int> beMoved() = 0;
};

#endif //EPISTOMATA_MOVABLE_H
