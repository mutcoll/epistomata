/**
 * @file Properties.cpp
 * @author jmmut
 * @date 2016-10-01.
 */

#include <utils/exception/StackTracedException.h>
#include <log/log.h>
#include "Properties.h"

std::map<enum Properties::Traits, std::string> traits::names = {
        {Properties::UNKNOWN, "unknown"},
        {Properties::NO_ACTION, "no_action"},
        {Properties::ASKABLE, "askable"},
        {Properties::TALKABLE, "talkable"},
        {Properties::GRABBABLE, "grabbable"},
        {Properties::EDIBLE, "edible"},
        {Properties::MOVABLE, "movable"},
        {Properties::KILLABLE, "killable"},
        {Properties::TEACHABLE, "teachable"},
        {Properties::GIVABLE, "givable"},
        {Properties::WALKABLE, "walkable"},
        {Properties::COMPOUND, "compound"},
};

Properties::Traits Properties::parse(std::string trait) {
    std::string nameList = "[";
    for(auto entry : traits::names) {
        if (trait == entry.second) {
            return entry.first;
        } else {
            nameList += entry.second + ", ";
        }
    }
    nameList.pop_back();
    nameList.pop_back();
    throw randomize::utils::exception::StackTracedException("there's no trait \"" + trait
            + "\", use one of: " + nameList + "]");
}

void Properties::forEach(std::function<void(const Traits &trait)> userFunc) const {
    for (const std::pair<Properties::Traits, std::string> &property : traits::names) {
        if (this->is(property.first)) {
            userFunc(property.first);
        }
    }
}

bool Properties::is(std::string first, Properties::Traits second) {
    return first == traits::names[second];
}

std::ostream &operator<<(std::ostream &out, const Properties &properties) {
    properties.forEach([&](const Properties::Traits &trait) {
        out << traits::names[trait] << ",";
    });
    return out;
}
