/**
 * @file main.cpp
 */

#include <iostream>
#include "utils/SignalHandler.h"
#include "log/log.h"
#include "managers/Map.h"
#include "managers/Engine.h"
#include "managers/Shell.h"

using namespace std;

int main(int argc, char **argv)
{
    SigsegvPrinter::activate();
    LOG_LEVEL(argc == 2? randomize::log::parseLogLevel(argv[1]) : LOG_WARN_LEVEL);

    Shell shell;
    shell.loop();

    cout << endl;
    return 0;
}
