/**
 * @file StringContextPrinter.cpp
 * @author jmmut
 * @date 2017-01-29.
 */

#include <string>
#include "JsonContextPrinter.h"
#include "memory/Change.h"

template<typename T>
struct Quoter {
    const T &printable;
};

template<typename T>
Quoter<T> quote(const T &printable) {
    return Quoter<T>{printable};
}

template<typename T>
std::ostream &operator<<(std::ostream &out, const Quoter<T> &q) {
    return out << "\"" << q.printable << "\"";
}

std::ostream &JsonContextPrinter::print(std::ostream &out, const Context &memory) {
    out << "{";
    print(out, memory, 0);
    out << "}" << std::endl;
    return out;
}

std::ostream &JsonContextPrinter::print(std::ostream &out, const Context &memory, size_t level) {
    int elements = 0;
    elements += memory.concepts().size() > 0;
    printConcepts(out, memory.concepts(), level);

    elements += memory.changes().size() > 0;
    if (elements > 1) {
        out << "\n,";
        --elements;
    }
    printChanges(out, memory.changes(), level);

    elements += memory.subcontext().size() > 0;
    if (elements > 1) {
        out << "\n,";
        --elements;
    }
    printSubContext(out, memory.subcontext(), level);

    elements += memory.hasGoals();
    if (elements > 1) {
        out << "\n,";
        --elements;
    }
    printGoals(out, memory.goals(), level);
    return out;
}

void JsonContextPrinter::printChange(std::ostream &out, const Change &change, size_t level) {
    out << indent(level) << "{\"" << TRAIT << "\": \"" << change << "\"";
    if (randomize::log::log_level == randomize::log::DEBUG_LEVEL) {
        out << ", \"self\":" << &change;
    }
    out << ", \"" << CERTAINTY << "\":" << change.certainty.get() << "\n";
    level++;
    if (change.objects.size() > 0) {
        out << indent(level) << ",\"" << OBJECTS << "\": [\n";
        printSortedConcepts(out, change.objects, level + 1);
        out << "]\n";
    }
    if (change.difference.size() > 0) {
        out << indent(level) << ",\"" << DIFFERENCE << "\": {\n";
        print(out, change.difference, level + 1);
        out << "}";
    }
    if (change.steps.size() > 0) {
        out << ", \n";
        printChanges(out, change.steps, level + 1);
    }
    level--;
    out << "}";
}

void JsonContextPrinter::printSubContext(std::ostream &out,
        const std::map<Concept, Context> &subcontext, size_t level) {
    if (subcontext.size() > 0) {
        out << indent(level) << "\"" << SUBCONTEXT << "\": {";
        level++;
        for (const auto &entry : subcontext) {
            out << std::endl << indent(level) << quote(entry.first) << ": {\n";
            this->print(out, entry.second, level + 1);
            out << " }";
        }
        --level;
        out << " }\n";
    }

}

void JsonContextPrinter::printGoals(std::ostream &out,
        const std::shared_ptr<Context> goals, size_t level) {
    if (goals != nullptr) {
        out << indent(level) << "\"" << GOALS << "\": [ {" << std::endl;
        level++;
        this->print(out, goals.operator*(), level);
        --level;
        out << "} ]\n";
    }
}

void JsonContextPrinter::printSortedConcepts(std::ostream &out,
        const SortedConcepts &sortedConcepts, size_t level) {
    if (sortedConcepts.size() > 0) {
        out << indent(level);
        size_t count = sortedConcepts.size();
        size_t i = 0;
        for (const Concept &concept : sortedConcepts) {
            out << quote(concept);
            if (++i != count) {
                out << ", ";
            }
        }
    }
}


std::string indent(size_t level) {
    std::string oneLevel("  ");
    std::string indentation;
    indentation.reserve(oneLevel.size() * level);
    for (size_t i = 0; i < level; ++i) {
        indentation.append(oneLevel);
    }
    return indentation;
}


std::string jsonPrint(const Concepts &concepts) {
    std::stringstream ss;
    JsonContextPrinter printer;
    printer.printConcepts(ss, concepts, 0);
    return ss.str();
}

std::string jsonPrint(const Changes &changes) {
    std::stringstream ss;
    JsonContextPrinter printer;
    printer.printChanges(ss, changes, 0);
    return ss.str();
}

std::string jsonPrint(const Change &change) {
    std::stringstream ss;
    JsonContextPrinter printer;
    printer.printChange(ss, change, 0);
    return ss.str();
}
std::string jsonPrint(const Concept &concept) {
    std::stringstream ss;
    ss << concept << std::endl;
    return ss.str();
}

std::string jsonPrint(const Context &context) {
    std::stringstream ss;
    JsonContextPrinter printer;
    printer.print(ss, context);
    return ss.str();
}

