/**
 * @file ContextPrinter.h
 * @author jmmut
 * @date 2017-01-29.
 */

#ifndef EPISTOMATA_CONTEXTPRINTER_H
#define EPISTOMATA_CONTEXTPRINTER_H


#include <ostream>
#include <memory/Context.h>

class ContextPrinter {
public:
    virtual std::ostream & print(std::ostream &out, const Context &memory) = 0;

    static constexpr const char* CONCEPTS = "concepts";
    static constexpr const char* CHANGES = "changes";
    static constexpr const char* TRAIT = "trait";
    static constexpr const char* OBJECTS = "objects";
    static constexpr const char* DIFFERENCE = "difference";
    static constexpr const char* CERTAINTY = "certainty";
    static constexpr const char* SUBCONTEXT = "subcontexts";
    static constexpr const char* GOALS = "goals";
};


#endif //EPISTOMATA_CONTEXTPRINTER_H
