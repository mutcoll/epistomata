/**
 * @file StringContextPrinter.h
 * @author jmmut
 * @date 2017-01-29.
 */

#ifndef EPISTOMATA_JSONCONTEXTPRINTER_H
#define EPISTOMATA_JSONCONTEXTPRINTER_H


#include "ContextPrinter.h"

class JsonContextPrinter : public ContextPrinter {
public:
    std::ostream &print(std::ostream &out, const Context &memory) override;

    template<typename T>
    void printConcepts(std::ostream &out, const T &concepts, size_t level);

    template<typename T>
    void printChanges(std::ostream &out, const T &changes, size_t level);

    void printChange(std::ostream &out, const Change &change, size_t level);

    void
    printSubContext(std::ostream &out, const std::map<Concept, Context> &subcontext, size_t level);

    std::ostream &print(std::ostream &out, const Context &memory, size_t level);

    void printGoals(std::ostream &ostream, const std::shared_ptr<Context> shared_ptr, size_t level);

private:
    int verbosity;

    void printSortedConcepts(std::ostream &out, const SortedConcepts &sortedConcepts, size_t level);
};

std::string indent(size_t level);

std::string jsonPrint(const Concepts &concepts);
std::string jsonPrint(const Changes &changes);
std::string jsonPrint(const Change &change);
std::string jsonPrint(const Concept &concept);
std::string jsonPrint(const Context &context);

template <typename T>
void JsonContextPrinter::printConcepts(std::ostream &out, const T &concepts, size_t level) {
    if (concepts.size() > 0) {
        out << indent(level) << "\"" << CONCEPTS << "\": {";
        size_t count = concepts.size();
        size_t i = 0;
        for (const Concept &concept : concepts) {
            out << "\"" << concept << "\" : " << concept.getCount();
            if (++i != count) {
                out << ",";
            }
        }
        out << "}";
    }
}

template <typename T>
void JsonContextPrinter::printChanges(std::ostream &out, const T &changes, size_t level) {
    if (changes.size() > 0) {
        out << indent(level) << "\"" << CHANGES << "\": [\n";
        level += 1;
        size_t count = changes.size();
        size_t i = 0;
        for (auto change : changes) {
            printChange(out, *change, level);
            if (++i != count) {
                out << ", \n";
            }
        }
        level -= 1;
        out << " ]\n";
    }
}


#endif //EPISTOMATA_JSONCONTEXTPRINTER_H
