/**
 * @file DotPrinter.h
 * @author jmmut
 * @date 2017-01-29.
 */

#ifndef EPISTOMATA_DOTPRINTER_H
#define EPISTOMATA_DOTPRINTER_H


#include "ContextPrinter.h"

class DotContextPrinter : public ContextPrinter {
public:
    std::ostream & print(std::ostream &out, const Context &memory) override;

    std::ostream & printConcepts(std::ostream &out, const Context &memory);

    std::ostream & printConceptNode(std::ostream &out,
            const std::shared_ptr<Concepts::NodeWrapper> node);

    std::ostream & printChanges(std::ostream &out, const Context &memory);

    std::ostream & printChangesNode(std::ostream &out,
            const std::shared_ptr<Changes::NodeWrapper> node);

    std::ostream &printRecursive(std::ostream &out, const Context &memory);

    std::ostream &printSubgraph(std::ostream &out,
            const std::string &sourceNode, const std::string &clusterName,
            const std::string &linkName, const Context &recursiveMemory);

    std::ostream &print(std::ostream &out, const std::string &nodeString, const std::string &nodeIdName,
            const std::string name, const SortedConcepts &concepts);
};


#endif //EPISTOMATA_DOTPRINTER_H
