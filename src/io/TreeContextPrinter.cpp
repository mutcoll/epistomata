/**
 * @file StringContextPrinter.cpp
 * @author jmmut
 * @date 2017-01-29.
 */

#include <string>
#include "TreeContextPrinter.h"
#include "memory/Change.h"

std::string indent(size_t level);

std::ostream &TreeContextPrinter::print(std::ostream &out, const Context &memory) {
    print(out, memory, 0);
    return out;
}

std::ostream &TreeContextPrinter::print(std::ostream &out, const Context &memory, size_t level) {
    printConcepts(out, memory.concepts(), level);
    printChanges(out, memory.changes(), level);
    printSubContext(out, memory.subcontext(), level);
    printGoals(out, memory.goals(), level);
    return out;
}

void TreeContextPrinter::printConcepts(std::ostream &out, const Concepts &concepts, size_t level) {
    if (concepts.size() > 0) {
        out << indent(level) << "concepts: ";
        for (auto &concept : concepts) {
            out << concept << ", ";
        }
        out << std::endl;
    }
}

void TreeContextPrinter::printConcepts(std::ostream &out, const SortedConcepts &concepts, size_t level) {
    if (concepts.size() > 0) {
        out << indent(level) << "concepts: ";
        for (auto &concept : concepts) {
            out << concept << ", ";
        }
        out << std::endl;
    }
}

void TreeContextPrinter::printChanges(std::ostream &out, const Changes &changes, size_t level) {
    if (changes.size() > 0) {
        out << indent(level) << "changes, " << changes.countConnections() << " inner connections: [";
        level += 1;
        Changes::iterator iterator = changes.begin();
        const Changes::iterator &end = changes.end();
        for (; iterator != end; ++iterator) {
            out << std::endl << indent(level)
                << "successors: " << iterator.wrapper().operator*()->successors.size()
                << ", predecessors: " << iterator.wrapper().operator*()->predecessors.size()
                << std::endl;
            printChange(out, iterator.operator*().operator*(), level);
        }
        level -= 1;
        out << " ]\n";
    }
}

void TreeContextPrinter::printChanges(std::ostream &out,
        const std::vector<std::shared_ptr<Change>> &changes, size_t level) {
    if (changes.size() > 0) {
        out << indent(level) << "changes: [\n";
        level += 1;
        for (auto conceptWrapper : changes) {
            out << std::endl;
            printChange(out, *conceptWrapper, level);
        }
        level -= 1;
        out << " ]\n";
    }
}

void TreeContextPrinter::printChange(std::ostream &out, const Change &change, size_t level) {
    out << indent(level) << change << "(" << change.certainty.get() << "): {\n";
    level++;
    if (change.objects.size() > 0) {
        out << indent(level) << "objects: \n";
        printConcepts(out, change.objects, level + 1);
    }
    if (change.difference.size() > 0) {
        out << indent(level) << "difference: \n";
        print(out, change.difference, level + 1);
    }
    level--;
    out << indent(level) << "}";
}

void TreeContextPrinter::printSubContext(std::ostream &out,
        const std::map<Concept, Context> &subcontext, size_t level) {
    if (subcontext.size() > 0) {
        out << indent(level) << "subcontext: [";
        level++;
        for (const auto &entry : subcontext) {
            out << std::endl << indent(level) << entry.first << ": {\n";
            this->print(out, entry.second, level + 1);
            out << indent(level) << "}";
        }
        --level;
        out << " ]\n";
    }

}

void TreeContextPrinter::printGoals(std::ostream &out,
        const std::shared_ptr<Context> goals, size_t level) {
    if (goals != nullptr) {
        out << indent(level) << "goals: [" << std::endl;
        level++;
        this->print(out, goals.operator*(), level);
        --level;
        out << " ]\n";
    }
}

std::string treePrint(const Concepts &concepts) {
    std::stringstream ss;
    TreeContextPrinter printer;
    printer.printConcepts(ss, concepts, 0);
    return ss.str();
}

std::string treePrint(const Changes &changes) {
    std::stringstream ss;
    TreeContextPrinter printer;
    printer.printChanges(ss, changes, 0);
    return ss.str();
}

std::string treePrint(const Change &change) {
    std::stringstream ss;
    TreeContextPrinter printer;
    printer.printChange(ss, change, 0);
    return ss.str();
}
std::string treePrint(const Concept &concept) {
    std::stringstream ss;
    ss << concept << std::endl;
    return ss.str();
}

