/**
 * @file StringContextPrinter.h
 * @author jmmut
 * @date 2017-01-29.
 */

#ifndef EPISTOMATA_STRINGCONTEXTPRINTER_H
#define EPISTOMATA_STRINGCONTEXTPRINTER_H


#include "ContextPrinter.h"

class TreeContextPrinter : public ContextPrinter {
public:
    std::ostream &print(std::ostream &out, const Context &memory) override;

    void printConcepts(std::ostream &out, const Concepts &concepts, size_t level);
    void printConcepts(std::ostream &out, const SortedConcepts &concepts, size_t level);

    void printChanges(std::ostream &out, const Changes &changes, size_t level);

    void printChanges(std::ostream &out, const std::vector<std::shared_ptr<Change>> &changes,
            size_t level);

    void printChange(std::ostream &out, const Change &change, size_t level);

    void
    printSubContext(std::ostream &out, const std::map<Concept, Context> &subcontext, size_t level);

    std::ostream &print(std::ostream &out, const Context &memory, size_t level);

    void printGoals(std::ostream &ostream, const std::shared_ptr<Context> shared_ptr, size_t level);
};

std::string treePrint(const Concepts &concepts);
std::string treePrint(const Changes &changes);
std::string treePrint(const Change &change);
std::string treePrint(const Concept &concept);

#endif //EPISTOMATA_STRINGCONTEXTPRINTER_H
