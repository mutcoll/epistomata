/**
 * @file DotPrinter.cpp
 * @author jmmut
 * @date 2017-01-29.
 */

#include "DotContextPrinter.h"
#include "memory/Change.h"

std::ostream & DotContextPrinter::print(std::ostream &out, const Context &memory) {
    out << "digraph graphMemory {\n";
    printRecursive(out, memory);
    out << "}\n";
    return out;
}

std::ostream & DotContextPrinter::printConcepts(std::ostream &out, const Context &memory) {
    memory.concepts().print(out);
    return out;
}

std::ostream & DotContextPrinter::printConceptNode(std::ostream &out,
        const std::shared_ptr<Concepts::NodeWrapper> node) {
    Concepts().printNode(out, node);
    return out;
}

std::ostream & DotContextPrinter::printChanges(std::ostream &out, const Context &memory) {
    Changes::iterator iterator = memory.changes().begin();
    const Changes::iterator &end = memory.changes().end();
    for (; iterator != end; ++iterator) {
        printChangesNode(out, iterator.wrapper().operator*());
    }
    return out;
}

std::ostream & DotContextPrinter::printChangesNode(std::ostream &out,
        const std::shared_ptr<Changes::NodeWrapper> node) {

    std::stringstream nodeId;
    nodeId << node;
    const std::string &nodeIdString = nodeId.str();

    std::stringstream nodeName;
    Changes().printNode(nodeName, node);
    const std::string &nodeString = nodeName.str();

    print(out, nodeString, nodeIdString + "objects", "objects", node->node->objects);
    printSubgraph(out, nodeString, nodeIdString + "disappear", "disappear", node->node->difference);
    return out;
}


inline std::ostream &print(std::ostream &out, Concept concept) {
    return out << "{\"" << concept << "\"[label=\"" << concept << "\"]}";
}
inline std::ostream &printString(std::ostream &out, std::string name) {
    return out << "{\"" << name << "\"[label=\"" << name << "\"]}";
}
inline std::ostream &print(std::ostream &out, std::string id, std::string name) {
    return out << "{\"" << id << "\"[label=\"" << name << "\"]}";
}



std::ostream &DotContextPrinter::printRecursive(std::ostream &out, const Context &memory) {

    printConcepts(out, memory);
    printChanges(out, memory);

    for (const std::pair<const Concept, Context> &entry : memory.subcontext()) {
        std::stringstream ss;
        ss << memory.concepts().findNode(entry.first).second;
        std::string clusterName = ss.str();
        auto found = memory.concepts().findNode(entry.first);
        std::stringstream sourceNode;
        if (found.first) {
            printConceptNode(sourceNode, found.second);
        } else {
            LOG_ERROR("inconsistency, %s is in subcontext, but not in concepts",
                    entry.first.getName().c_str());
            printString(sourceNode, "not_found_" + entry.first.getName());
        }
        printSubgraph(out, sourceNode.str(), clusterName, "subcontext", entry.second);
    }
    return out;
}

std::ostream & DotContextPrinter::printSubgraph(std::ostream &out,
        const std::string &sourceNode, const std::string &clusterName,
        const std::string &linkName, const Context &recursiveMemory) {

    out << "subgraph " << "\"cluster" << clusterName << "\" {\n";
    printRecursive(out, recursiveMemory);
    out << "}\n";
    out << sourceNode << " -> " << "\"cluster" << clusterName << "\"[label=" << linkName << "]\n";
    return out;
}

std::ostream &DotContextPrinter::print(std::ostream &out, const std::string &nodeString,
        const std::string &nodeIdName, const std::string name, const SortedConcepts &concepts) {
    throw randomize::utils::exception::StackTracedException("unimplemented");
    return out;
}
