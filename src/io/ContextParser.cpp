/**
 * @file ContextParser.cpp
 * @author jmmut
 * @date 2017-04-02.
 */

#include "ContextParser.h"
#include "ContextPrinter.h"
#include "memory/Change.h"

using randomize::utils::exception::StackTracedException;

Context ContextParser::parse(std::istream &inputStream) {
    std::string input;
    while (inputStream) {
        std::string word;
        inputStream >> word;
        input.append(word);
    }
    return parse(input);
}

Context ContextParser::parse(std::string &input) {
    Json::Value parsed;
    std::stringstream inputStream(input);
    try {
        inputStream >> parsed;
        Context context;
        addContext(parsed, context);
        return context;
    } catch (const StackTracedException &error) {
        throw; // don't add another stacktrace here
    } catch (const std::exception &error) {
        throw StackTracedException(
                std::string("error parsing json: ") + error.what() + "\noriginal string: " + input);
    }
}

void ContextParser::addContext(Json::Value jsonContext, Context &context) const {
    if (jsonContext.isObject()) {
        addConcepts(jsonContext, context);
        addChanges(jsonContext, context);
        addSubcontext(jsonContext, context);
        addGoals(jsonContext, context);
    } else if (not jsonContext.isNull()) {
        throw StackTracedException("context should be an object: " + jsonContext.toStyledString());
    }
}

void ContextParser::addConcepts(const Json::Value &parsed, Context &context) const {
    const Json::Value &concepts = parsed[ContextPrinter::CONCEPTS];
    if (concepts.isObject()) {
        for (const Json::Value &name : concepts.getMemberNames()) {
            Json::Value count = concepts[name.asString()];
            if (count.isDouble()) {
                context.concepts().addNode(Concept(name.asString(), count.asDouble()));
            }
        }
    } else if (not concepts.isNull()) {
        throw StackTracedException(std::string("\"") + ContextPrinter::CONCEPTS
                + "\" should be an object: " + parsed.toStyledString());
    }
}
void ContextParser::addSortedConcepts(const Json::Value &parsed, SortedConcepts &context) const {
    const Json::Value &objects = parsed[ContextPrinter::OBJECTS];
    if (objects.isArray()) {
        for (const Json::Value &name : objects) {
            if (name.isString()) {
                context.push_back(Concept(name.asString()));
            }
        }
    } else if (not objects.isNull()) {
        throw StackTracedException(std::string("\"") + ContextPrinter::OBJECTS
                + "\" should be an array: " + parsed.toStyledString());
    }
}

std::vector<PtrChange> ContextParser::getRecursiveChanges(const Json::Value &parsed) const {
    std::vector<PtrChange> changes;
    const Json::Value &jsonChanges = parsed[ContextPrinter::CHANGES];
    if (jsonChanges.isArray()) {
        for (const Json::Value &change : jsonChanges) {
            std::string trait = change[ContextPrinter::TRAIT].asString();
            SortedConcepts sortedConcepts;
            addSortedConcepts(change, sortedConcepts);
            Context difference;
            addContext(change[ContextPrinter::DIFFERENCE], difference);
            double certainty = change[ContextPrinter::CERTAINTY].asDouble();
            std::vector<PtrChange> steps = getRecursiveChanges(change);
            changes.push_back(std::make_shared<Change>(
                    trait, sortedConcepts, difference, Certainty(certainty), steps));
        }
    } else if (not jsonChanges.isNull()) {
        throw StackTracedException(std::string("\"") + ContextPrinter::CHANGES
                + "\" should be an array: " + parsed.toStyledString());
    }
    return changes;
}

void ContextParser::addChanges(const Json::Value &parsed, Context &context) const {
    std::vector<PtrChange> changes = getRecursiveChanges(parsed);
    for (auto &&change : changes) {
        context.changes().addNode(change);
    }
}

void ContextParser::addSubcontext(const Json::Value &parsed, Context &context) const {
    const Json::Value &subcontext = parsed[ContextPrinter::SUBCONTEXT];
    if (subcontext.isObject()) {
        for (const Json::Value &name : subcontext.getMemberNames()) {
            Json::Value innerContext = subcontext[name.asString()];
            if (innerContext.isObject()) {
                addContext(innerContext, context.subcontext(Concept(name.asString())));
            }
        }
    } else if (not subcontext.isNull()) {
        throw StackTracedException(std::string("\"") + ContextPrinter::SUBCONTEXT
                + "\" should be an object: " + parsed.toStyledString());
    }
}

void ContextParser::addGoals(const Json::Value &parsed, Context &context) const {
    const Json::Value &goals = parsed[ContextPrinter::GOALS];
    if (goals.isArray()) {
        for (const Json::Value &innerContext : goals) {
            if (innerContext.isObject()) {
                Context goal;
                addContext(innerContext, goal);
                context.setGoals(goal); // TODO at the moment the back-end only supports one goal, the last one
            }
        }
    } else if (not goals.isNull()) {
        throw StackTracedException(std::string("\"") + ContextPrinter::GOALS
                + "\" should be an object: " + parsed.toStyledString());
    }
}

