/**
 * @file ContextParser.h
 * @author jmmut
 * @date 2017-04-02.
 */

#ifndef EPISTOMATA_CONTEXTPARSER_H
#define EPISTOMATA_CONTEXTPARSER_H


#include <memory/Context.h>
#include "json/json.h"

class ContextParser {
public:
    Context parse(std::istream &s);
    Context parse(std::string &input);

private:
    void addContext(Json::Value value, Context &context) const;
    void addConcepts(const Json::Value &parsed, Context &context) const;
    void addSortedConcepts(const Json::Value &parsed, SortedConcepts &context) const;
    void addChanges(const Json::Value &parsed, Context &context) const;
    void addSubcontext(const Json::Value &parsed, Context &context) const;

    void addGoals(const Json::Value &parsed, Context &context) const;

    std::vector<PtrChange> getRecursiveChanges(const Json::Value &parsed) const;
};


#endif //EPISTOMATA_CONTEXTPARSER_H
