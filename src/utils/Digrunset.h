#ifndef EPISTOMATA_DIGRUNSET_H
#define EPISTOMATA_DIGRUNSET_H

#include <memory>
#include <vector>
#include <algorithm>
#include <sstream>
#include <set>
#include <tuple>
#include <unordered_set>
#include "utils/exception/StackTracedException.h"

namespace randomize {
namespace utils {
namespace graph {

/**
 * @class Digrunset
 *
 * This class is a simple directed graph.
 *
 * It stores a set of nodes, so that it is easy to know if it's present in the graph or not, and
 * then query its neighbours.
 */
template<typename NODE>
class Digrunset {
public:
    struct NodeWrapper;

    using PtrNodeWrapper = std::shared_ptr<NodeWrapper>;
    using Connection = std::pair<PtrNodeWrapper, PtrNodeWrapper>;

    using LessThanFunctionType = std::function<bool (PtrNodeWrapper, PtrNodeWrapper)>;
    using CompareFunctionType = std::function<bool (PtrNodeWrapper, PtrNodeWrapper, bool & equal)>;
    //    using InnerSetType = std::unordered_set<PtrNodeWrapper, LessThanFunctionType>;

    struct Hasher {
        inline size_t operator()(const PtrNodeWrapper& ptrNodeWrapper) const noexcept {
            return std::hash<NODE>()(ptrNodeWrapper->node);
        }
    };
    struct Equal {
        inline bool operator()(const PtrNodeWrapper& x, const PtrNodeWrapper &y) const noexcept {
            return std::hash<NODE>()(x->node) == std::hash<NODE>()(y->node);
        }
    };
    using InnerSetType = std::unordered_set<PtrNodeWrapper, Hasher, Equal>;

    struct NodeWrapper {
        NodeWrapper(const NODE &node,
                const InnerSetType &predecessors = {},
                const InnerSetType &successors = {})
                : node(node), predecessors(predecessors), successors(successors) { }

        NODE node;
        InnerSetType predecessors;
        InnerSetType successors;
    };

    struct Iterator {
        typename InnerSetType::const_iterator inner;

        explicit Iterator() : inner(nullptr) {}
        explicit Iterator(const typename InnerSetType::const_iterator &other) : inner(other) {}

        Iterator &operator=(const Iterator other) { inner = other.inner; return *this; }

        bool operator!=(const Iterator &rhs) const { return inner != rhs.inner; }
        void operator++() { ++inner; }
        NODE& operator*() { return inner.operator*().operator*().node; }
        typename InnerSetType::const_iterator wrapper() { return inner; }
        const typename InnerSetType::const_iterator wrapper() const { return inner; }
    };

//    using const_iterator = const Iterator; //wrong: const iterator is not a mutable iterator that points to a const element
    using iterator = Iterator;
    using value_type = NODE;


    /**
     * This constructor makes that the set comparator uses the internal NODE (the template
     * parameter) instead of `std::less<shared_ptr>`, because we don't want to store a
     * `set<NODE>` nor a `set<Node>` directly.
     */
    Digrunset() {}

//    Digrunset(LessThanFunctionType lessThan) : nodes(lessThan) {}
//
//    Digrunset(CompareFunctionType userCompare)
//            : nodes([=](PtrNodeWrapper first, PtrNodeWrapper second) {
//        bool equalIsUnused;
//        return userCompare(first, second, equalIsUnused);
//    }), innerCompare(userCompare) {}
//
    Digrunset(const Digrunset &other) {
//            : nodes(other.nodes.key_comp()), innerCompare(other.innerCompare) {
        putGraph(other);
    }
    Digrunset(Digrunset &&other)
            : nodes(std::move(other.nodes)), innerCompare(std::move(other.innerCompare)) {
    }

    Digrunset &operator=(Digrunset other) {
        std::swap(nodes, other.nodes);
        std::swap(innerCompare, other.innerCompare);
        return *this;
    }

    virtual ~Digrunset() {
        clear();
    }

    /**
     * adds all the nodes and connections from other graph, without overwriting the current ones
     */
    void addGraph(const Digrunset &other) {
        for (PtrNodeWrapper node : other.nodes) {
            addNode(node->node);
        }

        for (PtrNodeWrapper node : other.nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                addConnection(node->node, successor->node);
            }
            for (PtrNodeWrapper predecessor : node->predecessors) {
                addConnection(predecessor->node, node->node);
            }
        }
    }


    /**
     * adds all the nodes and connections from other graph, without overwriting the current ones
     */
    void putGraph(const Digrunset &other) {
        std::map<PtrNodeWrapper, PtrNodeWrapper> cache;
        for (PtrNodeWrapper node : other.nodes) {
            PtrNodeWrapper inserted;
            addNode(node->node, inserted);
            cache[node] = inserted;
        }

        for (PtrNodeWrapper node : other.nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                addConnection(cache[node], cache[successor]);
            }
            for (PtrNodeWrapper predecessor : node->predecessors) {
                addConnection(cache[predecessor], cache[node]);
            }
        }
    }

    // ------------- node methods -------------

    bool addNode(const NODE &node) {
        const std::pair<typename InnerSetType::iterator, bool> &inserted =
                nodes.insert(std::make_shared<NodeWrapper>(node));
        return inserted.second;
    }

    bool addNode(const NODE &node, PtrNodeWrapper &inserted) {
        const std::pair<typename InnerSetType::iterator, bool> &insertedPair =
                nodes.insert(std::make_shared<NodeWrapper>(node));
        inserted = insertedPair.first.operator*();
        return insertedPair.second;
    }

    /**
     * @return pair<bool // whether the node was found
     *      shared_ptr<NodeWrapper> // the pointer to element, or nullptr if was not found
     * >
     */
    std::pair<bool, PtrNodeWrapper> findNode(const NODE &node) const {
        auto wrapper = std::make_shared<NodeWrapper>(node);
        auto itSource = nodes.find(wrapper);
        if (itSource != nodes.end()) {
            return {true, *itSource};
        }
        return {false, nullptr};
    };

    /** this is slower than findNodeHint, but don't know why */
    bool findNode(const NODE &node, iterator &whereWasFound) const {
        auto wrapper = std::make_shared<NodeWrapper>(node);
        whereWasFound = Iterator(nodes.find(wrapper));
        return whereWasFound != Iterator(nodes.end());
    }

    /**
     * @param node
     * @return `true` if the node was removed. `false` if the node was not present.
     */
    bool removeNode(std::shared_ptr<NodeWrapper> node) {
        for (std::shared_ptr<NodeWrapper> successor : node->successors) {
            removeConnection(node, successor);
        }
        for (std::shared_ptr<NodeWrapper> predecessor : node->predecessors) {
            removeConnection(predecessor, node);
        }
        bool removed = nodes.erase(node) == 1;
        return removed;
    }


    // ------------- connection methods -------------

    bool addConnection(const NODE &source, const NODE &sink) {
        auto itSource = findNode(source);
        if (itSource.first) {
            auto itSink = findNode(sink);
            if (itSink.first) {
                addConnection(itSource.second, itSink.second);
                return true;
            }
        }
        return false;
    }

    void addConnection(PtrNodeWrapper source, PtrNodeWrapper sink) {
        source->successors.insert(sink);
        sink->predecessors.insert(source);
    }

    std::pair<bool, Connection> findConnection(const NODE &source, const NODE &sink) const {
        auto itSource = findNode(source);
        if (itSource.first) {
            auto itSink = findNode(sink);
            if (itSink.first) {
                return doFindConnection(itSource.second, itSink.second);
            }
        }
        return {false, {nullptr, nullptr}};
    }

    bool removeConnection(std::shared_ptr<NodeWrapper> source, std::shared_ptr<NodeWrapper> sink) {
        bool removed = false;
        auto it = std::find(source->successors.begin(), source->successors.end(), sink);
        if (it != source->successors.end()) {
            source->successors.erase(it);
            removed = true;
        }

        it = std::find(sink->predecessors.begin(), sink->predecessors.end(), source);
        if (it != sink->predecessors.end()) {
            sink->predecessors.erase(it);
            removed = true;
        }
        return removed;
    }

    Iterator begin() {
        return Iterator(nodes.begin());
    }
    Iterator end() {
        return Iterator(nodes.end());
    }
    const Iterator begin() const {
        return Iterator(nodes.cbegin());
    }
    const Iterator end() const {
        return Iterator(nodes.cend());
    }

    void clear() {
        for (PtrNodeWrapper node : nodes) {
            node->successors.clear();
            node->predecessors.clear();
        }
        nodes.clear();
    }

    size_t size() const { return nodes.size(); }

    size_t countConnections() const {
        size_t count = 0;
        for (PtrNodeWrapper node : nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                count++;
            }
        }
        return count;
    }

    /**
     * returns the set difference: nodes that are in this but are not in other.
     * This method loses the relationship among the nodes!
     */
    Digrunset operator-(const Digrunset &other) const {
        Digrunset result;
        for (const PtrNodeWrapper &node : nodes) {
            bool notPresentInOther = other.nodes.find(node) == other.nodes.end();
            if (notPresentInOther) {
                result.nodes.insert(node);
            }
        }
        return result;
    }

    /**
     * returns the set intersection: nodes that are in both this and other.
     * This method loses the relationship among the nodes!
     */
    Digrunset operator^(const Digrunset &other) const {
        Digrunset result;
        for (const PtrNodeWrapper &node : nodes) {
            bool presentInOther = other.nodes.find(node) != other.nodes.end();
            if (presentInOther) {
                result.nodes.insert(node);
            }
        }
        return result;
    }

    void walkForward(std::function<void (PtrNodeWrapper)> nodeFunc,
            std::function<void (PtrNodeWrapper, PtrNodeWrapper)> connectionFunc) const {
        for (PtrNodeWrapper node : nodes) {
            nodeFunc(node);
        }
        for (PtrNodeWrapper node : nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                connectionFunc(node, successor);
            }
        }
    }

    void walk(std::function<void (PtrNodeWrapper)> nodeFunc,
            std::function<void (PtrNodeWrapper, PtrNodeWrapper)> connectionFunc) const {
        for (PtrNodeWrapper node : nodes) {
            nodeFunc(node);
        }
        for (PtrNodeWrapper node : nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                connectionFunc(node, successor);
            }
            for (PtrNodeWrapper predecessor : node->predecessors) {
                connectionFunc(predecessor, node);
            }
        }
    }

    void print (std::ostream &out) const {
        walkForward(
                [&](PtrNodeWrapper node) {
                    printNode(out, node);
                    out << std::endl;
                },
                [&](PtrNodeWrapper source, PtrNodeWrapper sink) {
                    printConnection(out, source, sink);
                    out << std::endl;
                }
        );
    }

    void printNode(std::ostream &out, const std::shared_ptr<NodeWrapper> node) const {
        out << "{\"" << node << "\"[label=\"" << node->node << "\"]}";
    }

    void printConnection(std::ostream &out,
            const PtrNodeWrapper source, const PtrNodeWrapper sink) const {
        printNode(out, source);
        out << " -> ";
        printNode(out, sink);
    }

private:

    std::pair<bool, Connection> doFindConnection(
            std::shared_ptr<NodeWrapper> source, std::shared_ptr<NodeWrapper> sink) const {
        auto successorIt = std::find(
                source->successors.begin(),
                source->successors.end(),
                sink);
        bool successorFound = successorIt != source->successors.end();

        bool doChecks = true;
        if (doChecks) {
            auto predecessorIt = std::find(
                    sink->predecessors.begin(),
                    sink->predecessors.end(),
                    source);
            bool predecessorFound = predecessorIt != sink->predecessors.end();

            if (predecessorFound ^ successorFound) {
                std::stringstream ss;
                ss << "inconsistency found, node " << (predecessorFound ? sink->node : source->node)
                   << " has " << (predecessorFound ? source->node : sink->node)
                   << " linked, but not the other way";
                throw randomize::utils::exception::StackTracedException(ss.str());
            }
        }

        if (successorFound) {
            return {true, {source, sink}};
        }
        return {false, {nullptr, nullptr}};
    }

    InnerSetType nodes;
    CompareFunctionType innerCompare;
};

};  // graph
};  // utils
};  // randomize

#endif //EPISTOMATA_DIGRUNSET_H
