/**
 * @file FirstPathRanker.h
 * @author jmmut
 * @date 2017-02-07.
 */

#ifndef EPISTOMATA_FIRSTPATHRANKER_H
#define EPISTOMATA_FIRSTPATHRANKER_H


#include <memory>
#include "PathRanker.h"

class FirstPathRanker : public PathRanker {
public:
    virtual ~FirstPathRanker() override = default;

    GraphExplorer::Path getBest() override;

    bool shouldContinue() override;

    virtual void pushEdge(const WalkableGraph::Edge edge) override;

    virtual void popEdge() override;

    virtual EdgesIterable getEdges(const WalkableGraph::Node &from) const override;

private:
    std::shared_ptr<GraphExplorer::Path> firstPath;
};


#endif //EPISTOMATA_FIRSTPATHRANKER_H
