/**
 * @file PathRanker.h
 * @author jmmut
 * @date 2017-02-07.
 */

#ifndef EPISTOMATA_PATHRANKER_H
#define EPISTOMATA_PATHRANKER_H

#include <memory>
#include <utils/graphExplorer/GraphExplorer.h>

class PathRanker {
public:
    class EdgesIterable {
    public:
        EdgesIterable(const WalkableGraph::Node &node, const WalkableGraph::Edge &lastEdge,
                std::string agent)
                : changes(node.subcontext().at(Concept(agent)).changes()), lastChange(lastEdge),
                lastChangeAvailable(true), agent(agent) {}

        EdgesIterable(const WalkableGraph::Node &node, std::string agent)
                : changes(node.subcontext().at(Concept(agent)).changes()), lastChange(nullptr),
                lastChangeAvailable(false), agent(agent) {}

        Changes::iterator begin() {
            if (not isLastChangeAvailable()) {
                return changes.begin();
            } else {
                Changes::iterator where;
                bool found = changes.findNode(lastChange, where);
                if (found and where.wrapper().operator*()->successors.size() > 0) {
                    return Changes::Iterator(where.wrapper().operator*()->successors.begin());
                } else {
                    return changes.begin();
                }
            }
        }

        Changes::iterator end() {
            if (not isLastChangeAvailable()) {
                return changes.end();
            } else {
                Changes::iterator where;
                bool found = changes.findNode(lastChange, where);
                if (found and where.wrapper().operator*()->successors.size() > 0) {
                    return Changes::Iterator(where.wrapper().operator*()->successors.end());
                } else {
                    return changes.end();
                }
            }
        }
    private:
        const Changes &changes;
        const WalkableGraph::Edge &lastChange;
        const bool lastChangeAvailable;
        const std::string &agent;

        bool isLastChangeAvailable() { return lastChangeAvailable and lastChange != nullptr; }
    };

    virtual void pushEdge(const WalkableGraph::Edge edge) = 0;
    virtual void popEdge() = 0;
    virtual void processPath() = 0;

    virtual EdgesIterable getEdges(const WalkableGraph::Node &from) const = 0;
    virtual bool shouldContinue() = 0;
    virtual GraphExplorer::Path getBest() = 0;

    virtual size_t getPathsTried() = 0;
    virtual size_t getPathChanges() = 0;

    virtual ~PathRanker() = default;

};

#endif //EPISTOMATA_PATHRANKER_H
