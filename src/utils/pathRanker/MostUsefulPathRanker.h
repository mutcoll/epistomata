/**
 * @file MostUsefulPathRanker.h
 * @author jmmut
 * @date 2017-03-26.
 */

#ifndef EPISTOMATA_MOSTUSEFULPATHRANKER_H
#define EPISTOMATA_MOSTUSEFULPATHRANKER_H


#include <list>
#include "PathRanker.h"
#include "MostCertainPathRanker.h"

static const double ABSOLUTE_ACCOMPLISHMENT = 1.0;

class MostUsefulPathRanker : public MostCertainPathRanker {
public:
    MostUsefulPathRanker(const std::string &agent, const WalkableGraph::Node &node,
            const WalkableGraph::Node &goal);

    virtual ~MostUsefulPathRanker() override = default;

    virtual void pushEdge(const WalkableGraph::Edge edge) override;

    virtual void popEdge() override;

    virtual void processPath() override;

    virtual EdgesIterable getEdges(const WalkableGraph::Node &from) const override;

    virtual bool shouldContinue() override;

    virtual GraphExplorer::Path getBest() override;


    virtual size_t getPathsTried() override;
    virtual size_t getPathChanges() override;

private:
    const std::string &agent;
    const WalkableGraph::Node &start;
    const WalkableGraph::Node &goal;
    WalkableGraph::Node difference;

    GraphExplorer::Path best;
    std::vector<GraphExplorer::Path> currents;

    size_t pathsOverwritten;
    size_t pathsTried;

    double getComparableAccomplishment(double accomplishment, size_t size);
    double getAccomplishment(const GraphExplorer::Path &path);

    double getUsefulness(double certainty, double accomplishment) const;

    void keepBest();

    std::shared_ptr<Change> joinChanges(const WalkableGraph::Edge &first,
            const WalkableGraph::Edge &second) const;

    double getComparableUsefulness(
            const GraphExplorer::Path &path, const size_t secondDuration);

    void ensureAccumulated();
};


#endif //EPISTOMATA_MOSTUSEFULPATHRANKER_H
