/**
 * @file FirstPathRanker.cpp
 * @author jmmut
 * @date 2017-02-07.
 */

#include "FirstPathRanker.h"


GraphExplorer::Path FirstPathRanker::getBest() {
    return *firstPath;
}

bool FirstPathRanker::shouldContinue() {
    return firstPath == nullptr;
}

void FirstPathRanker::pushEdge(const WalkableGraph::Edge edge) {

}

void FirstPathRanker::popEdge() {

}

PathRanker::EdgesIterable FirstPathRanker::getEdges(const WalkableGraph::Node &from) const {
    return EdgesIterable(from, "");
}
