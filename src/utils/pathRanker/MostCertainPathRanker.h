/**
 * @file FirstPathRanker.h
 * @author jmmut
 * @date 2017-02-07.
 */

#ifndef EPISTOMATA_MOSTCERTAINPATHRANKER_H
#define EPISTOMATA_MOSTCERTAINPATHRANKER_H


#include <memory>
#include "PathRanker.h"

class MostCertainPathRanker : public PathRanker {
public:
    virtual ~MostCertainPathRanker() override = default;

    virtual GraphExplorer::Path getBest() override;

    virtual bool shouldContinue() override;

    virtual void pushEdge(const WalkableGraph::Edge edge) override;

    virtual void popEdge() override;

    virtual void processPath() override;

    virtual EdgesIterable getEdges(const WalkableGraph::Node &from) const override;

    static double getCertainty(std::shared_ptr<GraphExplorer::Path> path);
    static double getCertainty(const GraphExplorer::Path &path);

    virtual size_t getPathsTried() override;
    virtual size_t getPathChanges() override { return 0; };

    static std::shared_ptr<GraphExplorer::Path> getMostCertainPath(
            std::shared_ptr<GraphExplorer::Path> first,
            std::shared_ptr<GraphExplorer::Path> second);
    /**
     * Probability that an experiment X with p(X) = `firstCertainty`, will succeed at least
     * once if repeated `secondDuration` times.
     */
    static double getComparableCertainty(double firstCertainty, size_t secondDuration);

protected:

    std::shared_ptr<GraphExplorer::Path> mostCertainPath = nullptr;
    double certainty = 0;

    size_t pathsTried;
};


static const double ABSOLUTE_CERTAINTY = 1.0;
#endif //EPISTOMATA_MOSTCERTAINPATHRANKER_H
