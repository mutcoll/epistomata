/**
 * @file MostUsefulPathRanker.cpp
 * @author jmmut
 * @date 2017-03-26.
 */

#include "MostUsefulPathRanker.h"
#include "io/JsonContextPrinter.h"

MostUsefulPathRanker::MostUsefulPathRanker(const std::string &agent,
        const WalkableGraph::Node &start, const WalkableGraph::Node &goal)
        : agent(agent), start(start), goal(goal) {
    difference = goal - start;
    best.valid = false;
    pathsTried = 0;
    pathsOverwritten = 0;
}

void MostUsefulPathRanker::pushEdge(const WalkableGraph::Edge edge) {
    currents.emplace_back();
    if (currents.size() > 1) {
        GraphExplorer::Path &previousPath = *(--(--currents.end()));
        currents.back().edges.assign(previousPath.edges.begin(), previousPath.edges.end());
    }
    currents.back().edges.push_back(copyChange(edge.operator*()));
    currents.back().accumulated = nullptr;
    currents.back().valid = false;
}

std::shared_ptr<Change> MostUsefulPathRanker::joinChanges(const WalkableGraph::Edge &first,
        const WalkableGraph::Edge &second) const {
    Context difference(second->difference);
    difference += first->difference;
    double certainty = second->certainty.get() * first->certainty.get();
    return std::make_shared<Change>(Properties::UNKNOWN, std::move(difference), certainty);
}

void MostUsefulPathRanker::popEdge() {
    if (currents.size() > 0) {
        currents.pop_back();
    }
}

void MostUsefulPathRanker::processPath() {
    ensureAccumulated();
    keepBest();
}

void MostUsefulPathRanker::ensureAccumulated() {
    auto previousIt = currents.begin();
    for (auto pathIt = currents.begin(); pathIt != currents.end(); ++pathIt) {
        if (pathIt == currents.begin()) {
            if (pathIt->accumulated == nullptr) {
                pathIt->accumulated = pathIt->edges.back();
            }
        } else {
            if (pathIt->accumulated == nullptr) {
                pathIt->accumulated = joinChanges(previousIt->accumulated, pathIt->edges.back());
            }
        }
        previousIt = pathIt;
    }
    if (currents.size() == 0) {
        throw randomize::utils::exception::StackTracedException(
                "logic error: the path should not be empty");
    }
    GraphExplorer::Path &path = currents.back();
    if (not path.valid) {
        path.valid = true;
        path.accomplishment = difference.fulfillmentBy(path.accumulated->difference);
        path.certainty = path.accumulated->certainty.get();
        //    currents.back().achievedNode = currents.back().accumulated->difference + start;
    }
}

void MostUsefulPathRanker::keepBest() {
    bool overwrite;

    if (best.valid) {
        double newUsefulness = getComparableUsefulness(currents.back(), best.edges.size());
        double bestUsefulness = getComparableUsefulness(best, currents.back().edges.size());
        overwrite = newUsefulness > bestUsefulness;
    } else {
        overwrite = true;
    }

    ++pathsTried;
    if (overwrite) {
        LOG_DEBUG("found better plan: %s, %f certainty, %f accomplishment",
                changesTraitsToString(currents.back().edges).c_str(), currents.back().certainty,
                currents.back().accomplishment);
        ++pathsOverwritten;
        best = currents.back();
    }
}

double MostUsefulPathRanker::getComparableUsefulness(
        const GraphExplorer::Path &path, const size_t secondDuration) {

    return getUsefulness(
            getComparableCertainty(path.certainty, secondDuration),
            getComparableAccomplishment(path.accomplishment, secondDuration)
    );
}

double MostUsefulPathRanker::getUsefulness(double certainty, double accomplishment) const {
    return accomplishment * certainty;
}

bool MostUsefulPathRanker::shouldContinue() {
    return not best.valid
            or getUsefulness(best.accomplishment, best.certainty) < ABSOLUTE_ACCOMPLISHMENT;
}

GraphExplorer::Path MostUsefulPathRanker::getBest() {
    return best;
}

double MostUsefulPathRanker::getComparableAccomplishment(double firstAccomplishment,
        size_t secondDuration) {
    return firstAccomplishment * secondDuration;
}

double MostUsefulPathRanker::getAccomplishment(const GraphExplorer::Path &path) {
    Context collapsedChanges;
    for (auto &&edge : path.edges) {
        collapsedChanges += edge->difference;
    }
    return (goal - path.achievedNode).fulfillmentBy(collapsedChanges);
}

PathRanker::EdgesIterable MostUsefulPathRanker::getEdges(const WalkableGraph::Node &from) const {
    if (currents.size() > 0) {
        return EdgesIterable(from, currents.back().edges.back(), agent);
    } else {
        return EdgesIterable(from, agent);
    }
}

size_t MostUsefulPathRanker::getPathsTried() {
    return pathsTried;
}

size_t MostUsefulPathRanker::getPathChanges() {
    return pathsOverwritten;
}

