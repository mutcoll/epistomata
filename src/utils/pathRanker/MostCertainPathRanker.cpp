/**
 * @file FirstPathRanker.cpp
 * @author jmmut
 * @date 2017-02-07.
 */

#include <cmath>
#include "MostCertainPathRanker.h"

GraphExplorer::Path MostCertainPathRanker::getBest() {
    LOG_DEBUG("getting best path with certainty %f", getCertainty(mostCertainPath));
    return *mostCertainPath;
}

bool MostCertainPathRanker::shouldContinue() {
    return certainty < ABSOLUTE_CERTAINTY;
}

double MostCertainPathRanker::getCertainty(std::shared_ptr<GraphExplorer::Path> path) {
    if (path != nullptr) {
        return getCertainty(*path);
    } else {
        return 0;
    }
}

double MostCertainPathRanker::getCertainty(const GraphExplorer::Path &path) {
    double totalCertainty = 1;
    for (auto &&change : path.edges) {
        totalCertainty *= change->certainty.get();
    }
    return totalCertainty;
}

std::shared_ptr<GraphExplorer::Path> MostCertainPathRanker::getMostCertainPath(
        std::shared_ptr<GraphExplorer::Path> first, std::shared_ptr<GraphExplorer::Path> second) {
    if (first == nullptr) {
        return second;
    }
    if (second == nullptr) {
        return first;
    }

    size_t firstDuration = first->edges.size();
    size_t secondDuration = second->edges.size();
    double firstComparableCertainty = getComparableCertainty(getCertainty(first), secondDuration);
    double secondComparableCertainty = getComparableCertainty(getCertainty(second), firstDuration);

    if (firstComparableCertainty > secondComparableCertainty) {
        return first;
    } else if (firstComparableCertainty < secondComparableCertainty) {
        return second;
    } else {
        return firstDuration <= secondDuration ? first : second;
    }
}

double MostCertainPathRanker::getComparableCertainty(double firstCertainty, size_t secondDuration) {
    double firstFailureCertainty = 1 - firstCertainty;
    double failureProbability = 1;
    for (size_t i = 0; i < secondDuration; ++i) {
        failureProbability *= firstFailureCertainty;
    }
    return 1 - failureProbability;
}

void MostCertainPathRanker::pushEdge(const WalkableGraph::Edge edge) {

}


void MostCertainPathRanker::popEdge() {

}

PathRanker::EdgesIterable MostCertainPathRanker::getEdges(const WalkableGraph::Node &from) const {
    return EdgesIterable(from, "");
}

size_t MostCertainPathRanker::getPathsTried() {
    return pathsTried;
}

void MostCertainPathRanker::processPath() {

}


