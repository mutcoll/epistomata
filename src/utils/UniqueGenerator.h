/**
 * @file UniqueGenerator.h
 * @author jmmut
 * @date 2016-12-17.
 *
 * This template can be used to iterate a container with repeated elements, and only provide each
 * element once.
 *
 * It requires:
 * - on the container:
 *   - begin() and end() providers of iterators
 * - on the inner iterator:
 *   - operator++, operator!=, operator* as usual in an inerator
 *   - typedef "iterator"
 *   - operator< to be able to sort inside a set of unique elements
 *
 */

#ifndef EPISTOMATA_UNIQUEGENERATOR_H
#define EPISTOMATA_UNIQUEGENERATOR_H

#include <set>

template<typename T>
class UniqueGenerator {
    T &collection;
public:
    UniqueGenerator(T &collection) : collection(collection) {}

    class IteratorUniqueGenerator {
        typename T::iterator iter, current;
        std::set<typename T::iterator> elements;

    public:
        explicit IteratorUniqueGenerator(typename T::iterator it) : iter(it),current(it) {}

        void operator++();
        typename T::iterator operator*();
        bool operator!=(IteratorUniqueGenerator &other);
    };
    IteratorUniqueGenerator begin() { return IteratorUniqueGenerator(collection.begin()); }
    IteratorUniqueGenerator end() { return IteratorUniqueGenerator(collection.end()); }

};


template<typename T>
void UniqueGenerator<T>::IteratorUniqueGenerator::operator++() {
}

template<typename T>
typename T::iterator UniqueGenerator<T>::IteratorUniqueGenerator::operator*() {
    return current;
}

template<typename T>
bool UniqueGenerator<T>::IteratorUniqueGenerator::operator!=(
        UniqueGenerator::IteratorUniqueGenerator &other) {
    bool inserted = false, notFinished;
    typename decltype(elements)::iterator unused;

    while (not inserted and (notFinished = iter != other.iter)) {
        current = iter;
        std::tie(unused, inserted) = elements.insert(current);
        ++iter;
    }
    return notFinished;
}


#endif //EPISTOMATA_UNIQUEGENERATOR_H
