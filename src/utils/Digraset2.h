#ifndef EPISTOMATA_DIGRASET2_H
#define EPISTOMATA_DIGRASET2_H

#include <memory>
#include <vector>
#include <algorithm>
#include <sstream>
#include <set>
#include <tuple>
#include "utils/exception/StackTracedException.h"

namespace randomize {
namespace utils {
namespace graph {

/**
 * @class Digraset
 *
 * This class is a simple directed graph.
 *
 * It stores a set of nodes, so that it is easy to know if it's present in the graph or not, and
 * then query its neighbours.
 */
template<typename NODE>
class Digraset {
public:
    struct NodeWrapper {
        NodeWrapper(const NODE &node,
                const std::set<std::shared_ptr<NodeWrapper>> &predecessors = {},
                const std::set<std::shared_ptr<NodeWrapper>> &successors = {})
                : node(node), predecessors(predecessors), successors(successors) { }

        NODE node;
        std::set<std::shared_ptr<NodeWrapper>> predecessors;
        std::set<std::shared_ptr<NodeWrapper>> successors;
    };

    using PtrNodeWrapper = std::shared_ptr<NodeWrapper>;
    using Connection = std::pair<PtrNodeWrapper, PtrNodeWrapper>;

    using LessThanFunctionType = std::function<bool (PtrNodeWrapper, PtrNodeWrapper)>;
    using CompareFunctionType = std::function<bool (PtrNodeWrapper, PtrNodeWrapper, bool & equal)>;
    using InnerSetType = std::set<PtrNodeWrapper, LessThanFunctionType>;

    struct Iterator {
        typename InnerSetType::iterator inner;

        Iterator() : inner(nullptr) {}
        Iterator(const typename InnerSetType::iterator &other) : inner(other) {}

        Iterator &operator=(const Iterator other) { inner = other.inner; return *this; }

        bool operator!=(const Iterator &rhs) const { return inner != rhs.inner; }
        void operator++() { ++inner; }
        NODE& operator*() { return inner.operator*().operator*().node; }
        typename InnerSetType::iterator wrapper() { return inner; }
        const typename InnerSetType::iterator wrapper() const { return inner; }
    };

//    using const_iterator = const Iterator; //wrong: const iterator is not a mutable iterator that points to a const element
    using iterator = Iterator;
    using value_type = NODE;


    static bool defaultLessThan(PtrNodeWrapper first, PtrNodeWrapper second) {
        return first->node < second->node;
    }

    /**
     * This constructor makes that the set comparator uses the internal NODE (the template
     * parameter) instead of `std::less<shared_ptr>`, because we don't want to store a
     * `set<NODE>` nor a `set<Node>` directly.
     */
    Digraset() : nodes(defaultLessThan) {}

    Digraset(LessThanFunctionType lessThan) : nodes(lessThan) {}

    Digraset(CompareFunctionType userCompare)
            : nodes([=](PtrNodeWrapper first, PtrNodeWrapper second) {
        bool equalIsUnused;
        return userCompare(first, second, equalIsUnused);
    }), innerCompare(userCompare) {}

    Digraset(const Digraset &other)
            : nodes(other.nodes.key_comp()), innerCompare(other.innerCompare) {
        putGraph(other);
    }
    Digraset(Digraset &&other)
            : nodes(std::move(other.nodes)), innerCompare(std::move(other.innerCompare)) {
    }

    Digraset &operator=(Digraset other) {
        std::swap(nodes, other.nodes);
        std::swap(innerCompare, other.innerCompare);
        return *this;
    }

    virtual ~Digraset() {
        clear();
    }

    /**
     * adds all the nodes and connections from other graph, without overwriting the current ones
     */
    void addGraph(const Digraset &other) {
        for (PtrNodeWrapper node : other.nodes) {
            addNode(node->node);
        }

        for (PtrNodeWrapper node : other.nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                addConnection(node->node, successor->node);
            }
            for (PtrNodeWrapper predecessor : node->predecessors) {
                addConnection(predecessor->node, node->node);
            }
        }
    }


    /**
     * adds all the nodes and connections from other graph, without overwriting the current ones
     */
    void putGraph(const Digraset &other) {
        std::map<PtrNodeWrapper, PtrNodeWrapper> cache;
        for (PtrNodeWrapper node : other.nodes) {
            PtrNodeWrapper inserted;
            addNode(node->node, inserted);
            cache[node] = inserted;
        }

        for (PtrNodeWrapper node : other.nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                addConnection(cache[node], cache[successor]);
            }
            for (PtrNodeWrapper predecessor : node->predecessors) {
                addConnection(cache[predecessor], cache[node]);
            }
        }
    }

    // ------------- node methods -------------

    bool addNode(const NODE &node) {
        const std::pair<iterator, bool> &inserted =
                nodes.insert(std::make_shared<NodeWrapper>(node));
        return inserted.second;
    }

    bool addNode(const NODE &node, PtrNodeWrapper &inserted) {
        const std::pair<typename InnerSetType::iterator, bool> &insertedPair =
                nodes.insert(std::make_shared<NodeWrapper>(node));
        inserted = insertedPair.first.operator*();
        return insertedPair.second;
    }

    /**
     * @return `pair<bool, shared_ptr<NodeWrapper>>` whether the node was found
     *      and  the pointer to element, or nullptr if was not found
     */
    std::pair<bool, PtrNodeWrapper> findNode(const NODE &node) const {
        auto wrapper = std::make_shared<NodeWrapper>(node);
        auto itSource = nodes.find(wrapper);
        if (itSource != nodes.end()) {
            return {true, *itSource};
        }
        return {false, nullptr};
    };

    /** this is slower than findNodeHint, but don't know why */
    bool findNode(const NODE &node, iterator &whereWasFound) const {
        auto wrapper = std::make_shared<NodeWrapper>(node);
        whereWasFound = nodes.find(wrapper);
        return whereWasFound != nodes.end();
    }

    bool findNodeHint(const NODE &node, iterator &whereWasFound) const {
        PtrNodeWrapper wrapper = std::make_shared<NodeWrapper>(node);
        whereWasFound = Iterator(nodes.lower_bound(wrapper));
        bool equal = false;
        if (innerCompare and whereWasFound != nodes.end()) {
            innerCompare(wrapper, whereWasFound.wrapper().operator*(), equal);
        }
        return equal;
    };


    /**
     * @param node
     * @return `true` if the node was removed. `false` if the node was not present.
     */
    bool removeNode(std::shared_ptr<NodeWrapper> node) {
        for (std::shared_ptr<NodeWrapper> successor : node->successors) {
            removeConnection(node, successor);
        }
        for (std::shared_ptr<NodeWrapper> predecessor : node->predecessors) {
            removeConnection(predecessor, node);
        }
        bool removed = nodes.erase(node) == 1;
        return removed;
    }


    // ------------- connection methods -------------

    bool addConnection(const NODE &source, const NODE &sink) {
        auto itSource = findNode(source);
        if (itSource.first) {
            auto itSink = findNode(sink);
            if (itSink.first) {
                addConnection(itSource.second, itSink.second);
                return true;
            }
        }
        return false;
    }

    void addConnection(PtrNodeWrapper source, PtrNodeWrapper sink) {
        source->successors.insert(sink);
        sink->predecessors.insert(source);
    }

    std::pair<bool, Connection> findConnection(const NODE &source, const NODE &sink) const {
        auto itSource = findNode(source);
        if (itSource.first) {
            auto itSink = findNode(sink);
            if (itSink.first) {
                return doFindConnection(itSource.second, itSink.second);
            }
        }
        return {false, {nullptr, nullptr}};
    }

    bool removeConnection(std::shared_ptr<NodeWrapper> source, std::shared_ptr<NodeWrapper> sink) {
        bool removed = false;
        auto it = std::find(source->successors.begin(), source->successors.end(), sink);
        if (it != source->successors.end()) {
            source->successors.erase(it);
            removed = true;
        }

        it = std::find(sink->predecessors.begin(), sink->predecessors.end(), source);
        if (it != sink->predecessors.end()) {
            sink->predecessors.erase(it);
            removed = true;
        }
        return removed;
    }

    Iterator begin() {
        return Iterator(nodes.begin());
    }
    Iterator end() {
        return Iterator(nodes.end());
    }
    const Iterator begin() const {
        return Iterator(nodes.cbegin());
    }
    const Iterator end() const {
        return Iterator(nodes.cend());
    }

    void clear() {
        for (PtrNodeWrapper node : nodes) {
            node->successors.clear();
            node->predecessors.clear();
        }
        nodes.clear();
    }

    size_t size() const { return nodes.size(); }

    size_t countConnections() const {
        size_t count = 0;
        for (PtrNodeWrapper node : nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                count++;
            }
        }
        return count;
    }

    /**
     * returns the set difference: nodes that are in this but are not in other.
     * This method loses the relationship among the nodes!
     */
    Digraset operator-(const Digraset &other) const {
        auto thisBegin = begin();
        auto thisEnd = end();
        auto otherBegin = other.begin();
        auto otherEnd = other.end();
        Digraset result;

        while (thisBegin != thisEnd && otherBegin != otherEnd) {
            if (nodes.key_comp()(thisBegin.wrapper().operator*(),
                    otherBegin.wrapper().operator*())) {
                // present in this, not in other: store
                result.addNode(thisBegin.operator*());
                ++thisBegin;
            } else if (nodes.key_comp()(otherBegin.wrapper().operator*(),
                     thisBegin.wrapper().operator*())) {
                // present in other, not in this: ignore
                ++otherBegin;
            } else {
                // present in both this and other: ignore
                ++thisBegin;
                ++otherBegin;
            }
        }

        // we reached at least the end of other, copy the rest of this
        for (; thisBegin != thisEnd; ++thisBegin) {
            result.addNode(thisBegin.operator*());
        }
        return result;
    }

    /**
     * returns the set intersection: nodes that are in both this and other.
     * This method loses the relationship among the nodes!
     */
    Digraset operator^(const Digraset &other) const {
        auto thisBegin = begin();
        auto thisEnd = end();
        auto otherBegin = other.begin();
        auto otherEnd = other.end();
        Digraset result;
        if (innerCompare) {
            bool equal;
            while (thisBegin != thisEnd && otherBegin != otherEnd) {
                bool lessThan = innerCompare(thisBegin.wrapper().operator*(),
                        otherBegin.wrapper().operator*(), equal);
                if (lessThan) {
                    // present in this, not in other: ignore
                    ++thisBegin;
                } else if (not equal) {
                    // present in other, not in this: ignore
                    ++otherBegin;
                } else {
                    // present in both this and other: store
                    result.addNode(thisBegin.operator*());
                    ++thisBegin;
                    ++otherBegin;
                }
            }
        } else {
            while (thisBegin != thisEnd && otherBegin != otherEnd) {
                if (nodes.key_comp()(thisBegin.wrapper().operator*(),
                        otherBegin.wrapper().operator*())) {
                    // present in this, not in other: ignore
                    ++thisBegin;
                } else if (nodes.key_comp()(otherBegin.wrapper().operator*(),
                        thisBegin.wrapper().operator*())) {
                    // present in other, not in this: ignore
                    ++otherBegin;
                } else {
                    // present in both this and other: store
                    result.addNode(thisBegin.operator*());
                    ++thisBegin;
                    ++otherBegin;
                }
            }
        }
        return result;
    }

    /**
     * returns whether this is less than other. This can be used to order digrasets.
     */
    bool operator<(const Digraset &other) const {
        bool equal;
        return compare(other, equal);
    }

    /**
     * returns whether this is less than other, and whether they are equal.
     * This can be used to order digrasets.
     */
    bool compare(const Digraset &other, bool &equal) const {
        equal = false;
        if (this->size() < other.size()) {
            return true;
        } else if (this->size() > other.size()) {
            return false;
        } else {
            // return as lesser the one with the first lexicographical node
            auto thisBegin = begin();
            auto thisEnd = end();
            auto otherBegin = other.begin();
            auto otherEnd = other.end();

            while (thisBegin != thisEnd && otherBegin != otherEnd) {
                if (nodes.key_comp()(thisBegin.wrapper().operator*(),
                        otherBegin.wrapper().operator*())) {
                    // present in this, not in other: lesser
                    return true;
                } else if (nodes.key_comp()(otherBegin.wrapper().operator*(),
                        thisBegin.wrapper().operator*())) {
                    // present in other, not in this: greater
                    return false;
                } else {
                    // present in both this and other: continue comparing
                    ++thisBegin;
                    ++otherBegin;
                }
            }
            equal = true;
            return false;
        }
    }

    void walkForward(std::function<void (PtrNodeWrapper)> nodeFunc,
            std::function<void (PtrNodeWrapper, PtrNodeWrapper)> connectionFunc) const {
        for (PtrNodeWrapper node : nodes) {
            nodeFunc(node);
        }
        for (PtrNodeWrapper node : nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                connectionFunc(node, successor);
            }
        }
    }

    void walk(std::function<void (PtrNodeWrapper)> nodeFunc,
            std::function<void (PtrNodeWrapper, PtrNodeWrapper)> connectionFunc) const {
        for (PtrNodeWrapper node : nodes) {
            nodeFunc(node);
        }
        for (PtrNodeWrapper node : nodes) {
            for (PtrNodeWrapper successor : node->successors) {
                connectionFunc(node, successor);
            }
            for (PtrNodeWrapper predecessor : node->predecessors) {
                connectionFunc(predecessor, node);
            }
        }
    }

    void print (std::ostream &out) const {
        walkForward(
                [&](PtrNodeWrapper node) {
                    printNode(out, node);
                    out << std::endl;
                },
                [&](PtrNodeWrapper source, PtrNodeWrapper sink) {
                    printConnection(out, source, sink);
                    out << std::endl;
                }
        );
    }

    void printNode(std::ostream &out, const std::shared_ptr<NodeWrapper> node) const {
        out << "{\"" << node << "\"[label=\"" << node->node << "\"]}";
    }

    void printConnection(std::ostream &out,
            const PtrNodeWrapper source, const PtrNodeWrapper sink) const {
        printNode(out, source);
        out << " -> ";
        printNode(out, sink);
    }

private:

    std::pair<bool, Connection> doFindConnection(
            std::shared_ptr<NodeWrapper> source, std::shared_ptr<NodeWrapper> sink) const {
        auto successorIt = std::find(
                source->successors.begin(),
                source->successors.end(),
                sink);
        bool successorFound = successorIt != source->successors.end();

        bool doChecks = true;
        if (doChecks) {
            auto predecessorIt = std::find(
                    sink->predecessors.begin(),
                    sink->predecessors.end(),
                    source);
            bool predecessorFound = predecessorIt != sink->predecessors.end();

            if (predecessorFound ^ successorFound) {
                std::stringstream ss;
                ss << "inconsistency found, node " << (predecessorFound ? sink->node : source->node)
                   << " has " << (predecessorFound ? source->node : sink->node)
                   << " linked, but not the other way";
                throw randomize::utils::exception::StackTracedException(ss.str());
            }
        }

        if (successorFound) {
            return {true, {source, sink}};
        }
        return {false, {nullptr, nullptr}};
    }

    InnerSetType nodes;
    CompareFunctionType innerCompare;
};

};  // graph
};  // utils
};  // randomize

#endif //EPISTOMATA_DIGRASET2_H
