/**
 * @file IterativeDepthCertaintyGraphExplorer.cpp
 * @author jmmut
 * @date 2017-02-06.
 */

#include "IterativeDepthCertaintyGraphExplorer.h"
#include "utils/time/TimeRecorder.h"

IterativeDepthCertaintyGraphExplorer::IterativeDepthCertaintyGraphExplorer(
        const WalkableGraph &graph, PathRanker &pathRanker)
        : GraphExplorer(graph), pathRanker(pathRanker) {}

GraphExplorer::Path IterativeDepthCertaintyGraphExplorer::findPath(
        const WalkableGraph::Node &startParameter) {

    int maxRecursionLevel = 6;
    navigations = 0;
    randomize::utils::time::TimeRecorder timeRecorder;
    timeRecorder.start();
    for (int i = 0; i < maxRecursionLevel and pathRanker.shouldContinue(); ++i) {
        LOG_DEBUG("starting plan with %d steps", i + 1);
        recursionLevel = i;
        findPathRecursively(startParameter);
    }
    timeRecorder.stop();
    LOG_DEBUG("%lu navigations and %lu paths tried (changed mind %lu times) in %s", navigations,
            pathRanker.getPathsTried(), pathRanker.getPathChanges(),
            timeRecorder.toString().c_str());

    GraphExplorer::Path result(pathRanker.getBest());
    if (result.valid) {
        return result;
    }

    Path noPath;
    noPath.valid = false;
    return noPath;

}

void IterativeDepthCertaintyGraphExplorer::findPathRecursively(const WalkableGraph::Node &start) {
    if (recursionLevel >= 0) {
        recursiveCase(start);
    } else {
        baseCase();
    }
}

void IterativeDepthCertaintyGraphExplorer::recursiveCase(const WalkableGraph::Node &start) {
    if (randomize::log::log_level == LOG_VERBOSE_LEVEL) {
        std::vector<WalkableGraph::Edge> copy;
        for (const auto &edge : pathRanker.getEdges(start)) {
            copy.push_back(edge);
        }
        std::string s = changesTraitsToString(copy);
//        LOG_VERBOSE("%s", s.c_str());
        LOG_VERBOSE("%lu", copy.size());
        copy.clear();
    }
    for (auto edge : pathRanker.getEdges(start)) {
        bool applicable;
        ++navigations;
        WalkableGraph::Node hypotheticalNode = graph.navigate(start, edge, applicable);
        if (applicable) {
            pathRanker.pushEdge(edge);
            --recursionLevel;

            LOG_VERBOSE("try %s", edge->trait.c_str());
            findPathRecursively(hypotheticalNode);
            LOG_VERBOSE("pop");
            ++recursionLevel;
            if (not pathRanker.shouldContinue()) {
                return;
            }
            pathRanker.popEdge();
        }
    }
}

void IterativeDepthCertaintyGraphExplorer::baseCase() {
    pathRanker.processPath();
}
