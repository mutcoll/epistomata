/**
 * @file IterativeDepthCertaintyGraphExplorer.h
 * @author jmmut
 * @date 2017-02-06.
 */

#ifndef EPISTOMATA_ITERATIVEDEPTHCERTAINTYGRAPHEXPLORER_H
#define EPISTOMATA_ITERATIVEDEPTHCERTAINTYGRAPHEXPLORER_H



#include "GraphExplorer.h"
#include "utils/pathRanker/PathRanker.h"

class IterativeDepthCertaintyGraphExplorer : public GraphExplorer {
public:
    IterativeDepthCertaintyGraphExplorer(const WalkableGraph &graph, PathRanker &pathRanker);

    GraphExplorer::Path findPath(const WalkableGraph::Node &start) override;

    virtual ~IterativeDepthCertaintyGraphExplorer() override = default;

private:
    void findPathRecursively(const WalkableGraph::Node &start);
    void recursiveCase(const WalkableGraph::Node &start);

    PathRanker &pathRanker;

    int recursionLevel;
    size_t navigations;

    void baseCase();
};


#endif //EPISTOMATA_ITERATIVEDEPTHCERTAINTYGRAPHEXPLORER_H
