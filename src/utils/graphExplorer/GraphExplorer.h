/**
 * @file GraphExplorer.h
 * @author jmmut
 * @date 2017-02-05.
 */

#ifndef EPISTOMATA_GRAPHEXPLORER_H
#define EPISTOMATA_GRAPHEXPLORER_H

#include "WalkableGraph.h"

class GraphExplorer {
public:
    struct Path {
        std::vector<WalkableGraph::Edge> edges;
        WalkableGraph::Edge accumulated;
        WalkableGraph::Node achievedNode;
        bool valid = false;
        double certainty, accomplishment;
    };

    GraphExplorer(const WalkableGraph& graph) : graph(graph) {}
    virtual Path findPath(const WalkableGraph::Node &start) = 0;
    virtual ~GraphExplorer() = default;

protected:
    const WalkableGraph& graph;
};


#endif //EPISTOMATA_GRAPHEXPLORER_H
