/**
 * @file WalkableGraph.cpp
 * @author jmmut
 * @date 2017-02-05.
 */

#include "WalkableGraph.h"

WalkableGraph::WalkableGraph(const std::string &agent) : agent(agent) {}

WalkableGraph::Node WalkableGraph::navigate(const WalkableGraph::Node &from,
        WalkableGraph::Edge through, bool &applicable) const {
    return through->applyInIfPositive(from, applicable);
}

void WalkableGraph::navigateModifying(WalkableGraph::Node &from,
        WalkableGraph::Edge through, bool &applicable) const {
    return through->applyTo(from, applicable);
}

bool WalkableGraph::isApplicable(const WalkableGraph::Node &from,
        WalkableGraph::Edge through) const {
    return conceptsAreSubsetOf(through->objects, from.concepts());
}


