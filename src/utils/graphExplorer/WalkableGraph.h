/**
 * @file WalkableGraph.h
 * @author jmmut
 * @date 2017-02-05.
 */

#ifndef EPISTOMATA_WALKABLEGRAPH_H
#define EPISTOMATA_WALKABLEGRAPH_H

#include <memory/Context.h>
#include <memory/Change.h>

class WalkableGraph {
public:
    typedef Context Node;
    typedef std::shared_ptr<Change> Edge;

    WalkableGraph(const std::string &agent);

    WalkableGraph::Node navigate(const WalkableGraph::Node &from,
            WalkableGraph::Edge through, bool &applicable) const;
    void navigateModifying(WalkableGraph::Node &from,
            WalkableGraph::Edge through, bool &applicable) const;
    bool isApplicable(const WalkableGraph::Node &from, WalkableGraph::Edge through) const;

private:
    std::string agent;
};


#endif //EPISTOMATA_WALKABLEGRAPH_H
