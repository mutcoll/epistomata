/**
 * @file Map.cpp
 * @author jmmut
 * @date 2016-09-30.
 */

#include <iostream>
#include "Map.h"


void Map::insertNamedPositions() {
    PositionIterator iterator(sizeX, sizeY);
    for (size_t i = 0; i < positions.size(); ++i) {
        std::stringstream name;
        auto coords = iterator.getCoordinates(i);
        name << Position::genericName << "_" << coords.first << "_" << coords.second;
        positions[i] = std::make_shared<Position>(name.str());
    }
}

void Map::insertNeighbourPositions() {
    for (size_t i = 0; i < positions.size(); ++i) {
        auto neighbours = getNeighbours(i);
        for (auto &&neighbour : neighbours) {
            positions[i]->prepareToBeWalked(neighbour);
        }
    }
}

std::vector<std::shared_ptr<Entity>> Map::getNeighbours(size_t index) {
    std::vector<std::shared_ptr<Entity>> result;
    PositionIterator iterator(index, sizeX, sizeY);
    result.push_back(positions[iterator.getRelativeIndex(0, -1)]); // up
    result.push_back(positions[iterator.getRelativeIndex(0, +1)]); // down
    result.push_back(positions[iterator.getRelativeIndex(-1, 0)]); // left
    result.push_back(positions[iterator.getRelativeIndex(+1, 0)]); // right
    return result;
}

Map::~Map() {
    for (size_t i = 0; i < positions.size(); ++i) {
        positions[i]->clear();
    }
}

// --------------- Iterator ---------------------

Map::PositionIterator::PositionIterator(size_t sizeX, size_t sizeY) : PositionIterator(sizeX * sizeY, sizeX, sizeY) { }
Map::PositionIterator::PositionIterator(size_t pos, size_t sizeX, size_t sizeY)
        : pos(pos), sizeX(sizeX), sizeY(sizeY) {}

std::pair<size_t, size_t> Map::PositionIterator::getCoordinates() const {
    return getCoordinates(pos);
}

std::pair<size_t, size_t> Map::PositionIterator::getCoordinates(size_t position) const {
    return std::pair<size_t, size_t>(position % sizeX, position / sizeX);
}


Map::EntityIterator::EntityIterator(
        Map &map, const Map::PositionIterator &iterator, size_t entityIndex)
        : map(map), position(iterator), end(map.positionEnd()), entityIndex(entityIndex) {
    while (position != end && getPosition()->entitiesCount() == 0) {
        ++position;
        this->entityIndex = 0;
    }
}

void Map::EntityIterator::operator++() {
    if (getPosition()->entitiesCount() > entityIndex + 1) {
        entityIndex++;
    } else {
        ++position;
        entityIndex = 0;
        while (position != end && getPosition()->entitiesCount() == 0) {
            ++position;
        }
    }
}

void Map::EntityIterator::operator=(const Map::EntityIterator &other) {
    Map::EntityIterator aux(other.map, other.position, other.entityIndex);

    std::swap(this->map, aux.map);
    std::swap(this->position, aux.position);
    std::swap(this->entityIndex, aux.entityIndex);
    std::swap(this->end, aux.end);
}

