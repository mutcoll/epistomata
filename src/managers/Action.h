/**
 * @file Action.h
 * @author jmmut
 * @date 2016-12-01.
 */

#ifndef EPISTOMATA_ACTION_H
#define EPISTOMATA_ACTION_H

#include <vector>
#include <traits/Properties.h>
#include <memory>


class Entity;
using PtrEntity = std::shared_ptr<Entity>;
using VecEntity = std::vector<PtrEntity>;

struct Action {
    Properties::Traits trait;
    VecEntity objects;
};


#endif //EPISTOMATA_ACTION_H
