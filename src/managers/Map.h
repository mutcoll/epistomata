/**
 * @file Map.h
 * @author jmmut
 * @date 2016-09-30.
 */

#ifndef EPISTOMATA_MAP_H
#define EPISTOMATA_MAP_H


#include <vector>
#include <memory>
#include "traits/Entity.h"
#include "entities/Position.h"

class Map {
public:
    //    Map(Map && map) : positions(map.positions), sizeX(map.sizeX), sizeY(map.sizeY) { }

    Map(size_t x, size_t y) : sizeX(x), sizeY(y), positions(x * y) {
        insertNamedPositions();
        insertNeighbourPositions();
    }

    virtual ~Map();

    void insertNamedPositions();
    void insertNeighbourPositions();
    std::vector<std::shared_ptr<Entity>> getNeighbours(size_t index);


    struct PositionIterator {
        size_t pos, sizeX, sizeY;

        PositionIterator(size_t sizeX, size_t sizeY);
        PositionIterator(size_t pos, size_t sizeX, size_t sizeY);
        inline void operator++() { ++pos; }
        inline bool operator!=(const PositionIterator &it) const { return it.pos != pos; }
        inline PositionIterator& operator*() { return *this; }

        std::pair<size_t, size_t> getCoordinates() const;
        std::pair<size_t, size_t> getCoordinates(size_t position) const;
        inline size_t getIndex() const { return pos; }
        inline size_t getIndex(int x, int y) const {
            return inRangeY(y) * (int) sizeX + inRangeX(x);
        }
        inline size_t getRelativeIndex(int x, int y) const {
            const std::pair<size_t, size_t> &coordinates = getCoordinates();
            x += coordinates.first;
            y += coordinates.second;
            return getIndex(x, y);
        }
        inline size_t inRangeX(int x) const { return inRange(x, sizeX); }
        inline size_t inRangeY(int y) const { return inRange(y, sizeY); }
        inline size_t inRangePos(int pos) const { return inRange(pos, sizeX * sizeY); }
        inline size_t inRange(int n, size_t max) const { return (n + max ) % max; }
    };

    PositionIterator positionBegin() {
        return PositionIterator(0, sizeX, sizeY);
    }
    PositionIterator positionEnd() {
        return PositionIterator(positions.size(), sizeX, sizeY);
    }

    struct EntityIterator {
        Map &map;
        PositionIterator position;
        PositionIterator end;
        size_t entityIndex;

        EntityIterator(Map &map, const PositionIterator &iterator, size_t entityIndex);
        void operator++();
        void operator=(const EntityIterator &other);
        bool operator!=(EntityIterator it) {
            return it.position != position || it.entityIndex != entityIndex;
        }
        EntityIterator& operator*() { return *this; }
        bool operator<(const EntityIterator &other) const {
            return getEntity() < other.getEntity();
        }

        std::shared_ptr<Position> getPosition(size_t x, size_t y) {
            return map.positions[position.getIndex(x, y)];
        }
        std::shared_ptr<Position> getRelativePosition(int x, int y) {
            return map.positions[position.getRelativeIndex(x, y)];
        }
        std::shared_ptr<Position> getPosition(std::pair<size_t, size_t> x_y) {
            return getPosition(x_y.first, x_y.second);
        }
        std::shared_ptr<Position> getPosition() { return map.positions[position.pos]; }
        std::shared_ptr<Position> getPosition() const { return map.positions[position.pos]; }
        Position::EntityIterator getPositionIterator() {
            return Position::EntityIterator(entityIndex, *map.positions[position.pos]);
        }
        std::shared_ptr<Entity> getEntity() { return getPosition()->entity(entityIndex); }
        std::shared_ptr<Entity> getEntity() const { return getPosition()->entity(entityIndex); }
    };

    EntityIterator begin() {
        return EntityIterator(*this, positionBegin(), 0);
    }
    EntityIterator end() {
        return EntityIterator(*this, positionEnd(), 0);
    }

    typedef EntityIterator iterator;

    size_t sizeX, sizeY;
    std::vector<std::shared_ptr<Position>> positions;
};


#endif //EPISTOMATA_MAP_H
