/**
 * @file Action.h
 * @author jmmut
 * @date 2016-11-16.
 */

#ifndef EPISTOMATA_OPERATION_H
#define EPISTOMATA_OPERATION_H


#include <traits/Properties.h>
#include <memory/Context.h>
#include <entities/Position.h>
#include "Action.h"

namespace operations {

// operations declaration
bool operate(Position &position, Action &action);

bool noAction(Position &position, Action &action);
bool eat(Position &position, Action &action);
bool give(Position &position, Action &action);
bool grab(Position &position, Action &action);
bool move(Position &position, Action &action);
bool kill(Position &position, Action &action);
bool ask(Position &position, Action &action);


// operations storage
using Operation = bool (*)(Position &, Action &);

extern std::map<Properties::Traits, Operation> actions;


};  // operations


#endif //EPISTOMATA_OPERATION_H
