/**
 * @file Engine.h
 * @author jmmut
 * @date 2016-09-30.
 */

#ifndef EPISTOMATA_ENGINE_H
#define EPISTOMATA_ENGINE_H


#include "Map.h"
#include "traits/Visitor.h"

class Engine {
public:
    static std::string SYSTEM_PLOT;
    static std::string TREE_PLOT;
    static const int DRAWER_SPACE;

    Engine(Map &&map);
    Engine(size_t sizeX, size_t sizeY);

    void iterate();
    /** don't need to call this if you use iterate, only after moving */
    void finishMovement() const;

    // management
    void draw();
//    void visit(std::string entityName, Properties::Traits trait);
    void move(std::string entityName, int dx, int dy);
    void show(std::string entityName);
    void talk(std::string entityName);
    void ask(std::string entityName, std::string plotType);
    void teach(std::string entityName, std::string objectName, Properties::Traits objectTrait);
    Map::EntityIterator search(std::string entityName);
    Map::EntityIterator search(std::string entityName, const Map::PositionIterator &iterator);
    void list(std::ostream &output, std::string separator);
//    void move(Entity &entity, int dx, int dy, bool onlyPrepare);



private:
    Map map;
    bool hasToRemoveCurrent;
    Map::PositionIterator currentPosition;
    std::pair<std::string, Properties::Traits> lesson;


    void interaction() const;
    void action();
    void actionInPosition(std::shared_ptr<Position> position);

    class Impersonator : public Visitor, public Entity {
    public:
        static std::string genericName;
        std::string plotType;

        Properties getTraits() override;

        const std::string &getName() override;
        void receiveDrawer(Drawer &drawer) override;

        // visitor methods
        void ask(std::string who, Knowledge &knowledge) override;
        //    virtual void eat(Edible &edible) override;
        //    virtual void talk(Talkable &talkable) override;
        //    virtual void move(Movable &movable) override;
        //    virtual void teach(Teachable &teachable) override;
    };
public:
    size_t iterations, searches;

};


#endif //EPISTOMATA_ENGINE_H
