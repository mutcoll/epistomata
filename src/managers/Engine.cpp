#include <memory>
#include <entities/Book.h>
#include <entities/Character.h>
#include <entities/Fruit.h>
#include <entities/Seed.h>
#include <entities/Water.h>
#include <entities/Tree.h>
#include <entities/WaterSource.h>
#include <io/TreeContextPrinter.h>
#include <io/JsonContextPrinter.h>
#include "Operation.h"
#include "Engine.h"
#include "drawers/StdoutDrawer.h"
#include "utils/UniqueGenerator.h"

/**
 * @file Engine.cpp
 * @author jmmut
 * @date 2016-09-30.
 */

std::string Engine::SYSTEM_PLOT = "system";
std::string Engine::TREE_PLOT = "tree";

const int Engine::DRAWER_SPACE = 15;


Engine::Engine(Map &&map)
        : map(std::move(map)), hasToRemoveCurrent(false), currentPosition(map.sizeX, map.sizeY),
        iterations(0), searches(0) {}

Engine::Engine(size_t sizeX, size_t sizeY)
        : map(sizeX, sizeY),
        hasToRemoveCurrent(false),
        currentPosition(sizeX, sizeY),
        iterations(0), searches(0) {
    
    const std::string ALICE = "alice";
    const std::string POSITION_2_1 = "position_2_1";
    const std::string POSITION_0_0 = "position_0_0";
    const std::string POSITION_1_1 = "position_1_1";
    const std::string POSITION_1_0 = "position_1_0";
    const std::string BOOK = "book";

    Change grabWater(Properties::GRABBABLE,
            buildSortedConcepts({ALICE, WaterSource::genericName}),
            buildContext({Water::genericName}), 1);

    Change waterSourceIndication(Properties::UNKNOWN,
            {},
            buildContext({WaterSource::genericName}), 0.1);

    Change pathTo_10(buildMovement(ALICE, POSITION_0_0, POSITION_1_0,
            {Seed::genericName}, {POSITION_1_1}));
    Change waterSourceLocation(buildMovement(ALICE, POSITION_1_0, POSITION_1_1,
            {POSITION_0_0}, {WaterSource::genericName}));
    PtrChange pathToWater = Change::build({std::make_shared<Change>(pathTo_10),
            std::make_shared<Change>(waterSourceLocation)});
//    Change pathTo_11(buildMovementAppear(ALICE, POSITION_1_1, POSITION_2_1));

//    Change pathReturnTo_11(buildMovementAppear(ALICE, POSITION_1_1, POSITION_1_0));
    Change pathReturnTo_10(buildMovement(ALICE, POSITION_1_1, POSITION_1_0,
            {WaterSource::genericName}, {POSITION_0_0}));
    Change seedLocation(buildMovement(ALICE, POSITION_1_0, POSITION_0_0,
            {POSITION_1_1}, {Seed::genericName}));
    PtrChange pathFromWater = Change::build({std::make_shared<Change>(pathReturnTo_10),
            std::make_shared<Change>(seedLocation)});

    Context edibleContext;
    edibleContext.concepts().addNode(Concept(Seed::genericName));
    edibleContext.subcontext(Concept(ALICE)).concepts().addNode(Concept(Character::energyName, 1));

    Change fruitToSeed(Properties::EDIBLE,
            buildSortedConcepts({ALICE, Fruit::genericName}),
            edibleContext, 1);

    Change waterAndSeedToTree(Properties::GIVABLE,
            buildSortedConcepts({ALICE, Seed::genericName}),
            buildContext({Tree::genericName}) - buildContext({Water::genericName}), 1);

    Change treeToFruit(Properties::NO_ACTION,
            {},
            buildContext({Fruit::genericName}) - buildContext({Tree::genericName}), 1);


    std::shared_ptr<Character> character = std::make_shared<Character>(
            Character::childrenEnergy, ALICE);

    Concept aliceConcept(ALICE);

    SortedConcepts objects = buildSortedConcepts({ALICE, BOOK});
    Context difference;
    difference.subcontext(aliceConcept).changes().addNode(copyChange(waterSourceIndication));

    Change readWillTeachAliceTheWaterSourceLocation(
            Properties::Traits::ASKABLE, objects, difference, 1);
    character->beTaught(fruitToSeed);
    character->beTaught(waterAndSeedToTree);
    character->beTaught(treeToFruit);
    character->beTaught(grabWater);
//    character->beTaught(waterSourceLocation);
    character->beTaught(readWillTeachAliceTheWaterSourceLocation);
//    character->beTaught(seedLocation);
//    character->beTaught(pathTo_10);
//    character->beTaught(pathTo_11);
//    character->beTaught(pathReturnTo_11);
//    character->beTaught(pathReturnTo_10);
    character->beTaught(*pathToWater);
    character->beTaught(*pathFromWater);
//    character->beTaught(illusionTree0);
    //    character->beTaught(illusionTree1);
    //    character->beTaught(illusionTree2);
    //    character->beTaught(grabTree);
    //    character->beTaught(grabWater);
    Map::EntityIterator iterator = map.begin();
    iterator.getPosition(0, 0)->prepareToBeWalked(character);

    iterator.getPosition(0, 0)->prepareToBeWalked(std::make_shared<Seed>(3));
    iterator.getPosition(1, 1)->prepareToBeWalked(std::make_shared<WaterSource>());

    std::shared_ptr<Book> book = std::make_shared<Book>();
    //    book->beTaught(fruit);
    book->beTaught(waterSourceLocation);
    iterator.getPosition(0, 0)->prepareToBeWalked(book);
    //    map.positions[5].entities.push_back(std::make_shared<Book>("notebook"));

    for (std::shared_ptr<Position> position : map.positions) {
        position->finishMovement();
    }
}

void Engine::iterate() {
    interaction();
    action();
    finishMovement();
}

void Engine::interaction() const {
    for (auto &position : map.positions) {
        for (const std::shared_ptr<Entity> &entity : *position) {
            Action action = entity->action({position});
            if (not operations::operate(*position, action)) {
                LOG_INFO("operation %s unsuccessful", traits::names[action.trait].c_str());
            }
        }
    }
}

void Engine::action() {
    auto end = map.positionEnd();
    for (currentPosition = map.positionBegin(); currentPosition != end; ++currentPosition) {
        std::shared_ptr<Position> position = map.positions[currentPosition.getIndex()];
        actionInPosition(position);
    }
}

void Engine::actionInPosition(std::shared_ptr<Position> position) {
    for (std::shared_ptr<Entity> entity : *position) {
        if (entity->getTraits().is(Properties::KILLABLE)) {
            VecEntity objects = {entity};
            Action action = {Properties::KILLABLE, objects};
            bool success = operations::operate(*position, action);
            if (not success) {
                LOG_DEBUG("operation %s unsuccessful", traits::names[action.trait].c_str());
            }
        }

        if (entity->getTraits().is(Properties::GIVABLE)) {
            std::shared_ptr<Givable> givable = std::dynamic_pointer_cast<Givable>(entity);
            if (givable != nullptr) {
                actionInPosition(givable->getInventory());  // recursion
            } else {
                LOG_ERROR("inconsistency: %s says that it's Givable but casting failed",
                        entity->getName().c_str());
            }
        }
    }
}

void Engine::finishMovement() const {
    for (auto &position : map.positions) {
        position->finishMovement();
    }
}

// ------------- management -----------------

void Engine::draw() {
    StdoutDrawer drawer(DRAWER_SPACE);
    drawer.draw(map);
}

//void Engine::visit(std::string entityName, Properties::Traits trait) {
//    Map::EntityIterator iterator = search(entityName);
//    if (iterator != map.end()) {
//        if (iterator.getEntity()->getTraits().is(trait)) {
//            iterator.getEntity()->receiveVisitor(*this, trait);
//        } else {
//            std::cout << "entity " << iterator.getEntity()->getName()
//                      << " is not " << traits::names[trait]
//                      << ", try function \"show\" to see its traits";
//        }
//    } else {
//        std::cout << "Entity not found. try function \"list\" to see available entities";
//        return;
//    }
//}

void Engine::move(std::string entityName, int dx, int dy) {
    Map::EntityIterator iterator = search(entityName);
    if (iterator != map.end()) {
        Action action;
        action.trait = Properties::MOVABLE;
        action.objects.push_back(iterator.getEntity());
        action.objects.push_back(iterator.getRelativePosition(dx, dy));
        operations::operate(*iterator.getPosition(), action);
        currentPosition = iterator.position;
    } else {
        std::cout << "Entity not found. try function \"list\" to see available entities";
        return;
    }
}

void Engine::show(std::string entityName) {
    Map::EntityIterator iterator = search(entityName);
    if (iterator != map.end()) {
        std::cout << "name: " << entityName;

        auto coords = iterator.position.getCoordinates();
        std::cout << "\nposition: (" << coords.first << ", " << coords.second << ")";

        // print traits
        Properties properties = iterator.getEntity()->getTraits();
        std::cout << "\ntraits:";
        for (auto &&name : traits::names) {
            if (properties.is(name.first)) {
                std::cout << " " << name.second;
            }
        }

        if (iterator.getEntity()->getTraits().is(Properties::GIVABLE)) {
            std::shared_ptr<Givable> givable = std::dynamic_pointer_cast<Givable>(
                    iterator.getEntity());
            if (givable != nullptr) {
                std::cout << "\ninventory:";
                for (std::shared_ptr<Entity> object : *(givable->getInventory())) {
                    std::cout << " " << object->getName();
                }
            } else {
                LOG_ERROR("inconsistency: %s says that it's Givable but casting failed",
                        iterator.getEntity()->getName().c_str());
            }
        }

        if (iterator.getEntity()->getTraits().is(Properties::WALKABLE)) {
            std::shared_ptr<Walkable> walkable = std::dynamic_pointer_cast<Walkable>(
                    iterator.getEntity());
            if (walkable != nullptr) {
                std::cout << "\nwalkers:";
                for (size_t i = 0; i < walkable->entitiesCount(); ++i) {
                    std::cout << " " << walkable->entity(i)->getName();
                }
            } else {
                LOG_ERROR("inconsistency: %s says that it's Walkable but casting failed",
                        iterator.getEntity()->getName().c_str());
            }
        }
    } else {
        std::cout << "Entity not found. try function \"list\" to see available entities";
        return;
    }
}

void Engine::talk(std::string entityName) {
//    visit(entityName, Properties::TALKABLE);
}

void Engine::ask(std::string entityName, std::string plotType) {
    Map::EntityIterator iterator = search(entityName);
    if (iterator != map.end()) {
        std::shared_ptr<Impersonator> impersonator = std::make_shared<Impersonator>();
        impersonator->plotType = plotType;
        Action action;
        action.trait = Properties::ASKABLE;
        action.objects.push_back(impersonator);
        action.objects.push_back(iterator.getEntity());
        operations::operate(*iterator.getPosition(), action);
    } else {
        std::cout << "Entity not found. try function \"list\" to see available entities";
        return;
    }
}

void Engine::teach(std::string entityName, std::string objectName, Properties::Traits objectTrait) {
    lesson = {objectName, objectTrait};
//    visit(entityName, Properties::TEACHABLE);
}

Map::EntityIterator Engine::search(std::string entityName) {
    for (Map::EntityIterator &iterator : map) {
        if (iterator.getEntity()->getName() == entityName) {
            return iterator;
        }
    }
    return map.end();
}

Map::EntityIterator Engine::search(std::string entityName, const Map::PositionIterator &iterator) {
    auto it = Map::EntityIterator(map, iterator, 0);
    searches++;
    for (size_t i = 0; i < map.positions[iterator.getIndex()]->entitiesCount(); i++, ++it) {
        iterations++;
        if (it.getEntity()->getName() == entityName) {
            return it;
        }
    }
    return map.end();
}


void Engine::list(std::ostream &output, std::string separator) {
    for (const auto &entityIterator : UniqueGenerator<Map>(map)) {
        output << entityIterator.getEntity()->getName() << separator;
    }
}

// -------------- visitor ------------------

//void Engine::eat(Edible &edible) {
//    LOG_DEBUG("engine eats")
//}
//
//void Engine::talk(Talkable &talkable) {
//    LOG_DEBUG("engine talks")
//
//    std::string message = " " + talkable.getName() + ", what do you know about?";
//    std::cout << "engine:" << message << std::endl;
//    std::string response = talkable.beTalked(message);
//}
//
//void Engine::move(Movable &movable) {
//    LOG_DEBUG("engine moving %s as a walker", movable.getName().c_str());
//    int dx, dy;
//    std::tie(dx, dy) = movable.beMoved();
//    move(movable, dx, dy, true);
//}
//
//void Engine::teach(Teachable &teachable) {
//    LOG_DEBUG("engine teaches")
//
//    teachable.beTaught(
//            std::move(Change(lesson.second, {}, buildContext(lesson.first), {})));
//}
//


// ------------- Impersonator -----------------

std::string Engine::Impersonator::genericName = "impersonator";

Properties Engine::Impersonator::getTraits() {
    return Properties::UNKNOWN;
}

const std::string &Engine::Impersonator::getName() {
    return Impersonator::genericName;
}

void Engine::Impersonator::receiveDrawer(Drawer &drawer) {

}

void Engine::Impersonator::ask(std::string who, Knowledge &knowledge) {
    LOG_DEBUG("engine asks");
    if (plotType == SYSTEM_PLOT) {
        std::stringstream buffer;
        knowledge.get().print(buffer);
        std::string command = "echo '" + buffer.str() + "' | ./autograph.sh";

        // WARNING this might allow users to do bash injection via teaching arbitrary strings!
        int result = system(command.c_str());
        if (result != 0) {
            LOG_DEBUG("system plot failed. returned %d", result);
        }
    } else if (plotType == TREE_PLOT){
        knowledge.get().treePrint(std::cout);
    } else {
        JsonContextPrinter printer;
        printer.print(std::cout, knowledge.get());
    }
}
