/**
 * @file Shell.cpp
 * @author jmmut
 * @date 2016-10-01.
 */

#include <sstream>
#include "Shell.h"
#include "utils/Walkers.h"


namespace commands {
static const char *const ITERATE = "iterate";
static const char *const ITERATE_SHORT = "i";
static const char *const DRAW = "draw";
static const char *const LIST = "list";
static const char *const SHOW = "show";
static const char *const MOVE = "move";
static const char *const HELP = "help";
static const char *const ASK = "ask";
static const char *const TEACH = "teach";
static const char *const TALK = "talk";
static const char *const LOG_LEVEL = "loglevel";
static std::array<const char* const, 10> names = {{
        ITERATE, DRAW, LIST, SHOW, MOVE, HELP, ASK, TEACH, TALK, LOG_LEVEL
}};

}


Shell::Shell() : done(false), engine(6, 3) {}

void Shell::loop() {
    std::string line, word;
    std::stringstream lineStream;
    std::cout << "epistomata shell, type help to view commands, CTRL+D to exit";
    while (not done) {
        std::cout << "\n$ ";
        std::getline(std::cin, line);
        if (not std::cin) {
            done = true;
            break;
        }
        lineStream.clear();
        if (line.size() == 0) {
            lineStream.seekg(0);
        } else {
            lineStream.str("");
            lineStream << line;
        }

        lineStream >> word;
        if (word == commands::ITERATE || word == commands::ITERATE_SHORT) {
            engine.iterate();
            engine.draw();
        } else if (word == commands::HELP) {
            std::cout << "available commands: ";
            for (auto c : commands::names) {
                std::cout << c << " ";
            }
        } else if (word == commands::DRAW) {
            engine.draw();
        } else if (word == commands::ASK) {
            std::string plotType;
            lineStream >> word;
            if (not lineStream) {
                std::cout << "\"" << commands::ASK
                          << "\" takes an entity name as parameter. use \"" << commands::LIST
                          << "\" first";
                continue;
            }
            lineStream >> plotType; // don't care if no plotType is provided
            engine.ask(word, plotType);
        } else if (word == commands::TALK) {
            lineStream >> word;
            if (not lineStream) {
                std::cout << "\"" << commands::TALK
                          << "\" takes an entity name as parameter. use \"" << commands::LIST
                          << "\" first";
                continue;
            }
            engine.talk(word);
        } else if (word == commands::TEACH) {
            std::string entity, object, trait;
            lineStream >> entity;
            if (not lineStream) {
                std::cout << "\"" << commands::TEACH
                          << "\" takes an entity name as 1st parameter. use \"" << commands::LIST
                          << "\" first";
                continue;
            }
            lineStream >> object;
            if (not lineStream) {
                std::cout << "\"" << commands::TEACH
                          << "\" takes another entity name as 2nd parameter";
                continue;
            }

            lineStream >> trait;
            if (not lineStream) {
                std::cout << "\"" << commands::TEACH << "\" takes a trait as 3rd parameter";
                continue;
            }
            Properties::Traits parsedTrait;
            try {
                parsedTrait = Properties::parse(trait);
                engine.teach(entity, object, parsedTrait);
            } catch (std::exception &e) {
                std::cout << e.what();
            }
        } else if (word == commands::LIST) {
            engine.list(std::cout, " ");
        } else if (word == commands::SHOW) {
            lineStream >> word;
            if (not lineStream) {
                std::cout << "\"" << commands::SHOW
                          << "\" takes an entity name as parameter. use \"" << commands::LIST
                          << "\" first";
                continue;
            }
            engine.show(word);
        } else if (word == commands::MOVE) {
            std::string name;
            int dx, dy;
            lineStream >> name;
            if (not lineStream) {
                std::cout << "\"" << commands::MOVE
                          << "\" takes an entity name as 1st parameter. use \"" << commands::LIST
                          << "\" first";
                continue;
            }

            lineStream >> dx;
            if (not lineStream) {
                std::cout << "\"" << commands::MOVE << "\" takes a distance as 2nd parameter";
                lineStream.clear();
                continue;
            }

            lineStream >> dy;
            if (not lineStream) {
                std::cout << "\"" << commands::MOVE << "\" takes a distance as 3rd parameter";
                continue;
            }

            engine.move(name, dx, dy);
            engine.finishMovement();
        } else if (word == commands::LOG_LEVEL) {
            lineStream >> word;
            if (not lineStream) {
                std::cout << "\"" << commands::LOG_LEVEL
                          << "\" takes an log level name as parameter, ("
                          << randomize::utils::container_to_string_ss(randomize::log::getNames())
                          << ")";
                continue;
            }
            LOG_LEVEL(randomize::log::parseLogLevel(word.c_str()));
        } else {
            std::cout << "unrecognised command";
        }
    }
}

