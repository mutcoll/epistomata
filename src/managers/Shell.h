/**
 * @file Shell.h
 * @author jmmut
 * @date 2016-10-01.
 */

#ifndef EPISTOMATA_SHELL_H
#define EPISTOMATA_SHELL_H

#include <iostream>
#include "Engine.h"

class Shell {
public:
    Shell();

    void loop();

private:
    bool done;
    Engine engine;
};

#endif //EPISTOMATA_SHELL_H
