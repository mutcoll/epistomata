/**
 * @file Operation.cpp
 * @author jmmut
 * @date 2016-11-16.
 */

#include "Operation.h"
#include "traits/Entity.h"
#include "traits/Walkable.h"
#include "traits/Killable.h"
#include "traits/Askable.h"
#include "traits/Edible.h"
#include "traits/Givable.h"
#include "traits/Grabbable.h"
#include "traits/Visitor.h"
#include "utils/test/TestSuite.h"

std::map<Properties::Traits, operations::Operation> operations::actions = {
        {Properties::NO_ACTION, noAction},
        {Properties::EDIBLE, eat},
        {Properties::GIVABLE, give},
        {Properties::GRABBABLE, grab},
        {Properties::KILLABLE, kill},
        {Properties::MOVABLE, move},
        {Properties::ASKABLE, ask},
};


bool operations::operate(Position &position, Action &action) {

    std::map<Properties::Traits, Operation>::iterator found = actions.find(action.trait);
    if (found != actions.end()) {
        return found->second(position, action);
    } else {
        LOG_ERROR("operations \"%s\" unimplemented", traits::names[action.trait].c_str());
    }
    return false;
}

bool operations::noAction(Position &position, Action &action) {
    return true;
}

/**
 * @param action.objects: first is the subject, the actor. second is the object eaten
 */
bool operations::eat(Position &position, Action &action) {
    bool done = false;
    if (action.objects.size() == 2) {
        std::shared_ptr<Visitor> visitor = std::dynamic_pointer_cast<Visitor>(action.objects[0]);
        std::shared_ptr<Edible> edible = std::dynamic_pointer_cast<Edible>(action.objects[1]);
        if (visitor != nullptr && edible != nullptr) {
             size_t remaining = edible->remaining();
             size_t ate = visitor->eat(remaining);
             edible->beEaten(ate);
            done = true;
        }
    }
    return done;
}

/**
 * @param action.objects: first is the subject, the actor. second receives the gift
 */
bool operations::give(Position &position, Action &action) {
    bool done = false;
    if (action.objects.size() == 2) {
        std::shared_ptr<Visitor> visitor = std::dynamic_pointer_cast<Visitor>(action.objects[0]);
        std::shared_ptr<Givable> givable = std::dynamic_pointer_cast<Givable>(action.objects[1]);
        if (visitor != nullptr && givable != nullptr) {
            visitor->give(*givable);
            done = true;
        }
    }
    return done;
}

/**
 * @param action.objects: first is the subject, the actor. second is object grabbed
 */
bool operations::grab(Position &position, Action &action) {
    bool done = false;
    if (action.objects.size() == 2) {
        std::shared_ptr<Visitor> visitor = std::dynamic_pointer_cast<Visitor>(action.objects[0]);
        std::shared_ptr<Grabbable> grabbable = std::dynamic_pointer_cast<Grabbable>(action.objects[1]);
        ASSERT_EQUALS_OR_THROW(action.objects[1]->getTraits().is(Properties::GRABBABLE), true);
        if (visitor != nullptr) {
            visitor->grab(*grabbable);
            done = true;
        }
    }
    return done;
}

/**
 * @param action.objects: first is the subject, the actor. second is the place to which it moves
 */
bool operations::move(Position &position, Action &action) {
    if (action.objects.size() == 2) {
        std::shared_ptr<Walkable> walkableTo =
                std::dynamic_pointer_cast<Walkable>(action.objects[1]);

        if (walkableTo != nullptr) {
            position.prepareDeparture(action.objects[0]);
            walkableTo->prepareToBeWalked(action.objects[0]);
            return true;
        }
    }
    return false;
}

/**
 * @param action.objects: first is the subject, the actor. second is the place to which it moves
 */
bool operations::kill(Position &position, Action &action) {
    if (action.objects.size() == 1) {
        std::shared_ptr<Killable> killable = std::dynamic_pointer_cast<Killable>(action.objects[0]);

        if (killable != nullptr) {
            killable->beAttacked(1); // todo unharcode, but how? add a killer?
            if (killable->beKilled()) {
                position.prepareDeparture(killable);
                auto children = killable->reincarnate();
                for (auto &&child : children) {
                    position.prepareToBeWalked(child);
                }
                return true;
            }
        }
    }
    return false;
}

bool operations::ask(Position &position, Action &action) {
    if (action.objects.size() == 2) {
        std::shared_ptr<Visitor> asker = std::dynamic_pointer_cast<Visitor>(action.objects[0]);
        std::shared_ptr<Askable> asked = std::dynamic_pointer_cast<Askable>(action.objects[1]);

        if (asker != nullptr and asked != nullptr) {
            auto knowledge = asked->beAsked();
            asker->ask(asked->getName(), knowledge);
            return true;
        }
    }
    return false;
}
