#!/bin/bash

if [ "$1" == "" ]; then
	echo "pass the dot file as argument"
else
	fdp -T png $1 > $1.png
fi
